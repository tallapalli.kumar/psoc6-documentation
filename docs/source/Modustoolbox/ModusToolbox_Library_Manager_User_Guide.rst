========================================
ModusToolbox Library Manager User Guide
========================================

Overview
=========

The Library Manager provides a GUI to select which Board Support Package
(BSP) and version should be used by default when building a ModusToolbox
application. The tool also allows you to add and remove libraries, as
well as change their versions.

|image1|

**Definitions**

The following are the terms used in this guide that you may not be
familiar with:

   Local lib file – a .lib file present directly in the project whose
   library is being edited. Shared lib file – a .lib file that is used
   indirectly via the shared library mechanism. Target project – the
   project directory whose library is being directly edited.

   Fully editable mode – user is able to add, remove, and change the
   version of library.

   Restricted edit mode – user can manage version of selected library
   but cannot add or remove library.


Launch the Library Manager
===========================

You can launch the Library Manager from, and use it with, the Eclipse
IDE for ModusToolbox. You can also run it independently of the Eclipse
IDE.

From Eclipse IDE
-----------------

To run the Library Manager from an application within the Eclipse IDE,
right-click on the project and select **ModusToolbox > Library
Manager**.

|image2|

You can also open the Library Manager by clicking the link in the IDE
Quick Panel.

When the Library Manager opens, it collects a list of available BSPs and
libraries for the selected project, and all the necessary metadata from
a webservice. The tool also checks the current project directory for all
\*.lib files to find the currently selected libraries.

.. note::
   The ModusToolbox Project Creator tool runs the make getlibs
   command in addition to cloning the repo itself. So, projects are
   prepared for the Library Manager to parse. If you manually create a
   project using

git clone, then you will also need to run make getlibs.

Without the Eclipse IDE
------------------------

To run the Library Manager independently, navigate to the install
location and run the executable. On Windows, the default install
location for the Library Manager is:

   *<install_dir>\\tools_<version>\\library-manager*

For other operating systems, the installation directory will vary, based
on how the software was installed.

When run independently, the Library Manager opens with the target
directory set as <user-home> or the directory used from the previous
session. If you are using the Library Manager in stand-alone mode, you
must have first created and deployed a project. That is, the make
getlibs command must have been run to download all the default project
dependencies and .lib files.

From the Command Line
----------------------

You can run the tool from the command line. However, there are only a
few reasons to do this in practice. The primary use case would be part
of an overall build script for the entire application.

For information about command line options, run the tool using the -h
option.



GUI Description
================

Menus
------

The Library Manager has two menus, as follows:

   Settings

-  Offline – Check box to switch to offline mode and read the local copy
   of the manifest file installed from the offline content bundle.
   Refer to the *ModusToolbox User Guide* for more details.

-  Proxy Settings – Opens a dialog to specify direct or manual proxy
   settings.

|image3|

   Help

   -  View Help – Opens this document.

   -  About – Displays tool version information.

Fields
-------

The Library Manager contains the following fields at the top of the GUI:

   **Directory:** Location of the application's top-level directory,
   which contains one or more ModusToolbox makefile projects. Use the
   **Browse…** button to select a different directory.

   **Project** Location of the application's selected makefile. Use the
   pull-down menu to select another file, if applicable.

   **Active BSP:** The name of the currently active target board. Use
   the pull-down menu to choose any of the boards with a selected check
   box in the **Boards** tab. You can have multiple sets of BSP code in
   a project; however, the make project is built only for the selected
   **Active Board**.

**Filter:** Text field used to limit the number of boards or
libraries shown on the selected tab.

Tabs
-----

The Library Manager may display one or more of the following tabs,
depending several variables:

   **BSP tab:** Shows available BSPs and versions. This tab is hidden if
   there are no BSPs in the project and the project type is a library
   project or if the project uses shared BSPs.

   **Libraries tab:** Shows libraries supported by the Active BSP. This
   tab is hidden if the project type is a library project and if the
   project contains zero local libraries.

   **Shared BSPs tab:** Shows a read-only view of BSPs that are
   available via the shared library mechanism.
   This tab is hidden if the project is not using any shared BSPs or if
   the project type is a library project.

   **Shared Libraries tab:** Shows a read-only view of code libraries
   that will be used by the project. This tab is hidden if the project
   is not using any shared libraries or if the project type is a library
   project.


BSP and Library Versions
-------------------------

The Library Manager contains a **Version** column for each BSP and
library shown in the tool. You can select "Latest X.Y release" or "X.Y.Z
release." These represent tags for versions of BSPs/libraries in Cypress
GitHub repos:

   The “latest-vX.Y” tag is updated on an ongoing basis to point to a
   newer, backward-compatible version of the library when Cypress
   releases it.

   The “release-vX.Y.Z” tag always points to a specific, official
   release, and it does not change.

When you create a new project, Cypress instantiates it with .lib files
that point to the “latest-vX.Y” tag on the GitHub repo. When Cypress
releases a new version of a BSP/library, the “latest-vX.Y” tag on GitHub
moves to point to that newer version.

The next time you open the Library Manager for that project and select
or deselect an item or select different version of an item, the change
summary displays in the console. If a remote tag has a newer version,
the console displays a warning about the “latest-vX.Y” tag. It includes
a hyperlink to open a dialog that explains how the “latest-vX.Y” tag
auto-update works.

If you click **Apply**, your item(s) will be moved to the newer version.

Adding/Removing Non-Shared BSPs/Libraries
==========================================

The **BSP** and **Libraries** tabs (not **Shared** tabs) show selection
check boxes next to each of the listed BSPs and libraries supported in
the application. Selected check boxes represent BSPs/libraries already
included in the application, while those without selected check boxes
are not included. The Active BSP for the application is shown in bold
text with “(Active)” next to it.

|image4|

Adding BSPs/Libraries
----------------------

To add a BSP/library to your application, enable (select) the check box
for one or more item on each tab, and click **Apply** and the tool
starts updating the project and displaying progress in the status box.

|image5|

When complete, the additional items will display with selected check
boxes.



Removing BSPs/Libraries
------------------------

To remove a BSP/library to your application, disable (unselect) the
check box for one or more item on each tab, and click **Apply**.

**IMPORTANT NOTE:** Use caution when removing all Cypress BSPs from your
application. BSPs are responsible for ensuring key libraries are
present, including "psoc6make" for PSoC 6 projects and “baselib” for
Bluetooth projects. Before you remove all Cypress BSPs, make sure a
custom BSP is present in your application. For information on creating
and customizing BSPs, refer to the following:

   **PSoC 6:** In the *BSP User Guide*, which you can find in a link in
   the Quick Panel, under Documentation, for the BSP you are using.

   **Bluetooth:** In the *readme.md* file for the selected application.

Click **Apply** and the tool starts updating the project and displaying
progress in the status box.

|image6|

When complete, the removed items will display in the Library Manager
without selected check boxes. On disk, the libraries are removed by
creating a special .lib file with a commit set to a sentinel value that
indicates the library is removed.

Updating Shared BSPs/Libraries
===============================

For certain projects, BSPs and Libraries are shared. In these cases, you
may see additional tabs: **Shared BSPs** and **Shared Libraries**.

|image7|

You cannot update items on these shared tabs directly. Instead, you must
click the **Edit** icon |image20|. This opens another instance of the
Library Manager where the BSP and Libraries are not shared. Edit the
BSP/Library as described previously, and then close the second instance
of the Library Manager.

.. note:: 
   Even though the BSP/Library was updated in the 2\ :sup:`nd`
   instance of the Library Manager, the changes will not be displayed in
   the shared tabs.



Tool Change Description
========================

This section lists and describes the changes for each version of this
tool.

+-------------+-------------------------------------------------------+
| **Version** |    **Change Descriptions**                            |
+-------------+-------------------------------------------------------+
| 1.0         |    New tool.                                          |
+-------------+-------------------------------------------------------+
| 1.1         |    Added Settings and Help menus.                     |
|             +-------------------------------------------------------+
|             |    Moved link about version changes to the message    |
|             |    console, when it is applicable.                    |
|             +-------------------------------------------------------+
|             |    Added icon to indicate online/offline status.      |
|             +-------------------------------------------------------+
|             |    Removed Summary dialog; summary is shown in the    |
|             |    console.                                           |
+-------------+-------------------------------------------------------+



.. |image1| image:: ../_static/image/Modustoolbox/ModusToolbox-Library-Manager-User-Guide/library_manager1.png   
.. |image2| image:: ../_static/image/Modustoolbox/ModusToolbox-Library-Manager-User-Guide/library_manager2.png   
.. |image3| image:: ../_static/image/Modustoolbox/ModusToolbox-Library-Manager-User-Guide/library_manager3.png  
.. |image4| image:: ../_static/image/Modustoolbox/ModusToolbox-Library-Manager-User-Guide/library_manager4.png   
.. |image5| image:: ../_static/image/Modustoolbox/ModusToolbox-Library-Manager-User-Guide/library_manager5.png   
.. |image6| image:: ../_static/image/Modustoolbox/ModusToolbox-Library-Manager-User-Guide/library_manager6.png   
.. |image7| image:: ../_static/image/Modustoolbox/ModusToolbox-Library-Manager-User-Guide/library_manager7.png
.. |image20| image:: ../_static/image/Modustoolbox/ModusToolbox-Library-Manager-User-Guide/library_manager20.png   
