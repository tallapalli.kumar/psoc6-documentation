===========================
PSoC 6 Hello World Example
===========================

This code example demonstrates simple UART communication by printing a "Hello World" message on a terminal and blinks an LED using a Timer resource using PSoC :sup:`®` 6 MCU. 

.. raw:: html

   <a href="https://github.com/cypresssemiconductorco/mtb-example-psoc6-hello-world" target="_blank">Click here to redirect to PSoC6 Hello World Example</a><br><br>
