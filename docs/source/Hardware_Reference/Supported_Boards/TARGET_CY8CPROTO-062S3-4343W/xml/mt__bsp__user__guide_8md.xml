<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="mt__bsp__user__guide_8md" kind="file" language="Markdown">
    <compoundname>mt_bsp_user_guide.md</compoundname>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <programlisting>
<codeline><highlight class="normal">#<sp />ModusToolbox<sp />Board<sp />Support<sp />Package<sp />(BSP)<sp />Overview</highlight></codeline>
<codeline><highlight class="normal">##<sp />Introduction</highlight></codeline>
<codeline><highlight class="normal">A<sp />Board<sp />Support<sp />Package<sp />(BSP)<sp />provides<sp />a<sp />standard<sp />interface<sp />to<sp />a<sp />board's<sp />features<sp />and<sp />capabilities.<sp />The<sp />API<sp />is<sp />consistent<sp />across<sp />PSoC<sp />kits.<sp />Other<sp />software<sp />(such<sp />as<sp />middleware<sp />or<sp />the<sp />user's<sp />application)<sp />can<sp />use<sp />the<sp />BSP<sp />to<sp />configure<sp />and<sp />control<sp />the<sp />hardware.<sp />BSPs<sp />have<sp />the<sp />following<sp />characteristics:</highlight></codeline>
<codeline />
<codeline><highlight class="normal">*<sp />BSPs<sp />initialize<sp />device<sp />resources,<sp />such<sp />as<sp />clocks<sp />and<sp />power<sp />supplies<sp />to<sp />set<sp />up<sp />the</highlight></codeline>
<codeline><highlight class="normal">device<sp />to<sp />run<sp />firmware.</highlight></codeline>
<codeline><highlight class="normal">*<sp />BSPs<sp />contain<sp />linker<sp />scripts<sp />and<sp />startup<sp />code<sp />so<sp />you<sp />can<sp />customize<sp />them<sp />for<sp />your<sp />board.</highlight></codeline>
<codeline><highlight class="normal">*<sp />BSPs<sp />contain<sp />the<sp />hardware<sp />configuration<sp />(structures<sp />and<sp />macros)<sp />for<sp />both<sp />device</highlight></codeline>
<codeline><highlight class="normal">peripherals<sp />and<sp />board<sp />peripherals.</highlight></codeline>
<codeline><highlight class="normal">*<sp />BSPs<sp />provide<sp />abstraction<sp />to<sp />the<sp />board<sp />by<sp />providing<sp />common<sp />aliases<sp />or<sp />names<sp />to<sp />refer</highlight></codeline>
<codeline><highlight class="normal">to<sp />the<sp />board<sp />peripherals,<sp />such<sp />as<sp />buttons<sp />and<sp />LEDs.</highlight></codeline>
<codeline><highlight class="normal">*<sp />BSPs<sp />include<sp />the<sp />libraries<sp />for<sp />the<sp />default<sp />capabilities<sp />on<sp />the<sp />board.<sp />For<sp />example,</highlight></codeline>
<codeline><highlight class="normal">the<sp />BSP<sp />for<sp />a<sp />kit<sp />with<sp />CapSense<sp />capabilities<sp />includes<sp />the<sp />CapSense<sp />library.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">For<sp />a<sp />complete<sp />description<sp />of<sp />what<sp />the<sp />BSP<sp />provides<sp />and<sp />how<sp />it<sp />is<sp />used<sp />within<sp />ModusToolbox,<sp />see<sp />the<sp />[ModusToolbox<sp />User<sp />Guide](http://www.cypress.com/ModusToolboxUserGuide)</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />Quick<sp />Start<sp />with<sp />BSPs</highlight></codeline>
<codeline><highlight class="normal">This<sp />section<sp />provides<sp />a<sp />high-level<sp />view<sp />for<sp />using<sp />BSPs.<sp />You<sp />should<sp />be<sp />familiar<sp />with<sp />creating<sp />an<sp />application<sp />using<sp />both<sp />the<sp />ModusToolbox<sp />IDE<sp />and<sp />command-line<sp />environments.<sp />To<sp />use<sp />a<sp />BSP<sp />for<sp />a<sp />kit<sp />you<sp />need<sp />to<sp />perform<sp />the<sp />following<sp />steps:</highlight></codeline>
<codeline><highlight class="normal">1.<sp />Get<sp />a<sp />BSP<sp />using<sp />one<sp />of<sp />the<sp />following<sp />methods:</highlight></codeline>
<codeline />
<codeline><highlight class="normal"><sp /><sp /><sp />*<sp />Create<sp />an<sp />application<sp />with<sp />the<sp />Project<sp />Creation<sp />tool<sp />included<sp />with<sp />the<sp />ModusToolbox<sp />software<sp />installer<sp />(&lt;i&gt;&lt;<sp />ModusToolbox<sp />install&gt;/tools_2.x/project-creator&lt;/i&gt;).<sp />This<sp />tool<sp />can<sp />also<sp />be<sp />launched<sp />from<sp />the<sp />Eclipse<sp />IDE<sp />for<sp />ModusToolbox.<sp />The<sp />tool<sp />fetches<sp />the<sp />BSP<sp />for<sp />the<sp />kit<sp />that<sp />you<sp />selected,<sp />and<sp />places<sp />it<sp />in<sp />the<sp />*libs*<sp />directory.</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp />*<sp />In<sp />an<sp />existing<sp />application,<sp />use<sp />the<sp />Library<sp />Manager<sp />tool<sp />to<sp />fetch<sp />the<sp />required<sp />BSP.<sp />This<sp />tool<sp />is<sp />located<sp />in<sp />&lt;i&gt;&lt;<sp />ModusToolbox<sp />install<sp />&gt;/tools_2.x/library-manager&lt;/i&gt;.<sp />You<sp />can<sp />also<sp />launch<sp />it<sp />from<sp />within<sp />the<sp />ModusToolbox<sp />IDE.</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp />*<sp />In<sp />an<sp />existing<sp />application,<sp />create<sp />a<sp />.lib<sp />file<sp />specifying<sp />the<sp />BSP<sp />and<sp />version<sp />(commit,<sp />tag,<sp />or<sp />branch).<sp />Then<sp />run<sp />`make<sp />getlibs`<sp />to<sp />fetch<sp />the<sp />BSP<sp />and<sp />any<sp />associated<sp />libraries.<sp />The<sp />.lib<sp />file<sp />typically<sp />goes<sp />in<sp />the<sp />deps<sp />directory.</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp />*<sp />In<sp />an<sp />existing<sp />application,<sp />clone<sp />the<sp />BSP<sp />GitHub<sp />repository<sp />using<sp />the<sp />git<sp />clone<sp />command.<sp />For<sp />example:</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp />`git<sp />clone<sp />[https://github.com/cypresssemiconductorco/TARGET_CY8CPROTO-062-4343W/#latest-v1.X]`</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp />A<sp />BSP<sp />includes<sp />other<sp />libraries.<sp />To<sp />fetch<sp />all<sp />the<sp />libraries,<sp />use<sp />the<sp />`make<sp />getlibs`<sp />command<sp />from<sp />a<sp />command<sp />shell<sp />in<sp />the<sp />top<sp />directory<sp />of<sp />the<sp />application.</highlight></codeline>
<codeline><highlight class="normal">2.<sp />Add<sp />\#`include<sp />"cybsp.h"`<sp />to<sp />your<sp />*main.c*<sp />file.</highlight></codeline>
<codeline><highlight class="normal">3.<sp />Add<sp />`cybsp_init()`<sp />at<sp />the<sp />start<sp />of<sp />`main()`.</highlight></codeline>
<codeline><highlight class="normal">4.<sp />Refer<sp />to<sp />the<sp />API<sp />documentation<sp />available<sp />inside<sp />that<sp />BSP<sp />*docs*<sp />directory<sp />to<sp />learn<sp />more.</highlight></codeline>
<codeline />
<codeline><highlight class="normal">##<sp />References</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox](https://www.cypress.com/products/modustoolbox-software-environment)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[ModusToolbox<sp />User<sp />Guide](http://www.cypress.com/ModusToolboxUserGuide)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor,<sp />an<sp />Infineon<sp />Technologies<sp />Company](http://www.cypress.com)</highlight></codeline>
<codeline><highlight class="normal">*<sp />[Cypress<sp />Semiconductor<sp />GitHub](https://github.com/cypresssemiconductorco)</highlight></codeline>
    </programlisting>
    <location file="bsp/boards/mt_bsp_user_guide.md" />
  </compounddef>
</doxygen>