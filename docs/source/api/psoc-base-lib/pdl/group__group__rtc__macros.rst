=======
Macros
=======


.. toctree::
   
   group__group__rtc__day__of__the__week.rst
   group__group__rtc__dst__week__of__month.rst
   group__group__rtc__month.rst
   group__group__rtc__days__in__month.rst
   group__group__rtc__macros__interrupts.rst
   group__group__rtc__busy__status.rst







.. doxygengroup:: group_rtc_macros
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: