===================
RX Interrupt masks
===================

.. doxygengroup:: group_canfd_rx_interrupt_masks
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: