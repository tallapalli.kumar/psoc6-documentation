======================
Clock Trim (ILO, PILO)
======================

.. doxygengroup:: group_sysclk_trim
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
==============

.. toctree::

   group__group__sysclk__trim__funcs.rst