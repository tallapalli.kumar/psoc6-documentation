=================================
Frame/Pixel Management Functions
=================================

.. doxygengroup:: group_seglcd_functions_frame
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
