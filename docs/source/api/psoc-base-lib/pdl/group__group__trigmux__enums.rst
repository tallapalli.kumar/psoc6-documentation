=================
Enumerated Types
=================


API Reference
^^^^^^^^^^^^^^

.. toctree::
   
   group__group__trigmux__red__enums.rst
   group__group__trigmux__dst__enums.rst
   group__group__trigmux__in__enums.rst
   group__group__trigmux__out__enums.rst
   group__group__trigmux__1to1__enums.rst
   

.. doxygengroup:: group_trigmux_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: