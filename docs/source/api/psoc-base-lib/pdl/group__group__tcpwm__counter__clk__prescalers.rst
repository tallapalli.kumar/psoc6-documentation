======================
Counter CLK Prescalers
======================

.. doxygengroup:: group_tcpwm_counter_clk_prescalers
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: