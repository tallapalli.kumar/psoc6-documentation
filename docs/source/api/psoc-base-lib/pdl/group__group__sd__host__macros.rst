========
Macros
========


.. toctree::
   
   group__group__sd__host__macros__general__purpose.rst
   group__group__sd__host__macros__card__states.rst
   group__group__sd__host__macros__card__status.rst
   group__group__sd__host__macros__scr.rst
   group__group__sd__host__macros__cid.rst
   group__group__sd__host__macros__csd.rst   
   group__group__sd__host__macros__events.rst
   group__group__sd__host__macros__present__status.rst
   
   

.. doxygengroup:: group_sd_host_macros
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: