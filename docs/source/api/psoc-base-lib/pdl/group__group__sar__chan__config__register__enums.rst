=====================================
Channel Configuration Register Enums
=====================================


.. doxygengroup:: group_sar_chan_config_register_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: