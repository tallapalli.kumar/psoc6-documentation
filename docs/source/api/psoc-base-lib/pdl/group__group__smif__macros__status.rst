==============
Status Macros
==============

.. doxygengroup:: group_smif_macros_status
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: