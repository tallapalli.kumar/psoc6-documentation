=================
Enumerated Types
=================


.. toctree::
   
   group__group__sar__ctrl__register__enums.rst
   group__group__sar__sample__ctrl__register__enums.rst
   group__group__sar__sample__time__shift__enums.rst
   group__group__sar__range__thres__register__enums.rst
   group__group__sar__chan__config__register__enums.rst
   group__group__sar__intr__mask__t__register__enums.rst
   group__group__sar__mux__switch__register__enums.rst
   
   
   

.. doxygengroup:: group_sar_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: