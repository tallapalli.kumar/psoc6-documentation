=========================
Initialization Functions
=========================



.. doxygengroup:: group_ctdac_functions_init
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: