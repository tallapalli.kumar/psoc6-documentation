=================
Enumerated Types
=================



.. doxygengroup:: group_lpcomp_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: