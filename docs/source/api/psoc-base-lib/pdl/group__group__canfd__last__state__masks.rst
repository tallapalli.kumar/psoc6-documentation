======================================
Protocol Status Register (PSR) masks
======================================

.. doxygengroup:: group_canfd_last_state_masks
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

