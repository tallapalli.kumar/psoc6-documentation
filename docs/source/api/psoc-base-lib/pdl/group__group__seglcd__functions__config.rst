==============================
Block Configuration Functions
==============================

.. doxygengroup:: group_seglcd_functions_config
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
