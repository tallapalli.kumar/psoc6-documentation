======================
Output Trigger Modes
======================

.. doxygengroup:: group_tcpwm_output_trigger_modes
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: