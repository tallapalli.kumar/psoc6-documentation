==========
Functions
==========

.. doxygengroup:: group_sysclk_pm_funcs
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: