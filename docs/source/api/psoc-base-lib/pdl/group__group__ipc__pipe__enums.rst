================
Enumerated Types
================

.. doxygengroup:: group_ipc_pipe_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: