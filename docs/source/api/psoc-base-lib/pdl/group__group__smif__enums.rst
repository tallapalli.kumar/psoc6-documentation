=================
Enumerated Types
=================

.. doxygengroup:: group_smif_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: