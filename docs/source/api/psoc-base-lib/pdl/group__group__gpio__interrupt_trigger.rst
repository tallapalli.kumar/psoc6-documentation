=======================
Interrupt trigger type
=======================

.. doxygengroup:: group_gpio_interruptTrigger
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: