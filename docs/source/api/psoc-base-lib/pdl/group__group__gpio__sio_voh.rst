================================================================================
Regulated output voltage level (Voh) and input buffer trip-point of an SIO pair
================================================================================

.. doxygengroup:: group_gpio_sioVoh
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: