<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ctdac__functions__switches" kind="group">
    <compoundname>group_ctdac_functions_switches</compoundname>
    <title>Switch Control Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__switches_1ga35720692cc90978d7433d7446a438c03" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTDAC_SetAnalogSwitch</definition>
        <argsstring>(CTDAC_Type *base, uint32_t switchMask, cy_en_ctdac_switch_state_t state)</argsstring>
        <name>Cy_CTDAC_SetAnalogSwitch</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>switchMask</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctdac__enums_1gae03f3ae5e86579ba429987363ee10e9b">cy_en_ctdac_switch_state_t</ref></type>
          <declname>state</declname>
        </param>
        <briefdescription>
<para>Provide firmware control of the CTDAC switches. </para>        </briefdescription>
        <detaileddescription>
<para>Each call to this function can open a set of switches or close a set of switches.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The switches are configured by the reference source and output mode selections during initialization.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>switchMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The mask of the switches to either open or close. Select one or more values from <ref kindref="member" refid="group__group__ctdac__enums_1ga32e7a16091604113439333e17ae354b2">cy_en_ctdac_switches_t</ref> and "OR" them together.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>state</parametername>
</parameternamelist>
<parameterdescription>
<para>Open or close the switch(es). Select a value from <ref kindref="member" refid="group__group__ctdac__enums_1gae03f3ae5e86579ba429987363ee10e9b">cy_en_ctdac_switch_state_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />In<sp />addition<sp />to<sp />buffering<sp />the<sp />DAC<sp />output<sp />through<sp />Opamp0<sp />of<sp />the<sp />connecting</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />CTB,<sp />route<sp />the<sp />DAC<sp />output<sp />to<sp />a<sp />dedicated<sp />device<sp />pin<sp />by<sp />closing<sp />the<sp />CO6<sp />switch.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__switches_1ga35720692cc90978d7433d7446a438c03">Cy_CTDAC_SetAnalogSwitch</ref>(CTDAC0,<sp />(uint32_t)<sp /><ref kindref="member" refid="group__group__ctdac__enums_1gga32e7a16091604113439333e17ae354b2a5e781ccb78f6a4074999e3287c2a46ca">CY_CTDAC_SWITCH_CO6_MASK</ref>,<sp /><ref kindref="member" refid="group__group__ctdac__enums_1ggae03f3ae5e86579ba429987363ee10e9bae7fb4d27f795e0ab280aff681324eb81">CY_CTDAC_SWITCH_CLOSE</ref>);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="688" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctdac.c" bodystart="671" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="700" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__switches_1ga4adea8df5e58027a0f32e3de9a719bf3" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_CTDAC_GetAnalogSwitch</definition>
        <argsstring>(const CTDAC_Type *base)</argsstring>
        <name>Cy_CTDAC_GetAnalogSwitch</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Return the state (open or close) of the CTDAC switches. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The switches will be managed by the reference source and output mode selections when initializing the hardware.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Switch state. Compare this value to the masks found in <ref kindref="member" refid="group__group__ctdac__enums_1ga32e7a16091604113439333e17ae354b2">cy_en_ctdac_switches_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="858" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" bodystart="855" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="701" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__switches_1ga7a844f509085f39633b2d382238c0d18" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTDAC_SetSwitchCO6</definition>
        <argsstring>(CTDAC_Type *base, cy_en_ctdac_switch_state_t state)</argsstring>
        <name>Cy_CTDAC_SetSwitchCO6</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctdac__enums_1gae03f3ae5e86579ba429987363ee10e9b">cy_en_ctdac_switch_state_t</ref></type>
          <declname>state</declname>
        </param>
        <briefdescription>
<para>Open or close switch CO6 that controls whether the output gets routed directly to a pin or through Opamp0 of the CTB. </para>        </briefdescription>
        <detaileddescription>
<para>This function calls <ref kindref="member" refid="group__group__ctdac__functions__switches_1ga35720692cc90978d7433d7446a438c03">Cy_CTDAC_SetAnalogSwitch</ref> with the switchMask set to <ref kindref="member" refid="group__group__ctdac__enums_1gga32e7a16091604113439333e17ae354b2a5e781ccb78f6a4074999e3287c2a46ca">CY_CTDAC_SWITCH_CO6_MASK</ref>.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The switches is configured by the output mode selections during initialization.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This switch will temporarily be opened for deglitching if the deglitch mode is &lt;a href="group__group__ctdac__enums.html#group__group__ctdac__enums_1ggaa5bbaaf21bda846a109573f84f1e2735afc4a6c4c15e30a8c37456639ed6994d6"&gt;CY_CTDAC_DEGLITCHMODE_UNBUFFERED&lt;/a&gt; or &lt;a href="group__group__ctdac__enums.html#group__group__ctdac__enums_1ggaa5bbaaf21bda846a109573f84f1e2735a60e9b6705b3b6ad9d376e9ae683e53f3"&gt;CY_CTDAC_DEGLITCHMODE_BOTH&lt;/a&gt;.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>state</parametername>
</parameternamelist>
<parameterdescription>
<para>State of the switch, open or close.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />In<sp />addition<sp />to<sp />buffering<sp />the<sp />DAC<sp />output<sp />through<sp />Opamp0<sp />of<sp />the<sp />connecting</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />CTB,<sp />route<sp />the<sp />DAC<sp />output<sp />to<sp />a<sp />dedicated<sp />device<sp />pin<sp />by<sp />closing<sp />the<sp />CO6<sp />switch.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__switches_1ga7a844f509085f39633b2d382238c0d18">Cy_CTDAC_SetSwitchCO6</ref>(CTDAC0,<sp /><ref kindref="member" refid="group__group__ctdac__enums_1ggae03f3ae5e86579ba429987363ee10e9bae7fb4d27f795e0ab280aff681324eb81">CY_CTDAC_SWITCH_CLOSE</ref>);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="892" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" bodystart="889" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="702" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__switches_1ga649e2ee9ee577dc6397810d0244aa314" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTDAC_OpenAllSwitches</definition>
        <argsstring>(CTDAC_Type *base)</argsstring>
        <name>Cy_CTDAC_OpenAllSwitches</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Open all switches in the CTDAC (CO6 and CVD). </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Reset<sp />all<sp />the<sp />switches<sp />to<sp />destroy<sp />all<sp />previous<sp />connections.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__switches_1ga649e2ee9ee577dc6397810d0244aa314">Cy_CTDAC_OpenAllSwitches</ref>(CTDAC0);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="913" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" bodystart="910" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="703" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This set of functions is for controlling the two CTDAC analog switches, CVD, and CO6. </para>    </briefdescription>
    <detaileddescription>
<para>These are advanced functions. The switches will be managed by the reference source and output mode selections when initializing the hardware. </para>    </detaileddescription>
  </compounddef>
</doxygen>