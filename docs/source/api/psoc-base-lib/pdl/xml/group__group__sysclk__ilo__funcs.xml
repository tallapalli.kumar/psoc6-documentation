<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__sysclk__ilo__funcs" kind="group">
    <compoundname>group_sysclk_ilo_funcs</compoundname>
    <title>Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__sysclk__ilo__funcs_1gaabb2612adc103562da7d6dc451a33caa" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SysClk_IloEnable</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysClk_IloEnable</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Enables the ILO. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The watchdog timer (WDT) must be unlocked before calling this function.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />ILO<sp />needs<sp />to<sp />source<sp />the<sp />LFCLK.<sp />The<sp />watchdog<sp />timer<sp />(WDT)<sp />is<sp />unlocked</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />and<sp />all<sp />peripherals<sp />clocked<sp />using<sp />the<sp />LFCLK<sp />are<sp />disabled.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />the<sp />ILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__ilo__funcs_1gaabb2612adc103562da7d6dc451a33caa">Cy_SysClk_IloEnable</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />LFCLK<sp />source<sp />to<sp />the<sp />ILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__lf__funcs_1ga4d8aaf9e391eafb90aa8897d95525eb4">Cy_SysClk_ClkLfSetSource</ref>(<ref kindref="member" refid="group__group__sysclk__clk__lf__enums_1gga8bfd6f85870f0d30d15e73b3ed820db6a724311d4501aedcbd0699a3302aabaf9">CY_SYSCLK_CLKLF_IN_ILO</ref>);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1261" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="1253" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="1235" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__ilo__funcs_1ga544c284171c68dc5ef48bd24d2575952" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE bool</type>
        <definition>__STATIC_INLINE bool Cy_SysClk_IloIsEnabled</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysClk_IloIsEnabled</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Reports the Enabled/Disabled status of the ILO. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>Boolean status of ILO: true - Enabled, false - Disabled.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />LFCLK<sp />needs<sp />to<sp />be<sp />sourced<sp />by<sp />the<sp />PILO<sp />instead<sp />of<sp />the<sp />ILO.<sp />All</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />peripherals<sp />clocked<sp />by<sp />the<sp />LFCLK<sp />are<sp />disabled<sp />and<sp />the<sp />Watchdog</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />timer<sp />(WDT)<sp />is<sp />unlocked.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__sysclk__ilo__funcs_1ga544c284171c68dc5ef48bd24d2575952">Cy_SysClk_IloIsEnabled</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Disable<sp />the<sp />ILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__ilo__funcs_1gacc9d037c4d20e66f03f89f51596d4a55">Cy_SysClk_IloDisable</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />the<sp />PILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga4a6570666d4462b97d86e05f4e9fd492">Cy_SysClk_PiloEnable</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />LFCLK<sp />source<sp />to<sp />the<sp />PILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__lf__funcs_1ga4d8aaf9e391eafb90aa8897d95525eb4">Cy_SysClk_ClkLfSetSource</ref>(<ref kindref="member" refid="group__group__sysclk__clk__lf__enums_1gga8bfd6f85870f0d30d15e73b3ed820db6a51a3252aefb2b8198451b0d1589e47bf">CY_SYSCLK_CLKLF_IN_PILO</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1279" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="1276" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="1236" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__ilo__funcs_1gacc9d037c4d20e66f03f89f51596d4a55" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__sysclk__returns_1gad6699a184e2e3c01433251b0981558f3">cy_en_sysclk_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_sysclk_status_t Cy_SysClk_IloDisable</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_SysClk_IloDisable</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Disables the ILO. </para>        </briefdescription>
        <detaileddescription>
<para>ILO can't be disabled if WDT is enabled.</para><para><simplesect kind="return"><para>Error / status code: <linebreak />
CY_SYSCLK_SUCCESS - ILO successfully disabled <linebreak />
CY_SYSCLK_INVALID_STATE - Cannot disable the ILO if the WDT is enabled. CY_SYSCLK_INVALID_STATE - ECO already enabled For the PSoC 64 devices there are possible situations when function returns the PRA error status code. This is because for PSoC 64 devices the function uses the PRA driver to change the protected registers. Refer to <ref kindref="member" refid="group__group__pra__enums_1ga60be13e12e82986f8c0d6c6a6d4f12c5">cy_en_pra_status_t</ref> for more details.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The watchdog timer (WDT) must be unlocked before calling this function. Do not call this function if the WDT is enabled, because the WDT is clocked by the ILO.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />LFCLK<sp />needs<sp />to<sp />be<sp />sourced<sp />by<sp />the<sp />PILO<sp />instead<sp />of<sp />the<sp />ILO.<sp />All</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />peripherals<sp />clocked<sp />by<sp />the<sp />LFCLK<sp />are<sp />disabled<sp />and<sp />the<sp />Watchdog</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />timer<sp />(WDT)<sp />is<sp />unlocked.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(<ref kindref="member" refid="group__group__sysclk__ilo__funcs_1ga544c284171c68dc5ef48bd24d2575952">Cy_SysClk_IloIsEnabled</ref>())</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Disable<sp />the<sp />ILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__ilo__funcs_1gacc9d037c4d20e66f03f89f51596d4a55">Cy_SysClk_IloDisable</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />the<sp />PILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__pilo__funcs_1ga4a6570666d4462b97d86e05f4e9fd492">Cy_SysClk_PiloEnable</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />LFCLK<sp />source<sp />to<sp />the<sp />PILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__lf__funcs_1ga4d8aaf9e391eafb90aa8897d95525eb4">Cy_SysClk_ClkLfSetSource</ref>(<ref kindref="member" refid="group__group__sysclk__clk__lf__enums_1gga8bfd6f85870f0d30d15e73b3ed820db6a51a3252aefb2b8198451b0d1589e47bf">CY_SYSCLK_CLKLF_IN_PILO</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1319" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="1305" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="1237" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__sysclk__ilo__funcs_1gab8d32f79b480829d5193ba0c1741bc07" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_SysClk_IloHibernateOn</definition>
        <argsstring>(bool on)</argsstring>
        <name>Cy_SysClk_IloHibernateOn</name>
        <param>
          <type>bool</type>
          <declname>on</declname>
        </param>
        <briefdescription>
<para>Controls whether the ILO stays on during a hibernate, or through an XRES or brown-out detect (BOD) event. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>on</parametername>
</parameternamelist>
<parameterdescription>
<para>true = ILO stays on during hibernate or across XRES/BOD. <linebreak />
false = ILO turns off for hibernate or XRES/BOD.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Writes to the register/bit are ignored if the watchdog (WDT) is locked.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />ILO<sp />needs<sp />to<sp />clock<sp />the<sp />Real-Time-clock<sp />(RTC)<sp />during<sp />chip</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />hibernate,<sp />hard<sp />reset<sp />(XRES)<sp />and<sp />Brown-out<sp />detect<sp />(BOD)<sp />events.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />All<sp />peripherals<sp />clocked<sp />by<sp />the<sp />LFCLK<sp />are<sp />disabled<sp />and<sp />the</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Watchdog<sp />timer<sp />(WDT)<sp />is<sp />unlocked.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />ILO<sp />operation<sp />during<sp />chip<sp />hibernate<sp />mode<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__ilo__funcs_1gab8d32f79b480829d5193ba0c1741bc07">Cy_SysClk_IloHibernateOn</ref>(</highlight><highlight class="keyword">true</highlight><highlight class="normal">);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />the<sp />ILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__ilo__funcs_1gaabb2612adc103562da7d6dc451a33caa">Cy_SysClk_IloEnable</ref>();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />LFCLK<sp />source<sp />to<sp />the<sp />ILO<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__lf__funcs_1ga4d8aaf9e391eafb90aa8897d95525eb4">Cy_SysClk_ClkLfSetSource</ref>(<ref kindref="member" refid="group__group__sysclk__clk__lf__enums_1gga8bfd6f85870f0d30d15e73b3ed820db6a724311d4501aedcbd0699a3302aabaf9">CY_SYSCLK_CLKLF_IN_ILO</ref>);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1346" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" bodystart="1339" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_sysclk.h" line="1238" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>