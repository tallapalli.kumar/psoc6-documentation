<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ctdac__functions__interrupts" kind="group">
    <compoundname>group_ctdac_functions_interrupts</compoundname>
    <title>Interrupt Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__interrupts_1ga5d848e8b2f4ebc0fa8112954ed57c7b5" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_CTDAC_GetInterruptStatus</definition>
        <argsstring>(const CTDAC_Type *base)</argsstring>
        <name>Cy_CTDAC_GetInterruptStatus</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Return the interrupt status which gets set by the hardware when the CTDAC_VAL_NXT register value is transferred to the CTDAC_VAL register. </para>        </briefdescription>
        <detaileddescription>
<para>Once set, the CTDAC_VAL_NXT register is ready to accept a new value.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Interrupts are available in all update modes except &lt;a href="#group__group__ctdac_1group_ctdac_updatemode_direct_write"&gt;Direct write&lt;/a&gt;.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><itemizedlist>
<listitem><para>0: Value not moved from CTDAC_VAL_NXT to CTDAC_VAL</para></listitem><listitem><para>1: Value moved from CTDAC_VAL_NXT to CTDAC_VAL</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />CTDAC<sp />has<sp />interrupts<sp />enabled.<sp />Retrieve<sp />the<sp />interrupt</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />status<sp />to<sp />determine<sp />if<sp />the<sp />CTDAC<sp />is<sp />ready<sp />to<sp />accept<sp />a<sp />new<sp />value.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint8_t<sp />intrStatus;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Get<sp />the<sp />interrupt<sp />status<sp />for<sp />a<sp />specific<sp />CTDAC<sp />instance<sp />on<sp />the<sp />device.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />intrStatus<sp />=<sp /><ref kindref="member" refid="group__group__ctdac__functions__interrupts_1ga5d848e8b2f4ebc0fa8112954ed57c7b5">Cy_CTDAC_GetInterruptStatus</ref>(CTDAC0);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />If<sp />the<sp />interrupt<sp />status<sp />is<sp />1,<sp />the<sp />CTDAC<sp />is<sp />ready<sp />to<sp />accept<sp />a<sp />new<sp />value.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(1UL<sp />==<sp />intrStatus)</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />next<sp />value<sp />to<sp />output.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="947" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" bodystart="944" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="710" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__interrupts_1ga751d89f67434f47770b39b211ac488be" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTDAC_ClearInterrupt</definition>
        <argsstring>(CTDAC_Type *base)</argsstring>
        <name>Cy_CTDAC_ClearInterrupt</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Clear the interrupt that was set by the hardware when the CTDAC_VAL_NXT register value is transferred to the CTDAC_VAL register. </para>        </briefdescription>
        <detaileddescription>
<para>The interrupt must be cleared with this function so that the hardware can set subsequent interrupts and those interrupts can be forwarded to the interrupt controller, if enabled.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Interrupts are available in all update modes except &lt;a href="#group__group__ctdac_1group_ctdac_updatemode_direct_write"&gt;Direct write&lt;/a&gt;.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="974" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" bodystart="968" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="711" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__interrupts_1ga92e7c185ae5ea83c19fbd32f9949b6ea" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTDAC_SetInterrupt</definition>
        <argsstring>(CTDAC_Type *base)</argsstring>
        <name>Cy_CTDAC_SetInterrupt</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Force the CTDAC interrupt to trigger using software. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Interrupts are available in all update modes except &lt;a href="#group__group__ctdac_1group_ctdac_updatemode_direct_write"&gt;Direct write&lt;/a&gt;.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="994" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" bodystart="991" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="712" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__interrupts_1gae466daa55541a873790faee51e59061d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTDAC_SetInterruptMask</definition>
        <argsstring>(CTDAC_Type *base, uint32_t mask)</argsstring>
        <name>Cy_CTDAC_SetInterruptMask</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>mask</declname>
        </param>
        <briefdescription>
<para>Configure the CTDAC interrupt to be forwarded to the CPU interrupt controller. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Interrupts are available in all update modes except &lt;a href="#group__group__ctdac_1group_ctdac_updatemode_direct_write"&gt;Direct write&lt;/a&gt;.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mask</parametername>
</parameternamelist>
<parameterdescription>
<para>The CTDAC only has one interrupt so the mask is one bit.<itemizedlist>
<listitem><para>0: Disable CTDAC interrupt request (will not be forwarded to CPU interrupt controller)</para></listitem><listitem><para>1: Enable CTDAC interrupt request (will be forwarded to CPU interrupt controller)</para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />DMA<sp />will<sp />be<sp />used<sp />to<sp />update<sp />the<sp />DAC<sp />value.<sp />Disable<sp />the<sp />CTDAC<sp />interrupt.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctdac__functions__interrupts_1gae466daa55541a873790faee51e59061d">Cy_CTDAC_SetInterruptMask</ref>(CTDAC0,<sp />0UL);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1026" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" bodystart="1021" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="713" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__interrupts_1gafff937917044bf47b415bb1fa9a7de6d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_CTDAC_GetInterruptMask</definition>
        <argsstring>(const CTDAC_Type *base)</argsstring>
        <name>Cy_CTDAC_GetInterruptMask</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Return whether the CTDAC interrupt is forwarded to the CPU interrupt controller as configured by <ref kindref="member" refid="group__group__ctdac__functions__interrupts_1gae466daa55541a873790faee51e59061d">Cy_CTDAC_SetInterruptMask</ref>. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Interrupts are available in all update modes except &lt;a href="#group__group__ctdac_1group_ctdac_updatemode_direct_write"&gt;Direct write&lt;/a&gt;.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The CTDAC only has one interrupt so the return value is either 0 or 1.<itemizedlist>
<listitem><para>0: Interrupt output not forwarded to CPU interrupt controller</para></listitem><listitem><para>1: Interrupt output forwarded to CPU interrupt controller </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1051" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" bodystart="1048" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="714" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctdac__functions__interrupts_1gaeac6b6535bea7c9d2087f704a0515bfd" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_CTDAC_GetInterruptStatusMasked</definition>
        <argsstring>(const CTDAC_Type *base)</argsstring>
        <name>Cy_CTDAC_GetInterruptStatusMasked</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_c_t_d_a_c___type">CTDAC_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Return the bitwise AND of <ref kindref="member" refid="group__group__ctdac__functions__interrupts_1ga5d848e8b2f4ebc0fa8112954ed57c7b5">Cy_CTDAC_GetInterruptStatus</ref> and <ref kindref="member" refid="group__group__ctdac__functions__interrupts_1gae466daa55541a873790faee51e59061d">Cy_CTDAC_SetInterruptMask</ref>. </para>        </briefdescription>
        <detaileddescription>
<para>When high, the DAC interrupt is asserted and the interrupt is forwarded to the CPU interrupt controller.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Interrupts are available in all update modes except &lt;a href="#group__group__ctdac_1group_ctdac_updatemode_direct_write"&gt;Direct write&lt;/a&gt;.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><itemizedlist>
<listitem><para>0: Value not moved from CTDAC_VAL_NXT to CTDAC_VAL or not masked</para></listitem><listitem><para>1: Value moved from CTDAC_VAL_NXT to CTDAC_VAL and masked </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1075" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" bodystart="1073" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctdac.h" line="715" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This set of functions is related to the CTDAC interrupt. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>