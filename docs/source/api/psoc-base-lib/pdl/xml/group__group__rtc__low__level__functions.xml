<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__rtc__low__level__functions" kind="group">
    <compoundname>group_rtc_low_level_functions</compoundname>
    <title>Low-Level</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1gaefa2dfda62dd0bd93a020a5f3f109f55" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_RTC_ConvertDayOfWeek</definition>
        <argsstring>(uint32_t day, uint32_t month, uint32_t year)</argsstring>
        <name>Cy_RTC_ConvertDayOfWeek</name>
        <param>
          <type>uint32_t</type>
          <declname>day</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>month</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>year</declname>
        </param>
        <briefdescription>
<para>Returns a day of the week for a year, month, and day of month that are passed through parameters. </para>        </briefdescription>
        <detaileddescription>
<para>Zeller's congruence is used to calculate the day of the week. RTC HW block does not provide the converting function for day of week. This function should be called before <ref kindref="member" refid="group__group__rtc__general__functions_1ga47268d3ac315e0d41b4795362bc444a8">Cy_RTC_SetDateAndTime()</ref> to get the day of week.</para><para>For the Georgian calendar, Zeller's congruence is: h = (q + [13 * (m + 1)] + K + [K/4] + [J/4] - 2J) mod 7</para><para>h - The day of the week (0 = Saturday, 1 = Sunday, 2 = Monday, ., 6 = Friday). q - The day of the month. m - The month (3 = March, 4 = April, 5 = May, ..., 14 = February) K - The year of the century (year mod 100). J - The zero-based century (actually [year/100]) For example, the zero-based centuries for 1995 and 2000 are 19 and 20 respectively (not to be confused with the common ordinal century enumeration which indicates 20th for both cases).</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;In this algorithm January and February are counted as months 13 and 14 of the previous year.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>day</parametername>
</parameternamelist>
<parameterdescription>
<para>The day of the month, Valid range 1..31.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>month</parametername>
</parameternamelist>
<parameterdescription>
<para>The month of the year, see <ref kindref="compound" refid="group__group__rtc__month">Month definitions</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>year</parametername>
</parameternamelist>
<parameterdescription>
<para>The year value. Valid range non-zero value.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>Returns a day of the week, see <ref kindref="compound" refid="group__group__rtc__day__of__the__week">Day of the week definitions</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="934" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="904" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="599" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1ga7d79a94b5b584d933b012f11d34ac29e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE bool</type>
        <definition>__STATIC_INLINE bool Cy_RTC_IsLeapYear</definition>
        <argsstring>(uint32_t year)</argsstring>
        <name>Cy_RTC_IsLeapYear</name>
        <param>
          <type>uint32_t</type>
          <declname>year</declname>
        </param>
        <briefdescription>
<para>Checks whether the year passed through the parameter is leap or not. </para>        </briefdescription>
        <detaileddescription>
<para>This API is for checking an invalid value input for leap year. RTC HW block does not provide a validation checker against time/date values, the valid range of days in Month should be checked before SetDateAndTime() function call. Leap year is identified as a year that is a multiple of 4 or 400 but not 100.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>year</parametername>
</parameternamelist>
<parameterdescription>
<para>The year to be checked. Valid range non-zero value.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>False - The year is not leap. True - The year is leap. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="962" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="957" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="600" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1gaca23246d1ae0e7e6442215ef3d8ca068" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_RTC_DaysInMonth</definition>
        <argsstring>(uint32_t month, uint32_t year)</argsstring>
        <name>Cy_RTC_DaysInMonth</name>
        <param>
          <type>uint32_t</type>
          <declname>month</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>year</declname>
        </param>
        <briefdescription>
<para>Returns a number of days in a month passed through the parameters. </para>        </briefdescription>
        <detaileddescription>
<para>This API is for checking an invalid value input for days. RTC HW block does not provide a validation checker against time/date values, the valid range of days in Month should be checked before SetDateAndTime() function call.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>month</parametername>
</parameternamelist>
<parameterdescription>
<para>The month of the year, see <ref kindref="compound" refid="group__group__rtc__month">Month definitions</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>year</parametername>
</parameternamelist>
<parameterdescription>
<para>A year value. Valid range non-zero value.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A number of days in a month in the year passed through the parameters. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1002" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="985" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="601" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1gaf4ac08fe9e8eabc52d9f022cd3103e60" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_RTC_SyncFromRtc</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_RTC_SyncFromRtc</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>The Synchronizer updates RTC values into AHB RTC user registers from the actual RTC. </para>        </briefdescription>
        <detaileddescription>
<para>By calling this function, the actual RTC register values is copied to AHB user registers.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Only after calling &lt;a href="group__group__rtc__low__level__functions.html#group__group__rtc__low__level__functions_1gaf4ac08fe9e8eabc52d9f022cd3103e60"&gt;Cy_RTC_SyncFromRtc()&lt;/a&gt;, the RTC time values can be read. After &lt;a href="group__group__rtc__low__level__functions.html#group__group__rtc__low__level__functions_1gaf4ac08fe9e8eabc52d9f022cd3103e60"&gt;Cy_RTC_SyncFromRtc()&lt;/a&gt; calling the snapshot of the actual RTC registers are copied to the user registers. Meanwhile the RTC continues to clock. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1040" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="1019" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="602" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1ga8db7f22180c44fafceb083e2dcbf9555" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__rtc__enums_1ga45a1b113f07f81bc369d7bf9da75eaae">cy_en_rtc_status_t</ref></type>
        <definition>__STATIC_INLINE cy_en_rtc_status_t Cy_RTC_WriteEnable</definition>
        <argsstring>(cy_en_rtc_write_status_t writeEnable)</argsstring>
        <name>Cy_RTC_WriteEnable</name>
        <param>
          <type><ref kindref="member" refid="group__group__rtc__enums_1ga876952bea2bd3512cd8f523e00e38708">cy_en_rtc_write_status_t</ref></type>
          <declname>writeEnable</declname>
        </param>
        <briefdescription>
<para>Set/Clear writeable option for RTC user registers. </para>        </briefdescription>
        <detaileddescription>
<para>When the Write bit is set, data can be written into the RTC user registers. After all the RTC writes are done, the firmware must clear (call Cy_RTC_WriteEnable(RTC_WRITE_DISABLED)) the Write bit for the RTC update to take effect.</para><para>Set/Clear cannot be done if the RTC is still busy with a previous update (CY_RTC_BUSY = 1) or RTC Reading is executing.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>writeEnable</parametername>
</parameternamelist>
<parameterdescription>
<para>Write status, see <ref kindref="member" refid="group__group__rtc__enums_1ga876952bea2bd3512cd8f523e00e38708">cy_en_rtc_write_status_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>CY_RTC_SUCCESS - Set/Clear Write bit was successful. CY_RTC_INVALID_STATE - RTC is busy with a previous update. See <ref kindref="member" refid="group__group__rtc__enums_1ga45a1b113f07f81bc369d7bf9da75eaae">cy_en_rtc_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1092" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="1064" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="603" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1gab5b39e5ed5d0ce574633c7008f07d093" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_RTC_GetSyncStatus</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_RTC_GetSyncStatus</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Return current status of CY_RTC_BUSY. </para>        </briefdescription>
        <detaileddescription>
<para>The status indicates synchronization between the RTC user register and the actual RTC register. CY_RTC_BUSY bit is set if it is synchronizing. It is not possible to set the Read or Write bit until CY_RTC_BUSY clears.</para><para><simplesect kind="return"><para>The status of RTC user register synchronization. See <ref kindref="compound" refid="group__group__rtc__busy__status">RTC Status definitions</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1112" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="1109" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="604" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1ga3ca19fe52a0d3321de4c8ddce5b26c3f" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_RTC_ConvertBcdToDec</definition>
        <argsstring>(uint32_t bcdNum)</argsstring>
        <name>Cy_RTC_ConvertBcdToDec</name>
        <param>
          <type>uint32_t</type>
          <declname>bcdNum</declname>
        </param>
        <briefdescription>
<para>Converts an 8-bit BCD number into an 8-bit hexadecimal number. </para>        </briefdescription>
        <detaileddescription>
<para>Each byte is converted individually and returned as an individual byte in the 32-bit variable.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>bcdNum</parametername>
</parameternamelist>
<parameterdescription>
<para>An 8-bit BCD number. Each byte represents BCD.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>decNum An 8-bit hexadecimal equivalent number of the BCD number.</para></simplesect>
For example, for 0x11223344 BCD number, the function returns 0x2C in hexadecimal format. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1144" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="1133" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="605" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1ga97b5ef395c6fde3e299252f200d70942" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE uint32_t</type>
        <definition>__STATIC_INLINE uint32_t Cy_RTC_ConvertDecToBcd</definition>
        <argsstring>(uint32_t decNum)</argsstring>
        <name>Cy_RTC_ConvertDecToBcd</name>
        <param>
          <type>uint32_t</type>
          <declname>decNum</declname>
        </param>
        <briefdescription>
<para>Converts an 8-bit hexadecimal number into an 8-bit BCD number. </para>        </briefdescription>
        <detaileddescription>
<para>Each byte is converted individually and returned as an individual byte in the 32-bit variable.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>decNum</parametername>
</parameternamelist>
<parameterdescription>
<para>An 8-bit hexadecimal number. Each byte is represented in hex. 0x11223344 -&gt; 0x20 hex format.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>An 8-bit BCD equivalent of the passed hexadecimal number.</para></simplesect>
For example, for 0x11223344 hexadecimal number, the function returns 0x20 BCD number. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1176" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="1166" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="606" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1gab52c64f8d94f511d74a9c420203b24a0" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE <ref kindref="member" refid="group__group__rtc__enums_1ga40c0bfe67e6abf9f8807c8653de6a11b">cy_en_rtc_hours_format_t</ref></type>
        <definition>__STATIC_INLINE cy_en_rtc_hours_format_t Cy_RTC_GetHoursFormat</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_RTC_GetHoursFormat</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Returns current 12/24 hours format. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Before getting the RTC current hours format, the &lt;a href="group__group__rtc__low__level__functions.html#group__group__rtc__low__level__functions_1gaf4ac08fe9e8eabc52d9f022cd3103e60"&gt;Cy_RTC_SyncFromRtc()&lt;/a&gt; function should be called.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><simplesect kind="return"><para>The current RTC hours format. See <ref kindref="member" refid="group__group__rtc__enums_1ga40c0bfe67e6abf9f8807c8653de6a11b">cy_en_rtc_hours_format_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1196" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="1193" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="607" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1ga2180730a17231f562c6db1dda94ae5a6" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE bool</type>
        <definition>__STATIC_INLINE bool Cy_RTC_IsExternalResetOccurred</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_RTC_IsExternalResetOccurred</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>The function checks the reset cause and returns the Boolean result. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>True if the reset reason is the power cycle and the XRES (external reset). False if the reset reason is other than power cycle and the XRES.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Based on a return value the RTC time and date can be updated or skipped after the device reset. For example, you should skip the &lt;a href="group__group__rtc__alarm__functions.html#group__group__rtc__alarm__functions_1ga92807dc709c679c70c07d44cf0331a94"&gt;Cy_RTC_SetAlarmDateAndTime()&lt;/a&gt; call function if internal WDT reset occurs. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1217" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="1214" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="608" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1ga749c1f8be6f66b49d7e9205449ac3653" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_RTC_SyncToRtcAhbDateAndTime</definition>
        <argsstring>(uint32_t timeBcd, uint32_t dateBcd)</argsstring>
        <name>Cy_RTC_SyncToRtcAhbDateAndTime</name>
        <param>
          <type>uint32_t</type>
          <declname>timeBcd</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>dateBcd</declname>
        </param>
        <briefdescription>
<para>This function updates new time and date into the time and date RTC AHB registers. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>timeBcd</parametername>
</parameternamelist>
<parameterdescription>
<para>The BCD-formatted time variable which has the same bit masks as the RTC_TIME register:</para></parameterdescription>
</parameteritem>
</parameterlist>
[0:6] - Calendar seconds in BCD, the range 0-59. <linebreak />
[14:8] - Calendar minutes in BCD, the range 0-59. <linebreak />
[21:16] - Calendar hours in BCD, value depends on the 12/24-hour mode. <linebreak />
12HR: [21]:0 = AM, 1 = PM, [20:16] = 1 - 12; <linebreak />
24HR: [21:16] = 0-23. <linebreak />
[22] - Selects the 12/24-hour mode: 1 - 12-hour, 0 - 24-hour. <linebreak />
[26:24] - A calendar day of the week, the range 1 - 7, where 1 - Sunday. <linebreak />
 <parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>dateBcd</parametername>
</parameternamelist>
<parameterdescription>
<para>The BCD-formatted time variable which has the same bit masks as the RTC_DATE register:</para></parameterdescription>
</parameteritem>
</parameterlist>
[5:0] - A calendar day of a month in BCD, the range 1-31. <linebreak />
[12:8] - A calendar month in BCD, the range 1-12. <linebreak />
[23:16] - A calendar year in BCD, the range 0-99. <linebreak />
 <verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Ensure that the parameters are presented in the BCD format. Use the ConstructTimeDate() function to construct BCD time and date values. Refer to ConstructTimeDate() function description for more details about the RTC_TIME and RTC_DATE bit fields format.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;The RTC AHB registers can be updated only under condition that the Write bit is set and the RTC busy bit is cleared (RTC_BUSY = 0). Call the Cy_RTC_WriteEnable(CY_RTC_WRITE_ENABLED) and ensure that </verbatim><ref kindref="member" refid="group__group__rtc__low__level__functions_1ga8db7f22180c44fafceb083e2dcbf9555">Cy_RTC_WriteEnable()</ref> returned CY_RTC_SUCCESS. Then you can call <ref kindref="member" refid="group__group__rtc__low__level__functions_1ga749c1f8be6f66b49d7e9205449ac3653">Cy_RTC_SyncToRtcAhbDateAndTime()</ref>. Do not forget to clear the RTC Write bit to finish an RTC register update by calling Cy_RTC_WriteEnable(CY_RTC_WRITE_DISABLED) after you executed <ref kindref="member" refid="group__group__rtc__low__level__functions_1ga749c1f8be6f66b49d7e9205449ac3653">Cy_RTC_SyncToRtcAhbDateAndTime()</ref>. Ensure that <ref kindref="member" refid="group__group__rtc__low__level__functions_1ga8db7f22180c44fafceb083e2dcbf9555">Cy_RTC_WriteEnable()</ref> returned CY_RTC_SUCCESS. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1266" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="1262" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="610" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1gaef65b4e1818b36e57229ee4ff7a83784" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_RTC_SyncToRtcAhbAlarm</definition>
        <argsstring>(uint32_t alarmTimeBcd, uint32_t alarmDateBcd, cy_en_rtc_alarm_t alarmIndex)</argsstring>
        <name>Cy_RTC_SyncToRtcAhbAlarm</name>
        <param>
          <type>uint32_t</type>
          <declname>alarmTimeBcd</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>alarmDateBcd</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__rtc__enums_1gad3785b5ad8bf1b0d49cc5f639176db5f">cy_en_rtc_alarm_t</ref></type>
          <declname>alarmIndex</declname>
        </param>
        <briefdescription>
<para>This function updates new alarm time and date into the alarm tire and date RTC AHB registers. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>alarmTimeBcd</parametername>
</parameternamelist>
<parameterdescription>
<para>The BCD-formatted time variable which has the same bit masks as the ALMx_TIME register time fields:</para></parameterdescription>
</parameteritem>
</parameterlist>
[0:6] - Alarm seconds in BCD, the range 0-59. <linebreak />
[7] - Alarm seconds Enable: 0 - ignore, 1 - match. <linebreak />
[14:8] - Alarm minutes in BCD, the range 0-59. <linebreak />
[15] - Alarm minutes Enable: 0 - ignore, 1 - match. <linebreak />
[21:16] - Alarm hours in BCD, value depending on the 12/24-hour mode (RTC_CTRL_12HR)<linebreak />
12HR: [21]:0 = AM, 1 = PM, [20:16] = 1 - 12; <linebreak />
24HR: [21:16] = the range 0-23. <linebreak />
[23] - Alarm hours Enable: 0 - ignore, 1 - match. <linebreak />
[26:24] - An alarm day of the week, the range 1 - 7, where 1 - Monday. <linebreak />
[31] - An alarm day of the week Enable: 0 - ignore, 1 - match. <linebreak />
 <parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>alarmDateBcd</parametername>
</parameternamelist>
<parameterdescription>
<para>The BCD-formatted date variable which has the same bit masks as the ALMx_DATE register date fields: <linebreak />
[5:0] - An alarm day of a month in BCD, the range 1-31. <linebreak />
[7] - An alarm day of a month Enable: 0 - ignore, 1 - match. <linebreak />
[12:8] - An alarm month in BCD, the range 1-12. <linebreak />
[15] - An alarm month Enable: 0 - ignore, 1 - match. <linebreak />
[31] - The Enable alarm: 0 - Alarm is disabled, 1 - Alarm is enabled. <linebreak />
 </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>alarmIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The alarm index to be configured, see <ref kindref="member" refid="group__group__rtc__enums_1gad3785b5ad8bf1b0d49cc5f639176db5f">cy_en_rtc_alarm_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Ensure that the parameters are presented in the BCD format. Use the ConstructTimeDate() function to construct BCD time and date values. Refer to ConstructTimeDate() function description for more details about the RTC ALMx_TIME and ALMx_DATE bit-fields format.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;The RTC AHB registers can be updated only under condition that the Write bit is set and the RTC busy bit is cleared (RTC_BUSY = 0). Call the Cy_RTC_WriteEnable(CY_RTC_WRITE_ENABLED) and ensure that </verbatim><ref kindref="member" refid="group__group__rtc__low__level__functions_1ga8db7f22180c44fafceb083e2dcbf9555">Cy_RTC_WriteEnable()</ref> returned CY_RTC_SUCCESS. Then you can call <ref kindref="member" refid="group__group__rtc__low__level__functions_1ga749c1f8be6f66b49d7e9205449ac3653">Cy_RTC_SyncToRtcAhbDateAndTime()</ref>. Do not forget to clear the RTC Write bit to finish an RTC register update by calling the Cy_RTC_WriteEnable(CY_RTC_WRITE_DISABLED) after you executed <ref kindref="member" refid="group__group__rtc__low__level__functions_1ga749c1f8be6f66b49d7e9205449ac3653">Cy_RTC_SyncToRtcAhbDateAndTime()</ref>. Ensure that <ref kindref="member" refid="group__group__rtc__low__level__functions_1ga8db7f22180c44fafceb083e2dcbf9555">Cy_RTC_WriteEnable()</ref> returned CY_RTC_SUCCESS. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1333" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" bodystart="1319" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="611" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1ga37374ecb34a430c3735ef9ba585681b1" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__rtc__enums_1ga45a1b113f07f81bc369d7bf9da75eaae">cy_en_rtc_status_t</ref></type>
        <definition>cy_en_rtc_status_t Cy_RTC_SetNextDstTime</definition>
        <argsstring>(cy_stc_rtc_dst_format_t const *nextDst)</argsstring>
        <name>Cy_RTC_SetNextDstTime</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__rtc__dst__format__t">cy_stc_rtc_dst_format_t</ref> const *</type>
          <declname>nextDst</declname>
        </param>
        <briefdescription>
<para>A low-level DST function sets ALARM2 for a next DST event. </para>        </briefdescription>
        <detaileddescription>
<para>If Cy_RTC_GetDSTStatus() is true(=1), the next DST event should be the DST stop, then this function should be called with the DST stop time. Used by the <ref kindref="member" refid="group__group__rtc__dst__functions_1gafaa2c3a4ba08f8fa5050b42527680995">Cy_RTC_EnableDstTime</ref> and <ref kindref="member" refid="group__group__rtc__interrupt__functions_1gab9511169906c3a9f1130fdcd826a15e7">Cy_RTC_DstInterrupt</ref> functions.</para><para>If the time format(.format) is relative option(=0), the RelativeToFixed() is called to convert to a fixed date.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>nextDst</parametername>
</parameternamelist>
<parameterdescription>
<para>The structure with time at which a next DST event should occur (ALARM2 interrupt should occur). See <ref kindref="compound" refid="structcy__stc__rtc__config__t">cy_stc_rtc_config_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A validation check result of RTC register update. See <ref kindref="member" refid="group__group__rtc__enums_1ga45a1b113f07f81bc369d7bf9da75eaae">cy_en_rtc_status_t</ref>. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="957" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="906" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="613" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__rtc__low__level__functions_1gae2d29ebc8c658ccfefcc2389d26d6bec" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>bool</type>
        <definition>bool Cy_RTC_GetDstStatus</definition>
        <argsstring>(cy_stc_rtc_dst_t const *dstTime, cy_stc_rtc_config_t const *timeDate)</argsstring>
        <name>Cy_RTC_GetDstStatus</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__rtc__dst__t">cy_stc_rtc_dst_t</ref> const *</type>
          <declname>dstTime</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__rtc__config__t">cy_stc_rtc_config_t</ref> const *</type>
          <declname>timeDate</declname>
        </param>
        <briefdescription>
<para>A low-level DST function returns the current DST status using given time information. </para>        </briefdescription>
        <detaileddescription>
<para>This function is used in the initial state of a system. If the DST is enabled, the system sets the DST start or stop as a result of this function. Used by the <ref kindref="member" refid="group__group__rtc__dst__functions_1gafaa2c3a4ba08f8fa5050b42527680995">Cy_RTC_EnableDstTime</ref> and <ref kindref="member" refid="group__group__rtc__interrupt__functions_1gab9511169906c3a9f1130fdcd826a15e7">Cy_RTC_DstInterrupt</ref> functions.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>dstTime</parametername>
</parameternamelist>
<parameterdescription>
<para>The DST configuration structure, see <ref kindref="compound" refid="structcy__stc__rtc__dst__t">cy_stc_rtc_dst_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeDate</parametername>
</parameternamelist>
<parameterdescription>
<para>The time and date structure. The the appropriate DST time is set based on this time and date, see <ref kindref="compound" refid="structcy__stc__rtc__config__t">cy_stc_rtc_config_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>False - The current date and time is out of the DST period. True - The current date and time is in the DST period. </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1062" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_rtc.c" bodystart="981" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_rtc.h" line="614" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>