<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ctb__functions__switches" kind="group">
    <compoundname>group_ctb_functions_switches</compoundname>
    <title>Switch Control Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__switches_1ga63ec666c26a24b2668124d7ddb69d0d3" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTB_SetAnalogSwitch</definition>
        <argsstring>(CTBM_Type *base, cy_en_ctb_switch_register_sel_t switchSelect, uint32_t switchMask, cy_en_ctb_switch_state_t state)</argsstring>
        <name>Cy_CTB_SetAnalogSwitch</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1gaec6e363ff07a3f06e9e4f1c709c1574e">cy_en_ctb_switch_register_sel_t</ref></type>
          <declname>switchSelect</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>switchMask</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1gac8acf16f6a93889e58b75b4815fe28e1">cy_en_ctb_switch_state_t</ref></type>
          <declname>state</declname>
        </param>
        <briefdescription>
<para>Provide firmware control of the CTB switches. </para>        </briefdescription>
        <detaileddescription>
<para>Each call to this function can open a set of switches or close a set of switches in one register.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>switchSelect</parametername>
</parameternamelist>
<parameterdescription>
<para>A value of the enum <ref kindref="member" refid="group__group__ctb__enums_1gaec6e363ff07a3f06e9e4f1c709c1574e">cy_en_ctb_switch_register_sel_t</ref> to select the switch register</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>switchMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The mask of the switches to either open or close. The switch masks can be found in the following enums: <ref kindref="member" refid="group__group__ctb__enums_1ga489ce541855b5cc316c20a7f801b068c">cy_en_ctb_oa0_switches_t</ref>, <ref kindref="member" refid="group__group__ctb__enums_1ga2d6abb7e9f8fb4e7c41bf1480d829eeb">cy_en_ctb_oa1_switches_t</ref>, and <ref kindref="member" refid="group__group__ctb__enums_1gaf0495f79031c0cac37acdb7830cb0149">cy_en_ctb_ctd_switches_t</ref>. Use the enum that is consistent with the provided register.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>state</parametername>
</parameternamelist>
<parameterdescription>
<para><ref kindref="member" refid="group__group__ctb__enums_1ggac8acf16f6a93889e58b75b4815fe28e1a0d835ce0061b1e33561452130ffa11d5">CY_CTB_SWITCH_OPEN</ref> or <ref kindref="member" refid="group__group__ctb__enums_1ggac8acf16f6a93889e58b75b4815fe28e1ae2c904113408e3b49060ed262c68b296">CY_CTB_SWITCH_CLOSE</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />OA0<sp />has<sp />been<sp />configured<sp />as<sp />an<sp />opamp<sp />with<sp />10x<sp />output<sp />drive<sp />using<sp />the<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />pre-defined<sp />Cy_CTB_Fast_Opamp0_Opamp10x<sp />configuration.<sp />The<sp />10x<sp />output<sp />has</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />a<sp />dedicated<sp />connection<sp />to<sp />Pin<sp />9.2.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Call<sp />SetAnalogSwitch<sp />to<sp />route<sp />the<sp />non-inverting<sp />input<sp />of<sp />OA0<sp />to<sp />Pin<sp />9.0</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />and<sp />the<sp />inverting<sp />input<sp />to<sp />Pin<sp />9.1.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Note<sp />that<sp />the<sp />CTB<sp />port<sp />may<sp />vary<sp />based<sp />on<sp />device.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Select<sp />OA0<sp />switch<sp />register.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__enums_1gaec6e363ff07a3f06e9e4f1c709c1574e">cy_en_ctb_switch_register_sel_t</ref><sp />switchSelect<sp />=<sp /><ref kindref="member" refid="group__group__ctb__enums_1ggaec6e363ff07a3f06e9e4f1c709c1574ea50c28d673af60be66d7e4a4902dbabd3">CY_CTB_SWITCH_OA0_SW</ref>;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Select<sp />two<sp />switches<sp />for<sp />Pin<sp />0<sp />and<sp />Pin<sp />1<sp />of<sp />the<sp />CTB<sp />port.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />switchMask<sp />=<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga489ce541855b5cc316c20a7f801b068caf66ad18756866b4ee3f3cd2da7b0fcc0">CY_CTB_SW_OA0_POS_PIN0_MASK</ref><sp />|<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga489ce541855b5cc316c20a7f801b068ca44bd09da7a365d775cfc1e4dc25b214c">CY_CTB_SW_OA0_NEG_PIN1_MASK</ref>;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />state<sp />of<sp />the<sp />switches<sp />to<sp />closed<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__enums_1gac8acf16f6a93889e58b75b4815fe28e1">cy_en_ctb_switch_state_t</ref><sp />state<sp />=<sp /><ref kindref="member" refid="group__group__ctb__enums_1ggac8acf16f6a93889e58b75b4815fe28e1ae2c904113408e3b49060ed262c68b296">CY_CTB_SWITCH_CLOSE</ref>;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__switches_1ga63ec666c26a24b2668124d7ddb69d0d3">Cy_CTB_SetAnalogSwitch</ref>(CTBM0,<sp />switchSelect,<sp />switchMask,<sp />state);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1306" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctb.c" bodystart="1259" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1088" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__switches_1gac46b64ef819b09e96d0d932e9d44e735" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>uint32_t</type>
        <definition>uint32_t Cy_CTB_GetAnalogSwitch</definition>
        <argsstring>(const CTBM_Type *base, cy_en_ctb_switch_register_sel_t switchSelect)</argsstring>
        <name>Cy_CTB_GetAnalogSwitch</name>
        <param>
          <type>const <ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1gaec6e363ff07a3f06e9e4f1c709c1574e">cy_en_ctb_switch_register_sel_t</ref></type>
          <declname>switchSelect</declname>
        </param>
        <briefdescription>
<para>Return the open or closed state of the specified analog switch. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>switchSelect</parametername>
</parameternamelist>
<parameterdescription>
<para>A value of the enum <ref kindref="member" refid="group__group__ctb__enums_1gaec6e363ff07a3f06e9e4f1c709c1574e">cy_en_ctb_switch_register_sel_t</ref> to select the switch register</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>The state of the switches in the provided register. Compare this value to the switch masks in the following enums: <ref kindref="member" refid="group__group__ctb__enums_1ga489ce541855b5cc316c20a7f801b068c">cy_en_ctb_oa0_switches_t</ref>, <ref kindref="member" refid="group__group__ctb__enums_1ga2d6abb7e9f8fb4e7c41bf1480d829eeb">cy_en_ctb_oa1_switches_t</ref>, and <ref kindref="member" refid="group__group__ctb__enums_1gaf0495f79031c0cac37acdb7830cb0149">cy_en_ctb_ctd_switches_t</ref>.</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Query<sp />if<sp />the<sp />positive<sp />terminal<sp />of<sp />Opamp0<sp />is<sp />connected<sp />to<sp />P9.0<sp />or<sp />not.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />switchMask;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />switchMask<sp />=<sp /><ref kindref="member" refid="group__group__ctb__functions__switches_1gac46b64ef819b09e96d0d932e9d44e735">Cy_CTB_GetAnalogSwitch</ref>(CTBM0,<sp /><ref kindref="member" refid="group__group__ctb__enums_1ggaec6e363ff07a3f06e9e4f1c709c1574ea50c28d673af60be66d7e4a4902dbabd3">CY_CTB_SWITCH_OA0_SW</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />((uint32_t)<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga489ce541855b5cc316c20a7f801b068caf66ad18756866b4ee3f3cd2da7b0fcc0">CY_CTB_SW_OA0_POS_PIN0_MASK</ref><sp />==<sp />(switchMask<sp />&amp;<sp />((uint32_t)<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga489ce541855b5cc316c20a7f801b068caf66ad18756866b4ee3f3cd2da7b0fcc0">CY_CTB_SW_OA0_POS_PIN0_MASK</ref>)))</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />positive<sp />terminal<sp />of<sp />OA0<sp />is<sp />connected<sp />to<sp />P9.0.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1352" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctb.c" bodystart="1331" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1089" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__switches_1ga6a3c738b405bc5ffdfd1a0464022dd43" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTB_OpenAllSwitches</definition>
        <argsstring>(CTBM_Type *base)</argsstring>
        <name>Cy_CTB_OpenAllSwitches</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <briefdescription>
<para>Open all the switches and disable all hardware (SAR Sequencer and DSI) control of the switches. </para>        </briefdescription>
        <detaileddescription>
<para>Primarily used as a quick method of re-configuring all analog connections that are sparsely closed.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Reset<sp />all<sp />the<sp />switches<sp />to<sp />destroy<sp />all<sp />previous<sp />connections.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__switches_1ga6a3c738b405bc5ffdfd1a0464022dd43">Cy_CTB_OpenAllSwitches</ref>(CTBM0);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1154" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1147" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1090" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__switches_1ga1071f19b5c0808366de90f17e1e25b57" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTB_EnableSarSeqCtrl</definition>
        <argsstring>(CTBM_Type *base, cy_en_ctb_switch_sar_seq_t switchMask)</argsstring>
        <name>Cy_CTB_EnableSarSeqCtrl</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga59d4d9205cd078c9cd13da897cff59c9">cy_en_ctb_switch_sar_seq_t</ref></type>
          <declname>switchMask</declname>
        </param>
        <briefdescription>
<para>Enable SAR sequencer control of specified switch(es). </para>        </briefdescription>
        <detaileddescription>
<para>This allows the SAR ADC to use routes through the CTB when configuring its channels.</para><para>There are three switches in the CTB that can be enabled by the SAR sequencer.<itemizedlist>
<listitem><para>D51: This switch connects the negative input of Opamp0 to the SARBUS0</para></listitem><listitem><para>D52: This switch connects the positive input of Opamp1 to the SARBUS0</para></listitem><listitem><para>D62: This switch connects the positive input of Opamp1 to the SARBUS1</para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>switchMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The switch or switches in which to enable SAR sequencer control. Use an enumerated value from <ref kindref="member" refid="group__group__ctb__enums_1ga59d4d9205cd078c9cd13da897cff59c9">cy_en_ctb_switch_sar_seq_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />SAR<sp />ADC<sp />has<sp />been<sp />configured<sp />to<sp />sample<sp />the<sp />opamp<sp />outputs.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Enable<sp />SAR<sp />sequencer<sp />control<sp />of<sp />all<sp />three<sp />switches<sp />in<sp />the<sp />CTB.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__switches_1ga1071f19b5c0808366de90f17e1e25b57">Cy_CTB_EnableSarSeqCtrl</ref>(CTBM0,<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga59d4d9205cd078c9cd13da897cff59c9ad1fdbc2f7df70acf1fcfc095b55afbbf">CY_CTB_SW_SEQ_CTRL_D51_D52_D62_MASK</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1188" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1183" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1091" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__switches_1ga66dc5c7b4b9dcb61e2373a015e2c35e1" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTB_DisableSarSeqCtrl</definition>
        <argsstring>(CTBM_Type *base, cy_en_ctb_switch_sar_seq_t switchMask)</argsstring>
        <name>Cy_CTB_DisableSarSeqCtrl</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga59d4d9205cd078c9cd13da897cff59c9">cy_en_ctb_switch_sar_seq_t</ref></type>
          <declname>switchMask</declname>
        </param>
        <briefdescription>
<para>Disable SAR sequencer control of specified switch(es). </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>switchMask</parametername>
</parameternamelist>
<parameterdescription>
<para>The switch or switches in which to disable SAR sequencer control. Use an enumerated value from <ref kindref="member" refid="group__group__ctb__enums_1ga59d4d9205cd078c9cd13da897cff59c9">cy_en_ctb_switch_sar_seq_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />The<sp />SAR<sp />ADC<sp />no<sp />longer<sp />needs<sp />to<sp />sample<sp />the<sp />opamp<sp />outputs.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Disable<sp />SAR<sp />sequencer<sp />control<sp />of<sp />all<sp />three<sp />switches<sp />in<sp />the<sp />CTB.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__switches_1ga66dc5c7b4b9dcb61e2373a015e2c35e1">Cy_CTB_DisableSarSeqCtrl</ref>(CTBM0,<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga59d4d9205cd078c9cd13da897cff59c9ad1fdbc2f7df70acf1fcfc095b55afbbf">CY_CTB_SW_SEQ_CTRL_D51_D52_D62_MASK</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1215" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1210" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1092" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This set of functions is for controlling routing switches. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>