<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__crypto__srv__functions" kind="group">
    <compoundname>group_crypto_srv_functions</compoundname>
    <title>Server Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__crypto__srv__functions_1ga20467da28097037178b4e9a2bb16095c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>cy_en_crypto_status_t Cy_Crypto_Server_Start_Base</definition>
        <argsstring>(cy_stc_crypto_config_t const *config, cy_stc_crypto_server_context_t *context)</argsstring>
        <name>Cy_Crypto_Server_Start_Base</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__crypto__config__t">cy_stc_crypto_config_t</ref> const *</type>
          <declname>config</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__crypto__server__context__t">cy_stc_crypto_server_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This function starts the Basic functionality for the Crypto server on the server side core, sets up an interrupt for the IPC Crypto channel, sets up an interrupt to catch Crypto HW errors. </para>        </briefdescription>
        <detaileddescription>
<para>This function available for Server side only.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The Crypto server can be run as singleton (not multi-instance) application on the one of available cores at the same time.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>The Crypto configuration structure.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the <ref kindref="compound" refid="structcy__stc__crypto__server__context__t">cy_stc_crypto_server_context_t</ref> structure that stores the Crypto server context.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Configure<sp />Server<sp />and<sp />Client<sp />as<sp />follows:</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Server:</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp /><sp />-<sp />IPC<sp />channel<sp />for<sp />communication:<sp />9</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp /><sp />-<sp />IPC<sp />interrupt<sp />structure<sp />used<sp />for<sp />new<sp />request<sp />notifications:<sp />1</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp /><sp />-<sp />Data<sp />request<sp />handler:<sp />default</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp /><sp />-<sp />Hardware<sp />errors<sp />handler:<sp />default</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Client:</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp /><sp />-<sp />IPC<sp />channel<sp />for<sp />communication:<sp />9</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp /><sp />-<sp />IPC<sp />interrupt<sp />structure<sp />used<sp />for<sp />data<sp />ready<sp />notifications:<sp />2</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp /><sp />-<sp />Data<sp />Complete<sp />callback:<sp />not<sp />used</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />MY_CHAN_CRYPTO<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(uint32_t)(9u)<sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />IPC<sp />data<sp />channel<sp />for<sp />the<sp />Crypto<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />MY_INTR_CRYPTO_SRV<sp /><sp /><sp /><sp /><sp />(uint32_t)(1u)<sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />IPC<sp />interrupt<sp />structure<sp />for<sp />the<sp />Crypto<sp />server<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />MY_INTR_CRYPTO_CLI<sp /><sp /><sp /><sp /><sp />(uint32_t)(2u)<sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />IPC<sp />interrupt<sp />structure<sp />for<sp />the<sp />Crypto<sp />client<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />MY_INTR_CRYPTO_SRV_MUX<sp />(IRQn_Type)(2u)<sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />CM0+<sp />IPC<sp />interrupt<sp />mux<sp />number<sp />the<sp />Crypto<sp />server<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />MY_INTR_CRYPTO_CLI_MUX<sp />(IRQn_Type)(3u)<sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />CM0+<sp />IPC<sp />interrupt<sp />mux<sp />number<sp />the<sp />Crypto<sp />client<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#define<sp />MY_INTR_CRYPTO_ERR_MUX<sp />(IRQn_Type)(4u)<sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />CM0+<sp />ERROR<sp />interrupt<sp />mux<sp />number<sp />the<sp />Crypto<sp />server<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcy__stc__crypto__config__t">cy_stc_crypto_config_t</ref><sp />myCryptoConfig<sp />=</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.ipcChannel<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />MY_CHAN_CRYPTO,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.acquireNotifierChannel<sp />*/</highlight><highlight class="normal"><sp />MY_INTR_CRYPTO_SRV,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.releaseNotifierChannel<sp />*/</highlight><highlight class="normal"><sp />MY_INTR_CRYPTO_CLI,</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.releaseNotifierConfig<sp />*/</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />#if<sp />(CY_CPU_CORTEX_M0P)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.intrSrc<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />MY_INTR_CRYPTO_CLI_MUX,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.cm0pSrc<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />cpuss_interrupts_ipc_2_IRQn,<sp /></highlight><highlight class="comment">/*<sp />depends<sp />on<sp />selected<sp />releaseNotifierChannel<sp />value<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />#else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.intrSrc<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />cpuss_interrupts_ipc_2_IRQn,<sp /></highlight><highlight class="comment">/*<sp />depends<sp />on<sp />selected<sp />releaseNotifierChannel<sp />value<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />#endif</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.intrPriority<sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />2u,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />},</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.userCompleteCallback<sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />NULL,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.userGetDataHandler<sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />NULL,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.userErrorHandler<sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />NULL,</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.acquireNotifierConfig<sp />*/</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />#if<sp />(CY_CPU_CORTEX_M0P)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.intrSrc<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />MY_INTR_CRYPTO_SRV_MUX,<sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />to<sp />use<sp />with<sp />DeepSleep<sp />mode<sp />should<sp />be<sp />in<sp />DeepSleep<sp />capable<sp />muxer's<sp />range<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.cm0pSrc<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />cpuss_interrupts_ipc_1_IRQn,<sp /></highlight><highlight class="comment">/*<sp />depends<sp />on<sp />selected<sp />acquireNotifierChannel<sp />value<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />#else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.intrSrc<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />cpuss_interrupts_ipc_1_IRQn,<sp /></highlight><highlight class="comment">/*<sp />depends<sp />on<sp />selected<sp />acquireNotifierChannel<sp />value<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />#endif</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.intrPriority<sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />2u,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />},</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.cryptoErrorIntrConfig<sp />*/</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />#if<sp />(CY_CPU_CORTEX_M0P)</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.intrSrc<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />MY_INTR_CRYPTO_ERR_MUX,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.cm0pSrc<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />cpuss_interrupt_crypto_IRQn,</highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />#else</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.intrSrc<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />cpuss_interrupt_crypto_IRQn,</highlight></codeline>
<codeline><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />#endif</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />.intrPriority<sp /><sp /><sp /><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal"><sp />2u,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />};</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcy__stc__crypto__server__context__t">cy_stc_crypto_server_context_t</ref><sp /><sp />myCryptoServerContext;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />cryptoStatus;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />cryptoStatus<sp />=<sp /><ref kindref="member" refid="group__group__crypto__srv__functions_1ga20467da28097037178b4e9a2bb16095c">Cy_Crypto_Server_Start_Base</ref>(&amp;myCryptoConfig,<sp />&amp;myCryptoServerContext);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />...<sp />check<sp />for<sp />errors...<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="161" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_crypto_server.c" bodystart="97" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_server.h" line="71" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__srv__functions_1gaccdace40a137ec8634f6f2114832e162" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>cy_en_crypto_status_t Cy_Crypto_Server_Start_Extra</definition>
        <argsstring>(cy_stc_crypto_config_t const *config, cy_stc_crypto_server_context_t *context)</argsstring>
        <name>Cy_Crypto_Server_Start_Extra</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__crypto__config__t">cy_stc_crypto_config_t</ref> const *</type>
          <declname>config</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__crypto__server__context__t">cy_stc_crypto_server_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This function starts the Extra functionality for the Crypto server on the server side core, sets up an interrupt for the IPC Crypto channel, sets up an interrupt to catch Crypto HW errors. </para>        </briefdescription>
        <detaileddescription>
<para>This function available for Server side only.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The Crypto server can be run as singleton (not multi-instance) application on the one of available cores at the same time.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>The Crypto configuration structure.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the <ref kindref="compound" refid="structcy__stc__crypto__server__context__t">cy_stc_crypto_server_context_t</ref> structure that stores the Crypto server context.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></para></simplesect>
Refer to <ref kindref="member" refid="group__group__crypto__srv__functions_1ga20467da28097037178b4e9a2bb16095c">Cy_Crypto_Server_Start_Base</ref> for the function usage example. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="227" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_crypto_server.c" bodystart="163" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_server.h" line="101" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__srv__functions_1gaf91c7e2d0fd473dd35d8e1df05e5c7f5" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>cy_en_crypto_status_t Cy_Crypto_Server_Start_Full</definition>
        <argsstring>(cy_stc_crypto_config_t const *config, cy_stc_crypto_server_context_t *context)</argsstring>
        <name>Cy_Crypto_Server_Start_Full</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__crypto__config__t">cy_stc_crypto_config_t</ref> const *</type>
          <declname>config</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__crypto__server__context__t">cy_stc_crypto_server_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>This function starts the Full functionality for the Crypto server on the server side core, sets up an interrupt for the IPC Crypto channel, sets up an interrupt to catch Crypto HW errors. </para>        </briefdescription>
        <detaileddescription>
<para>This function available for Server side only.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The Crypto server can be run as singleton (not multi-instance) application on the one of available cores at the same time.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>config</parametername>
</parameternamelist>
<parameterdescription>
<para>The Crypto configuration structure.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the <ref kindref="compound" refid="structcy__stc__crypto__server__context__t">cy_stc_crypto_server_context_t</ref> structure that stores the Crypto server context.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></para></simplesect>
Refer to <ref kindref="member" refid="group__group__crypto__srv__functions_1ga20467da28097037178b4e9a2bb16095c">Cy_Crypto_Server_Start_Base</ref> for the function usage example. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="293" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_crypto_server.c" bodystart="229" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_server.h" line="131" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__srv__functions_1ga0b7269e7ff67b9bee97d393830fcd976" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref></type>
        <definition>cy_en_crypto_status_t Cy_Crypto_Server_Stop</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_Crypto_Server_Stop</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function stops the Crypto server by disabling the IPC notify interrupt and Crypto error interrupt. </para>        </briefdescription>
        <detaileddescription>
<para>This function available for Server side only.</para><para><simplesect kind="return"><para><ref kindref="member" refid="group__group__crypto__enums_1ga3a65158da3dc5cb6aa016ae599542aad">cy_en_crypto_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="383" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_crypto_server.c" bodystart="354" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_server.h" line="147" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__srv__functions_1gab3a5d2a4bd48f96e41d47117aabdfef6" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_Crypto_Server_Process</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_Crypto_Server_Process</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function parses input data received from the Crypto Client, runs the appropriate Crypto function and releases the Crypto IPC channel. </para>        </briefdescription>
        <detaileddescription>
<para>This function available for Server side only.</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;This function should be used only when user register own GetDataHandler function. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="733" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_crypto_server.c" bodystart="410" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_server.h" line="162" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__srv__functions_1ga247aad529a2ad367f757c4b2aa1995da" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_Crypto_Server_GetDataHandler</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_Crypto_Server_GetDataHandler</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function is a IPC Crypto channel notify interrupt-routine. </para>        </briefdescription>
        <detaileddescription>
<para>It receives information from the Crypto client, runs the process if user not setup own handler.</para><para>This function available for Server side only. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="759" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_crypto_server.c" bodystart="735" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_server.h" line="175" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__crypto__srv__functions_1ga36706e765604590078a2a55c6355b897" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_Crypto_Server_ErrorHandler</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_Crypto_Server_ErrorHandler</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>This function is a routine to handle an interrupt caused by the Crypto hardware error. </para>        </briefdescription>
        <detaileddescription>
<para>This function available for Server side only. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="408" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_crypto_server.c" bodystart="385" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_server.h" line="187" />
      </memberdef>
      </sectiondef>
      <sectiondef kind="define">
      <memberdef id="group__group__crypto__srv__functions_1gae2935d79ec28a59c04eb95cf1d29c89b" kind="define" prot="public" static="no">
        <name>Cy_Crypto_Server_Start</name>
        <initializer><ref kindref="member" refid="group__group__crypto__srv__functions_1gaf91c7e2d0fd473dd35d8e1df05e5c7f5">Cy_Crypto_Server_Start_Full</ref></initializer>
        <briefdescription>
<para>Backward compatibility macro for the Server Start function - it starts the Server with Full functionality configuration. </para>        </briefdescription>
        <detaileddescription>
        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_server.h" bodystart="191" column="9" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_crypto_server.h" line="191" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>