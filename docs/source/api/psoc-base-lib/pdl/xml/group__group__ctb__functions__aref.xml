<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ctb__functions__aref" kind="group">
    <compoundname>group_ctb_functions_aref</compoundname>
    <title>Reference Current Mode Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__aref_1ga5a5dfa4b6150d4ba15f05a5c5ccdcaae" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_CTB_SetCurrentMode</definition>
        <argsstring>(CTBM_Type *base, cy_en_ctb_current_mode_t currentMode)</argsstring>
        <name>Cy_CTB_SetCurrentMode</name>
        <param>
          <type><ref kindref="compound" refid="struct_c_t_b_m___type">CTBM_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga279738cdf45bae5815fa28d15158b928">cy_en_ctb_current_mode_t</ref></type>
          <declname>currentMode</declname>
        </param>
        <briefdescription>
<para>High level function to configure the current modes of the opamps. </para>        </briefdescription>
        <detaileddescription>
<para>This function configures all opamps of the CTB to the same current mode. These modes are differentiated by the reference current level, the opamp input range, and the Deep Sleep mode operation.</para><para><itemizedlist>
<listitem><para>The reference current level is set using <ref kindref="member" refid="group__group__ctb__functions__aref_1ga9aa45db21ccdca8c3770f22d781f1a44">Cy_CTB_SetIptatLevel</ref></para></listitem><listitem><para>When 1 uA current level is used in Deep Sleep,<itemizedlist>
<listitem><para>All generators in the AREF must be enabled in Deep Sleep. That is, <ref kindref="member" refid="group__group__sysanalog__functions_1ga5d6d537beaf82fddf8e06f30b1d308b3">Cy_SysAnalog_SetDeepSleepMode</ref> is called with CY_SYSANALOG_DEEPSLEEP_IPTAT_IZTAT_VREF.</para></listitem></itemizedlist>
</para></listitem><listitem><para>When 100 nA current level is used,<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ctb__functions__aref_1gaa6518a1a10a15d0bc1715b71ea0bcf37">Cy_CTB_EnableRedirect</ref> is called to route the AREF IPTAT reference to the opamp IZTAT and disable the opamps IPTAT.</para></listitem><listitem><para>The IPTAT generator is enabled in Deep Sleep. That is, <ref kindref="member" refid="group__group__sysanalog__functions_1ga5d6d537beaf82fddf8e06f30b1d308b3">Cy_SysAnalog_SetDeepSleepMode</ref> is called with CY_SYSANALOG_DEEPSLEEP_IPTAT_2 unless it is already configured for CY_SYSANALOG_DEEPSLEEP_IPTAT_IZTAT_VREF.</para></listitem></itemizedlist>
</para></listitem></itemizedlist>
</para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The IPTAT level is a chip wide configuration so multiple opamps cannot operate at different IPTAT levels. When calling &lt;a href="group__group__ctb__functions__aref.html#group__group__ctb__functions__aref_1ga5a5dfa4b6150d4ba15f05a5c5ccdcaae"&gt;Cy_CTB_SetCurrentMode&lt;/a&gt; for a CTB instance on the device, it should be called for all other CTB instances as well.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Current Mode &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;IPTAT Level &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Input Range &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Deep Sleep Operation  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;&lt;a href="group__group__ctb__enums.html#group__group__ctb__enums_1gga279738cdf45bae5815fa28d15158b928ae79c4f20db95be6f845a1664e3e1a2ba"&gt;CY_CTB_CURRENT_HIGH_ACTIVE&lt;/a&gt; &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;1 uA &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Rail-to-Rail (charge pump enabled) &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Disabled in Deep Sleep  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;&lt;a href="group__group__ctb__enums.html#group__group__ctb__enums_1gga279738cdf45bae5815fa28d15158b928a0bc0fbd0ad5387ddbab821402ee5ceeb"&gt;CY_CTB_CURRENT_HIGH_ACTIVE_DEEPSLEEP&lt;/a&gt; &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;1 uA &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;0 - VDDA-1.5 V (charge pump disabled) &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Enabled in Deep Sleep  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;&lt;a href="group__group__ctb__enums.html#group__group__ctb__enums_1gga279738cdf45bae5815fa28d15158b928a6f9459a876b5b2e5442ec31dec329aff"&gt;CY_CTB_CURRENT_LOW_ACTIVE_DEEPSLEEP&lt;/a&gt; &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;100 nA &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;0 - VDDA-1.5 V (charge pump disabled) &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Enabled in Deep Sleep  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para><para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;The output range of the opamp is 0.2 V to VDDA - 0.2 V (depending on output load).&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>Pointer to structure describing registers</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>currentMode</parametername>
</parameternamelist>
<parameterdescription>
<para>Current mode selection</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Configure<sp />all<sp />CTBs<sp />on<sp />the<sp />device<sp />to<sp />use<sp />a<sp />1<sp />uA<sp />IPTAT<sp />reference.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />Enable<sp />the<sp />charge<sp />pump<sp />and<sp />disable<sp />Deep<sp />Sleep<sp />operation<sp />for<sp />both<sp />opamps<sp />in<sp />the<sp />specified<sp />CTB<sp />block.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__aref_1ga5a5dfa4b6150d4ba15f05a5c5ccdcaae">Cy_CTB_SetCurrentMode</ref>(CTBM0,<sp /><ref kindref="member" refid="group__group__ctb__enums_1gga279738cdf45bae5815fa28d15158b928ae79c4f20db95be6f845a1664e3e1a2ba">CY_CTB_CURRENT_HIGH_ACTIVE</ref>);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="773" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_ctb.c" bodystart="703" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1117" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__aref_1ga9aa45db21ccdca8c3770f22d781f1a44" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTB_SetIptatLevel</definition>
        <argsstring>(cy_en_ctb_iptat_t iptat)</argsstring>
        <name>Cy_CTB_SetIptatLevel</name>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1ga8430fbb8a80a0e6423f91062071dc018">cy_en_ctb_iptat_t</ref></type>
          <declname>iptat</declname>
        </param>
        <briefdescription>
<para>Set the IPTAT reference level to 1 uA or 100 nA. </para>        </briefdescription>
        <detaileddescription>
<para>The IPTAT generator is used by the CTB for slope offset drift.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>iptat</parametername>
</parameternamelist>
<parameterdescription>
<para>Value from enum <ref kindref="member" refid="group__group__ctb__enums_1ga8430fbb8a80a0e6423f91062071dc018">cy_en_ctb_iptat_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Configure<sp />all<sp />CTBs<sp />on<sp />the<sp />device<sp />to<sp />use<sp />a<sp />100<sp />nA<sp />IPTAT<sp />reference</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />for<sp />ultra<sp />low<sp />power.<sp />In<sp />order<sp />for<sp />the<sp />opamps<sp />to<sp />function<sp />with<sp />a<sp />100<sp />nA</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />IPTAT<sp />reference,<sp />Cy_CTB_EnableRedirect()<sp />must<sp />be<sp />called.<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />This<sp />is<sp />a<sp />low<sp />level<sp />function<sp />that<sp />only<sp />configures<sp />the<sp />IPTAT<sp />reference<sp />level.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />The<sp />preferred<sp />method<sp />is<sp />the<sp />Cy_CTB_SetCurrentMode()<sp />as<sp />it<sp />is<sp />a<sp />high</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />level<sp />function<sp />that<sp />configures<sp />both<sp />the<sp />IPTAT<sp />reference<sp />and<sp />the<sp />current<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />redirection.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__aref_1ga9aa45db21ccdca8c3770f22d781f1a44">Cy_CTB_SetIptatLevel</ref>(<ref kindref="member" refid="group__group__ctb__enums_1gga8430fbb8a80a0e6423f91062071dc018a3db7156e7ea8ee0aea0ab8b8c06d103d">CY_CTB_IPTAT_LOW</ref>);</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1424" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1419" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1118" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__aref_1gac13e18752439691a314b8f58082179b7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTB_SetClkPumpSource</definition>
        <argsstring>(cy_en_ctb_clk_pump_source_t clkPump)</argsstring>
        <name>Cy_CTB_SetClkPumpSource</name>
        <param>
          <type><ref kindref="member" refid="group__group__ctb__enums_1gab17cdf969bc01f75e31685058296f747">cy_en_ctb_clk_pump_source_t</ref></type>
          <declname>clkPump</declname>
        </param>
        <briefdescription>
<para>Set the clock source for both charge pumps in the CTB. </para>        </briefdescription>
        <detaileddescription>
<para>Recall that each opamp has its own charge pump. The clock can come from:</para><para><itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ctb__enums_1ggab17cdf969bc01f75e31685058296f747a328fdf373b541780577a1512901ea537">CY_CTB_CLK_PUMP_SRSS</ref> - a dedicated clock pump divider <ref kindref="compound" refid="group__group__sysclk__clk__pump">Pump Clock</ref>. Call the following functions to configure the pump clock from the SRSS:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__sysclk__clk__pump__funcs_1ga9fbaf9986d5413d5bb14db80f75ccc63">Cy_SysClk_ClkPumpSetSource</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__sysclk__clk__pump__funcs_1ga06b0c10e1854e3368a025b620e4a2433">Cy_SysClk_ClkPumpDisable</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__sysclk__clk__pump__funcs_1ga62b3f0a129cc361f11d39ec476d5d662">Cy_SysClk_ClkPumpSetDivider</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__sysclk__clk__pump__funcs_1gabe269c0ae478e843f436300e17d73f80">Cy_SysClk_ClkPumpEnable</ref></para></listitem></itemizedlist>
</para></listitem><listitem><para><ref kindref="member" refid="group__group__ctb__enums_1ggab17cdf969bc01f75e31685058296f747af379a95545f77d200a58cdadbe9e93fc">CY_CTB_CLK_PUMP_PERI</ref> - one of the peripheral clock dividers <ref kindref="compound" refid="group__group__sysclk__clk__peripheral">Peripherals Clock Dividers</ref>. Call the following functions to configure a Peri Clock divider as the pump clock:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__sysclk__clk__peripheral__funcs_1gac61ce8988b801b5edd829703e5c1ac3e">Cy_SysClk_PeriphDisableDivider</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__sysclk__clk__peripheral__funcs_1gacb66324e3191006f6d1cee8b29238fb2">Cy_SysClk_PeriphAssignDivider</ref> with the IP block set to PCLK_PASS_CLOCK_PUMP_PERI</para></listitem><listitem><para><ref kindref="member" refid="group__group__sysclk__clk__peripheral__funcs_1gac67931844e6ec06b5ff88f4f3119e127">Cy_SysClk_PeriphSetDivider</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__sysclk__clk__peripheral__funcs_1ga8c626f11b583d8296516bbcf4a22f98e">Cy_SysClk_PeriphEnableDivider</ref></para></listitem></itemizedlist>
</para></listitem><listitem><para><ref kindref="member" refid="group__group__ctb__enums_1ggab17cdf969bc01f75e31685058296f747a51fb3ec9c15089d1a402a134cec7366a">CY_CTB_CLK_PUMP_DSCLK</ref> - a PASS6Av2 deep sleep clock source ref group_sysanalog_dsclk. Call the following functions to configure the Deep Sleep Clock Source:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__sysanalog__functions_1ga22710413640667a25b48c5814e470810">Cy_SysAnalog_SetDeepSleepClock</ref> and either:</para></listitem><listitem><para><ref kindref="member" refid="group__group__sysanalog__functions_1gaa4f3c03d4ecb3f121c7d5b155e855bfa">Cy_SysAnalog_LpOscEnable</ref> or:</para></listitem><listitem><para><ref kindref="member" refid="group__group__sysclk__mf__funcs_1ga03fe8fcf693221a4ad150a2f3e9fcce4">Cy_SysClk_MfoEnable</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__sysclk__mf__funcs_1ga8d7b2b4f121d2cdee14f8ff4dc14fbd0">Cy_SysClk_ClkMfSetDivider</ref> (if needed)</para></listitem><listitem><para><ref kindref="member" refid="group__group__sysclk__mf__funcs_1ga27962298db8338018645f00e827a3f6d">Cy_SysClk_ClkMfEnable</ref></para></listitem></itemizedlist>
</para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>clkPump</parametername>
</parameternamelist>
<parameterdescription>
<para>Clock source selection (PumpClk, PeriClk or DSClk) for the pump. Select a value from <ref kindref="member" refid="group__group__ctb__enums_1gab17cdf969bc01f75e31685058296f747">cy_en_ctb_clk_pump_source_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Use<sp />a<sp />24<sp />Mhz<sp />clock<sp />from<sp />the<sp />SRSS<sp />for<sp />the<sp />charge<sp />pump<sp />clock.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Set<sp />the<sp />source<sp />to<sp />clock<sp />Path<sp />0,<sp />which<sp />has<sp />been<sp />configured<sp />for<sp />100<sp />MHz<sp />FLL.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__pump__funcs_1ga9fbaf9986d5413d5bb14db80f75ccc63">Cy_SysClk_ClkPumpSetSource</ref>(<ref kindref="member" refid="group__group__sysclk__clk__pump__enums_1ggab816c6625c6a20701956a4831526cddaa0ddc1f655b605f0939f6ea9635de3378">CY_SYSCLK_PUMP_IN_CLKPATH0</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Divide<sp />the<sp />100<sp />Mhz<sp />by<sp />4.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__pump__funcs_1ga62b3f0a129cc361f11d39ec476d5d662">Cy_SysClk_ClkPumpSetDivider</ref>(<ref kindref="member" refid="group__group__sysclk__clk__pump__enums_1gga8207bb47d9ecd521dc95375c1d46189eaadac3fd4b4235df3814fdf3c8468a273">CY_SYSCLK_PUMP_DIV_4</ref>);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__pump__funcs_1gabe269c0ae478e843f436300e17d73f80">Cy_SysClk_ClkPumpEnable</ref>();</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Select<sp />the<sp />source<sp />for<sp />the<sp />pump<sp />clock<sp />to<sp />be<sp />from<sp />the<sp />SRSS.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__aref_1gac13e18752439691a314b8f58082179b7">Cy_CTB_SetClkPumpSource</ref>(<ref kindref="member" refid="group__group__ctb__enums_1ggab17cdf969bc01f75e31685058296f747a328fdf373b541780577a1512901ea537">CY_CTB_CLK_PUMP_SRSS</ref>);</highlight></codeline>
</programlisting> </para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />Use<sp />a<sp />25<sp />Mhz<sp />clock<sp />from<sp />the<sp />PeriClk<sp />dividers<sp />for<sp />the<sp />charge<sp />pump<sp />clock.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />dividerNum<sp />=<sp />1UL;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />dividerValue<sp />=<sp />1UL;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Use<sp />the<sp />8-bit<sp />divider<sp />type<sp />and<sp />divider<sp />number<sp />1<sp />from<sp />the<sp />PeriClk,<sp />which<sp />has<sp />been<sp />configured<sp />for<sp />50<sp />MHz.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__peripheral__funcs_1gacb66324e3191006f6d1cee8b29238fb2">Cy_SysClk_PeriphAssignDivider</ref>(PCLK_PASS_CLOCK_PUMP_PERI,<sp /><ref kindref="member" refid="group__group__sysclk__clk__peripheral__enums_1gga06138349be16d91fd5d00ded2f4592b8ad2299deae6e36a070de5a60368b319ad">CY_SYSCLK_DIV_8_BIT</ref>,<sp />dividerNum);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Divide<sp />the<sp />50<sp />MHz<sp />PeriClk<sp />by<sp />2<sp />(dividerValue<sp />+<sp />1).<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__peripheral__funcs_1gac67931844e6ec06b5ff88f4f3119e127">Cy_SysClk_PeriphSetDivider</ref>(<ref kindref="member" refid="group__group__sysclk__clk__peripheral__enums_1gga06138349be16d91fd5d00ded2f4592b8ad2299deae6e36a070de5a60368b319ad">CY_SYSCLK_DIV_8_BIT</ref>,<sp />dividerNum,<sp />dividerValue);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__sysclk__clk__peripheral__funcs_1ga8c626f11b583d8296516bbcf4a22f98e">Cy_SysClk_PeriphEnableDivider</ref>(<ref kindref="member" refid="group__group__sysclk__clk__peripheral__enums_1gga06138349be16d91fd5d00ded2f4592b8ad2299deae6e36a070de5a60368b319ad">CY_SYSCLK_DIV_8_BIT</ref>,<sp />dividerNum);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Select<sp />the<sp />source<sp />for<sp />the<sp />pump<sp />clock<sp />to<sp />be<sp />from<sp />the<sp />PeriClk<sp />dividers.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__aref_1gac13e18752439691a314b8f58082179b7">Cy_CTB_SetClkPumpSource</ref>(<ref kindref="member" refid="group__group__ctb__enums_1ggab17cdf969bc01f75e31685058296f747af379a95545f77d200a58cdadbe9e93fc">CY_CTB_CLK_PUMP_PERI</ref>);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1488" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1469" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1119" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__aref_1gaa6518a1a10a15d0bc1715b71ea0bcf37" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTB_EnableRedirect</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_CTB_EnableRedirect</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Normally, the AREF IZTAT is routed to the CTB IZTAT and the AREF IPTAT is routed to the CTB IPTAT: </para>        </briefdescription>
        <detaileddescription>
<para><itemizedlist>
<listitem><para>CTB.IZTAT = AREF.IZTAT</para></listitem><listitem><para>CTB.IPTAT = AREF.IPTAT</para></listitem></itemizedlist>
</para><para>However, the AREF IPTAT can be redirected to the CTB IZTAT and the CTB IPTAT is off.</para><para><itemizedlist>
<listitem><para>CTB.IZTAT = AREF.IPTAT</para></listitem><listitem><para>CTB.IPTAT = HiZ</para></listitem></itemizedlist>
</para><para>The redirection applies to all opamps on the device and should be used when the IPTAT bias level is set to 100 nA (see <ref kindref="member" refid="group__group__ctb__functions__aref_1ga9aa45db21ccdca8c3770f22d781f1a44">Cy_CTB_SetIptatLevel</ref>).</para><para>When the CTB.IPTAT is HiZ, the CTB cannot compensate for the slope of the offset across temperature.</para><para><simplesect kind="return"><para>None</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para /></simplesect>
<programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />All<sp />CTBs<sp />on<sp />the<sp />device<sp />have<sp />been<sp />configured<sp />to<sp />use</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />a<sp />100<sp />nA<sp />IPTAT<sp />reference.<sp />Redirect<sp />the<sp />AREF<sp />IPTAT<sp />current<sp />to<sp />the<sp />CTB<sp />IZTAT</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />for<sp />proper<sp />functionality.<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />This<sp />is<sp />a<sp />low<sp />level<sp />function<sp />that<sp />only<sp />enables<sp />the<sp />current<sp />redirection.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />The<sp />preferred<sp />method<sp />is<sp />the<sp />Cy_CTB_SetCurrentMode()<sp />as<sp />it<sp />is<sp />a<sp />high</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />level<sp />function<sp />that<sp />configures<sp />both<sp />the<sp />current<sp />redirection<sp />and<sp />the</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp /><sp />*<sp />IPTAT<sp />reference.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__ctb__functions__aref_1gaa6518a1a10a15d0bc1715b71ea0bcf37">Cy_CTB_EnableRedirect</ref>();</highlight></codeline>
</programlisting></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1523" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1520" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1120" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ctb__functions__aref_1gaf6a14c3b143dc0699aaa001e01114355" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>__STATIC_INLINE void</type>
        <definition>__STATIC_INLINE void Cy_CTB_DisableRedirect</definition>
        <argsstring>(void)</argsstring>
        <name>Cy_CTB_DisableRedirect</name>
        <param>
          <type>void</type>
        </param>
        <briefdescription>
<para>Disable the redirection of the AREF IPTAT to the CTB IZTAT for all opamps on the device as enabled by <ref kindref="member" refid="group__group__ctb__functions__aref_1gaa6518a1a10a15d0bc1715b71ea0bcf37">Cy_CTB_EnableRedirect</ref>. </para>        </briefdescription>
        <detaileddescription>
<para><simplesect kind="return"><para>None </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1538" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" bodystart="1535" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_ctb.h" line="1121" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>This set of functions impacts all opamps on the chip. </para>    </briefdescription>
    <detaileddescription>
<para>Notice how some of these functions do not take a base address input. When calling <ref kindref="member" refid="group__group__ctb__functions__aref_1ga5a5dfa4b6150d4ba15f05a5c5ccdcaae">Cy_CTB_SetCurrentMode</ref> for a CTB instance on the device, it should be called for all other CTB instances as well. This is because there is only one IPTAT level (1 uA or 100 nA) chip wide. </para>    </detaileddescription>
  </compounddef>
</doxygen>