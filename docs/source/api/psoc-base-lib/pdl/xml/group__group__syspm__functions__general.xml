<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__syspm__functions__general" kind="group">
    <compoundname>group_syspm_functions_general</compoundname>
    <title>General</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__syspm__functions__general_1gaf98b0b378f526cf5542b946c8dcf74ee" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></type>
        <definition>cy_en_syspm_status_t Cy_SysPm_WriteVoltageBitForFlash</definition>
        <argsstring>(cy_en_syspm_flash_voltage_bit_t value)</argsstring>
        <name>Cy_SysPm_WriteVoltageBitForFlash</name>
        <param>
          <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1gab60333c26a4265095d1d082e5b153cb5">cy_en_syspm_flash_voltage_bit_t</ref></type>
          <declname>value</declname>
        </param>
        <briefdescription>
<para>Function that changes the voltage setting for flash. </para>        </briefdescription>
        <detaileddescription>
<para><verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;Call this function before system enters ULP mode. Call this function after the system enters LP mode.&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>value</parametername>
</parameternamelist>
<parameterdescription>
<para>Value to be set in the flash voltage control register. See <ref kindref="member" refid="group__group__syspm__data__enumerates_1gab60333c26a4265095d1d082e5b153cb5">cy_en_syspm_flash_voltage_bit_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><itemizedlist>
<listitem><para>CY_SYSPM_SUCCESS - The voltage is set.</para></listitem><listitem><para>CY_SYSPM_CANCELED - Operation was canceled. Call the function again until the function returns CY_SYSPM_SUCCESS. See <ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref>.</para></listitem></itemizedlist>
</para></simplesect>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />The<sp />system<sp />may<sp />continue<sp />operating<sp />while<sp />the<sp />voltage<sp />on<sp />Vccd<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp />discharges<sp />to<sp />the<sp />new<sp />voltage.<sp />The<sp />time<sp />it<sp />takes<sp />to<sp />reach<sp />the<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp />new<sp />voltage<sp />depends<sp />on<sp />operating<sp />conditions,<sp />including<sp />the<sp />load<sp />current</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp />on<sp />Vccd<sp />and<sp />external<sp />capacitor<sp />size.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />SRSS_PWR_BUCK_CTL<sp />=<sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />_CLR_SET_FLD32U((SRSS_PWR_BUCK_CTL),<sp />SRSS_PWR_BUCK_CTL_BUCK_OUT1_SEL,<sp />(uint32_t)<sp /><ref kindref="member" refid="group__group__syspm__data__enumerates_1gga3917bcfb5a7b4151ec1b0fcd95c37d5aa0009b3f9bef2d360c1b221cd8c801650">CY_SYSPM_BUCK_OUT1_VOLTAGE_ULP</ref>);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2986" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_syspm.c" bodystart="2921" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_syspm.h" line="1820" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__syspm__functions__general_1gad4960d176e56bce8631015843a143e1b" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SysPm_SaveRegisters</definition>
        <argsstring>(cy_stc_syspm_backup_regs_t *regs)</argsstring>
        <name>Cy_SysPm_SaveRegisters</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__syspm__backup__regs__t">cy_stc_syspm_backup_regs_t</ref> *</type>
          <declname>regs</declname>
        </param>
        <briefdescription>
<para>Saves non-retained UDB registers and the slow and fast clock dividers before system entering system Deep Sleep. </para>        </briefdescription>
        <detaileddescription>
<para>Must be called if programmable logic or function are implemented in the UDB array.</para><para>Cypress ID #280370, #1451.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>regs</parametername>
</parameternamelist>
<parameterdescription>
<para>The structure where the registers are saved.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />There<sp />is<sp />a<sp />need<sp />to<sp />save<sp />the<sp />non-retained<sp />UDB<sp />registers<sp />while<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp /><sp />in<sp />system<sp />Deep<sp />Sleep<sp />routine.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">static</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcy__stc__syspm__backup__regs__t">cy_stc_syspm_backup_regs_t</ref><sp />regs;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Save<sp />non-retained<sp />UDB<sp />registers<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syspm__functions__general_1gad4960d176e56bce8631015843a143e1b">Cy_SysPm_SaveRegisters</ref>(&amp;regs);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />SCB_SCR<sp />|=<sp />SCB_SCR_SLEEPDEEP_Msk;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />__WFI();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Restore<sp />non-retained<sp />UDB<sp />registers<sp />after<sp />system<sp />Deep<sp />Sleep<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syspm__functions__general_1ga300fc24bbd19d12771dc2dd472658ab7">Cy_SysPm_RestoreRegisters</ref>(&amp;regs);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="3028" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_syspm.c" bodystart="3008" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_syspm.h" line="1822" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__syspm__functions__general_1ga300fc24bbd19d12771dc2dd472658ab7" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type>void</type>
        <definition>void Cy_SysPm_RestoreRegisters</definition>
        <argsstring>(cy_stc_syspm_backup_regs_t const *regs)</argsstring>
        <name>Cy_SysPm_RestoreRegisters</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__syspm__backup__regs__t">cy_stc_syspm_backup_regs_t</ref> const *</type>
          <declname>regs</declname>
        </param>
        <briefdescription>
<para>Restores non-retained UDB registers and the slow and fast clock dividers before system entering system Deep Sleep. </para>        </briefdescription>
        <detaileddescription>
<para>Must be called if programmable logic or function are implemented in the UDB array.</para><para>Cypress ID #280370, #1451.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>regs</parametername>
</parameternamelist>
<parameterdescription>
<para>The structure with data stored (using <ref kindref="member" refid="group__group__syspm__functions__general_1gad4960d176e56bce8631015843a143e1b">Cy_SysPm_SaveRegisters()</ref>) into the required registers after Deep Sleep.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="par"><title>Function Usage</title><para><programlisting><codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Scenario:<sp />There<sp />is<sp />a<sp />need<sp />to<sp />save<sp />the<sp />non-retained<sp />UDB<sp />registers<sp />while<sp /></highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*<sp /><sp />in<sp />system<sp />Deep<sp />Sleep<sp />routine.</highlight></codeline>
<codeline><highlight class="comment"><sp /><sp /><sp /><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">static</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcy__stc__syspm__backup__regs__t">cy_stc_syspm_backup_regs_t</ref><sp />regs;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Save<sp />non-retained<sp />UDB<sp />registers<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syspm__functions__general_1gad4960d176e56bce8631015843a143e1b">Cy_SysPm_SaveRegisters</ref>(&amp;regs);</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />SCB_SCR<sp />|=<sp />SCB_SCR_SLEEPDEEP_Msk;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />__WFI();</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Restore<sp />non-retained<sp />UDB<sp />registers<sp />after<sp />system<sp />Deep<sp />Sleep<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__syspm__functions__general_1ga300fc24bbd19d12771dc2dd472658ab7">Cy_SysPm_RestoreRegisters</ref>(&amp;regs);</highlight></codeline>
</programlisting></para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="3070" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_syspm.c" bodystart="3050" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_syspm.h" line="1823" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>