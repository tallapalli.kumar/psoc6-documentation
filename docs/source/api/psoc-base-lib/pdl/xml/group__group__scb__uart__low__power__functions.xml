<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__scb__uart__low__power__functions" kind="group">
    <compoundname>group_scb_uart_low_power_functions</compoundname>
    <title>Low Power Callbacks</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__scb__uart__low__power__functions_1gaa26ee4aa1c2ac9fb98daddbfe6e151aa" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></type>
        <definition>cy_en_syspm_status_t Cy_SCB_UART_DeepSleepCallback</definition>
        <argsstring>(cy_stc_syspm_callback_params_t *callbackParams, cy_en_syspm_callback_mode_t mode)</argsstring>
        <name>Cy_SCB_UART_DeepSleepCallback</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref> *</type>
          <declname>callbackParams</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>This function handles the transition of the SCB UART into and out of Deep Sleep mode. </para>        </briefdescription>
        <detaileddescription>
<para>It prevents the device from entering Deep Sleep mode if the UART is transmitting data or has any data in the RX FIFO. If the UART is ready to enter Deep Sleep mode, it is disabled. The UART is enabled when the device fails to enter Deep Sleep mode or it is awakened from Deep Sleep mode. While the UART is disabled, it stops driving the outputs and ignores the inputs. Any incoming data is ignored. Refer to section <ref kindref="member" refid="group__group__scb__uart_1group_scb_uart_lp">Low Power Support</ref> for more information about UART pins when SCB disabled.</para><para>This function must be called during execution of <ref kindref="member" refid="group__group__syspm__functions__power_1ga5150c28fe4d2626720c1fbf74b3111ca">Cy_SysPm_CpuEnterDeepSleep</ref>, to do it, register this function as a callback before calling <ref kindref="member" refid="group__group__syspm__functions__power_1ga5150c28fe4d2626720c1fbf74b3111ca">Cy_SysPm_CpuEnterDeepSleep</ref> : specify <ref kindref="member" refid="group__group__syspm__data__enumerates_1gga8c2960c0164ead1cfa86e7d6846b6ff0abc51d74deff0ceea4304b01b2d57bd9d">CY_SYSPM_DEEPSLEEP</ref> as the callback type and call <ref kindref="member" refid="group__group__syspm__functions__callback_1ga0d58b00c8dc764a6371590f70e2f73c7">Cy_SysPm_RegisterCallback</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>callbackParams</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the callback parameters structure <ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>Callback mode, see <ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="374" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="301" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="657" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__uart__low__power__functions_1gad72ab6609be39108f73aea440d229bce" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref></type>
        <definition>cy_en_syspm_status_t Cy_SCB_UART_HibernateCallback</definition>
        <argsstring>(cy_stc_syspm_callback_params_t *callbackParams, cy_en_syspm_callback_mode_t mode)</argsstring>
        <name>Cy_SCB_UART_HibernateCallback</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref> *</type>
          <declname>callbackParams</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></type>
          <declname>mode</declname>
        </param>
        <briefdescription>
<para>This function handles the transition of the SCB UART into Hibernate mode. </para>        </briefdescription>
        <detaileddescription>
<para>It prevents the device from entering Hibernate mode if the UART is transmitting data or has any data in the RX FIFO. If the UART is ready to enter Hibernate mode, it is disabled. If the device fails to enter Hibernate mode, the UART is enabled. While the UART is disabled, it stops driving the outputs and ignores the inputs. Any incoming data is ignored. Refer to section <ref kindref="member" refid="group__group__scb__uart_1group_scb_uart_lp">Low Power Support</ref> for more information about UART pins when SCB disabled.</para><para>This function must be called during execution of <ref kindref="member" refid="group__group__syspm__functions__power_1gae97647a28c370674ba57d451d21d1c51">Cy_SysPm_SystemEnterHibernate</ref>. To do it, register this function as a callback before calling <ref kindref="member" refid="group__group__syspm__functions__power_1gae97647a28c370674ba57d451d21d1c51">Cy_SysPm_SystemEnterHibernate</ref> : specify <ref kindref="member" refid="group__group__syspm__data__enumerates_1gga8c2960c0164ead1cfa86e7d6846b6ff0a613e2f83e3ab88e3569cf34ff0fa5912">CY_SYSPM_HIBERNATE</ref> as the callback type and call <ref kindref="member" refid="group__group__syspm__functions__callback_1ga0d58b00c8dc764a6371590f70e2f73c7">Cy_SysPm_RegisterCallback</ref>.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>callbackParams</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the callback parameters structure <ref kindref="compound" refid="structcy__stc__syspm__callback__params__t">cy_stc_syspm_callback_params_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>mode</parametername>
</parameternamelist>
<parameterdescription>
<para>Callback mode, see <ref kindref="member" refid="group__group__syspm__data__enumerates_1gae06cd8869fe61d709ad6145ca9f3cd63">cy_en_syspm_callback_mode_t</ref></para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__syspm__data__enumerates_1ga601b1cb722cb091133caf33d8ab235ca">cy_en_syspm_status_t</ref> </para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="409" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_uart.c" bodystart="406" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_uart.h" line="658" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>