<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__scb__i2c__master__low__level__functions" kind="group">
    <compoundname>group_scb_i2c_master_low_level_functions</compoundname>
    <title>Master Low-Level</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__master__low__level__functions_1ga15fa1a79148b846fd348ce39094876f0" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></type>
        <definition>cy_en_scb_i2c_status_t Cy_SCB_I2C_MasterSendStart</definition>
        <argsstring>(CySCB_Type *base, uint32_t address, cy_en_scb_i2c_direction_t bitRnW, uint32_t timeoutMs, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_MasterSendStart</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>address</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__scb__i2c__enums_1ga6a168a5e662ce781920027bebbab5d8d">cy_en_scb_i2c_direction_t</ref></type>
          <declname>bitRnW</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>timeoutMs</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Generates a Start condition and sends a slave address with the Read/Write bit. </para>        </briefdescription>
        <detaileddescription>
<para>This function is blocking. It does not return until the Start condition and address byte are sent and a ACK/NAK is received, or an error or timeout occurs.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>address</parametername>
</parameternamelist>
<parameterdescription>
<para>7 bit right justified slave address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>bitRnW</parametername>
</parameternamelist>
<parameterdescription>
<para>This sets the value of the Read/Write bit in the address, thus defining the direction of the following transfer. See <ref kindref="member" refid="group__group__scb__i2c__enums_1ga6a168a5e662ce781920027bebbab5d8d">cy_en_scb_i2c_direction_t</ref> for the set of constants.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeoutMs</parametername>
</parameternamelist>
<parameterdescription>
<para>Defines in milliseconds the time for which this function can block. If that time expires, the function returns. If a zero is passed, the function waits forever for the action to complete. If a timeout occurs, the SCB block is reset. Note The maximum value is UINT32_MAX/1000.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;After a read transaction is initiated and the slave ACKs the address, at least one byte must be read before completing the transaction or changing its direction. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1792" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1743" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="696" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__master__low__level__functions_1ga2177b4fb74cf33e57e6fa59d04bdfa7a" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></type>
        <definition>cy_en_scb_i2c_status_t Cy_SCB_I2C_MasterSendReStart</definition>
        <argsstring>(CySCB_Type *base, uint32_t address, cy_en_scb_i2c_direction_t bitRnW, uint32_t timeoutMs, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_MasterSendReStart</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>address</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__scb__i2c__enums_1ga6a168a5e662ce781920027bebbab5d8d">cy_en_scb_i2c_direction_t</ref></type>
          <declname>bitRnW</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>timeoutMs</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Generates a ReStart condition and sends a slave address with the Read/Write bit. </para>        </briefdescription>
        <detaileddescription>
<para>This function is blocking. It does not return until the ReStart condition and address byte are sent and an ACK/NAK is received, or an error or timeout occurs.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>address</parametername>
</parameternamelist>
<parameterdescription>
<para>A 7-bit right-justified slave address.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>bitRnW</parametername>
</parameternamelist>
<parameterdescription>
<para>This sets the value of the Read/Write bit in the address, thus defining the direction of the following transfer. See <ref kindref="member" refid="group__group__scb__i2c__enums_1ga6a168a5e662ce781920027bebbab5d8d">cy_en_scb_i2c_direction_t</ref> for the set of constants.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeoutMs</parametername>
</parameternamelist>
<parameterdescription>
<para>Defines in milliseconds the time for which this function can block. If that time expires, the function returns. If a zero is passed, the function waits forever for the action to complete. If a timeout occurs, the SCB block is reset. Note The maximum value is UINT32_MAX/1000.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;A successful transaction must be initiated by &lt;a href="group__group__scb__i2c__master__low__level__functions.html#group__group__scb__i2c__master__low__level__functions_1ga15fa1a79148b846fd348ce39094876f0"&gt;Cy_SCB_I2C_MasterSendStart&lt;/a&gt; before calling this function. If this condition is not met, this function does nothing and returns &lt;a href="group__group__scb__i2c__enums.html#group__group__scb__i2c__enums_1ggaf621eb0719ad9ad7178d02268575b247a87d64b12d320a5119189d10f4d4adf18"&gt;CY_SCB_I2C_MASTER_NOT_READY&lt;/a&gt;.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;After a read transaction is initiated and the slave ACKs the address, at least one byte must be read before completing the transaction or changing its direction. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1902" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1840" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="698" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__master__low__level__functions_1ga4b098cd781eaa5cd287ae33e05c86a0d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></type>
        <definition>cy_en_scb_i2c_status_t Cy_SCB_I2C_MasterSendStop</definition>
        <argsstring>(CySCB_Type *base, uint32_t timeoutMs, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_MasterSendStop</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>timeoutMs</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Generates a Stop condition to complete the current transaction. </para>        </briefdescription>
        <detaileddescription>
<para>This function is blocking. It does not return until the Stop condition is generated, or an error or timeout occurs.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeoutMs</parametername>
</parameternamelist>
<parameterdescription>
<para>Defines in milliseconds the time for which this function can block. If that time expires, the function returns. If a zero is passed, the function waits forever for the action to complete. If a timeout occurs, the SCB block is reset. Note The maximum value is UINT32_MAX/1000.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;A successful transaction must be initiated by &lt;a href="group__group__scb__i2c__master__low__level__functions.html#group__group__scb__i2c__master__low__level__functions_1ga15fa1a79148b846fd348ce39094876f0"&gt;Cy_SCB_I2C_MasterSendStart&lt;/a&gt; or &lt;a href="group__group__scb__i2c__master__low__level__functions.html#group__group__scb__i2c__master__low__level__functions_1ga2177b4fb74cf33e57e6fa59d04bdfa7a"&gt;Cy_SCB_I2C_MasterSendReStart&lt;/a&gt; before calling this function. If this condition is not met, this function does nothing and returns. &lt;a href="group__group__scb__i2c__enums.html#group__group__scb__i2c__enums_1ggaf621eb0719ad9ad7178d02268575b247a87d64b12d320a5119189d10f4d4adf18"&gt;CY_SCB_I2C_MASTER_NOT_READY&lt;/a&gt;.&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Even after the slave NAKs the address, this function must be called to complete the transaction. &lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1969" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="1941" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="700" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__master__low__level__functions_1ga0679a2b27cf3be4d1a6cd20246688fef" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></type>
        <definition>cy_en_scb_i2c_status_t Cy_SCB_I2C_MasterReadByte</definition>
        <argsstring>(CySCB_Type *base, cy_en_scb_i2c_command_t ackNack, uint8_t *byte, uint32_t timeoutMs, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_MasterReadByte</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__scb__i2c__enums_1ga4600410051f7febf79d167ffbf85d7ef">cy_en_scb_i2c_command_t</ref></type>
          <declname>ackNack</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>byte</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>timeoutMs</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Reads one byte from a slave and generates an ACK or prepares to generate a NAK. </para>        </briefdescription>
        <detaileddescription>
<para>The NAK will be generated before a Stop or ReStart condition by <ref kindref="member" refid="group__group__scb__i2c__master__low__level__functions_1ga4b098cd781eaa5cd287ae33e05c86a0d">Cy_SCB_I2C_MasterSendStop</ref> or <ref kindref="member" refid="group__group__scb__i2c__master__low__level__functions_1ga2177b4fb74cf33e57e6fa59d04bdfa7a">Cy_SCB_I2C_MasterSendReStart</ref> function appropriately. This function is blocking. It does not return until a byte is received, or an error or timeout occurs.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>ackNack</parametername>
</parameternamelist>
<parameterdescription>
<para>A response to a received byte. See <ref kindref="member" refid="group__group__scb__i2c__enums_1ga4600410051f7febf79d167ffbf85d7ef">cy_en_scb_i2c_command_t</ref> for the set of constants.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>byte</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the location to store the Read byte.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeoutMs</parametername>
</parameternamelist>
<parameterdescription>
<para>Defines in milliseconds the time for which this function can block. If that time expires, the function returns. If a zero is passed, the function waits forever for the action to complete. If a timeout occurs, the SCB block is reset. Note The maximum value is UINT32_MAX/1000.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;A successful transaction must be initiated by &lt;a href="group__group__scb__i2c__master__low__level__functions.html#group__group__scb__i2c__master__low__level__functions_1ga15fa1a79148b846fd348ce39094876f0"&gt;Cy_SCB_I2C_MasterSendStart&lt;/a&gt; or &lt;a href="group__group__scb__i2c__master__low__level__functions.html#group__group__scb__i2c__master__low__level__functions_1ga2177b4fb74cf33e57e6fa59d04bdfa7a"&gt;Cy_SCB_I2C_MasterSendReStart&lt;/a&gt; before calling this function. If this condition is not met, this function does nothing and returns &lt;a href="group__group__scb__i2c__enums.html#group__group__scb__i2c__enums_1ggaf621eb0719ad9ad7178d02268575b247a87d64b12d320a5119189d10f4d4adf18"&gt;CY_SCB_I2C_MASTER_NOT_READY&lt;/a&gt;. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2068" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="2015" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="701" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__scb__i2c__master__low__level__functions_1ga7eb0684e4d362668f0637d1e82405551" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></type>
        <definition>cy_en_scb_i2c_status_t Cy_SCB_I2C_MasterWriteByte</definition>
        <argsstring>(CySCB_Type *base, uint8_t byte, uint32_t timeoutMs, cy_stc_scb_i2c_context_t *context)</argsstring>
        <name>Cy_SCB_I2C_MasterWriteByte</name>
        <param>
          <type><ref kindref="compound" refid="struct_cy_s_c_b___type">CySCB_Type</ref> *</type>
          <declname>base</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>byte</declname>
        </param>
        <param>
          <type>uint32_t</type>
          <declname>timeoutMs</declname>
        </param>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> *</type>
          <declname>context</declname>
        </param>
        <briefdescription>
<para>Sends one byte to a slave. </para>        </briefdescription>
        <detaileddescription>
<para>This function is blocking. It does not return until a byte is transmitted, or an error or timeout occurs.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>base</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the I2C SCB instance.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>byte</parametername>
</parameternamelist>
<parameterdescription>
<para>The byte to write to a slave.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>timeoutMs</parametername>
</parameternamelist>
<parameterdescription>
<para>Defines in milliseconds the time for which this function can block. If that time expires, the function returns. If a zero is passed, the function waits forever for the action to complete. If a timeout occurs, the SCB block is reset. Note The maximum value is UINT32_MAX/1000.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>context</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the context structure <ref kindref="compound" refid="structcy__stc__scb__i2c__context__t">cy_stc_scb_i2c_context_t</ref> allocated by the user. The structure is used during the I2C operation for internal configuration and data retention. The user must not modify anything in this structure.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para><ref kindref="member" refid="group__group__scb__i2c__enums_1gaf621eb0719ad9ad7178d02268575b247">cy_en_scb_i2c_status_t</ref></para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;div class="admonition note"&gt;&lt;p class="admonition-title"&gt;note&lt;/p&gt;&lt;p&gt;&lt;p&gt;A successful transaction must be initiated by &lt;a href="group__group__scb__i2c__master__low__level__functions.html#group__group__scb__i2c__master__low__level__functions_1ga15fa1a79148b846fd348ce39094876f0"&gt;Cy_SCB_I2C_MasterSendStart&lt;/a&gt; or &lt;a href="group__group__scb__i2c__master__low__level__functions.html#group__group__scb__i2c__master__low__level__functions_1ga2177b4fb74cf33e57e6fa59d04bdfa7a"&gt;Cy_SCB_I2C_MasterSendReStart&lt;/a&gt; before calling this function. If this condition is not met, this function does nothing and returns &lt;a href="group__group__scb__i2c__enums.html#group__group__scb__i2c__enums_1ggaf621eb0719ad9ad7178d02268575b247a87d64b12d320a5119189d10f4d4adf18"&gt;CY_SCB_I2C_MASTER_NOT_READY&lt;/a&gt;. &lt;/p&gt;&lt;/p&gt;&lt;/div&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="2135" bodyfile="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/source/cy_scb_i2c.c" bodystart="2107" column="1" file="/var/tmp/gitlab-runner1/builds/7e5c9300/0/repo/psoc6pdl/drivers/include/cy_scb_i2c.h" line="703" />
      </memberdef>
      </sectiondef>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>