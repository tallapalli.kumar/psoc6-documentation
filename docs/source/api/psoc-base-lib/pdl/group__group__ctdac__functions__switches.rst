=========================
Switch Control Functions
=========================

.. doxygengroup:: group_ctdac_functions_switches
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: