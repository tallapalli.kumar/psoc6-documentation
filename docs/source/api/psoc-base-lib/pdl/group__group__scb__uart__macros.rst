=======
Marcos
=======

.. toctree::

   group__group__scb__uart__macros__irda__lp__ovs.rst
   group__group__scb__uart__macros__rx__fifo__status.rst
   group__group__scb__uart__macros__tx__fifo__status.rst
   group__group__scb__uart__macros__receive__status.rst
   group__group__scb__uart__macros__transmit__status.rst
   group__group__scb__uart__macros__callback__events.rst

.. doxygengroup:: group_scb_uart_macros 
   :project: pdl 
   :members: