=======================
Function return values
=======================

.. doxygengroup:: group_sysclk_returns
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: