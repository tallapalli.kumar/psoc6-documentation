==========
Functions
==========


.. toctree::
   
   group__group__ctdac__functions__init.rst
   group__group__ctdac__functions__basic.rst
   group__group__ctdac__functions__switches.rst
   group__group__ctdac__functions__interrupts.rst
   group__group__ctdac__functions__syspm__callback.rst
   



.. doxygengroup:: group_ctdac_functions
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: