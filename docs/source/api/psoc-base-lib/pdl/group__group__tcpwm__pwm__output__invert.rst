==================
PWM output invert
==================

.. doxygengroup:: group_tcpwm_pwm_output_invert
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: