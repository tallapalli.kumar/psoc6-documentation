=================
Enumerated Types
=================


.. doxygengroup:: group_ctb_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: