=================
Global Variables
=================

.. doxygengroup:: group_sysanalog_globals
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: