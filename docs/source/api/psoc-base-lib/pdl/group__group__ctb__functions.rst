==========
Functions
==========



.. toctree::
   
   group__group__ctb__functions__init.rst
   group__group__ctb__functions__basic.rst
   group__group__ctb__functions__comparator.rst
   group__group__ctb__functions__sample__hold.rst
   group__group__ctb__functions__interrupts.rst
   group__group__ctb__functions__switches.rst
   group__group__ctb__functions__trim.rst
   group__group__ctb__functions__aref.rst
   
   
   
   
   
   

.. doxygengroup:: group_ctb_functions
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: