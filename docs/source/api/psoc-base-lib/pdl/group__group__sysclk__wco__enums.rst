================
Enumerated Types
================

.. doxygengroup:: group_sysclk_wco_enums
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   