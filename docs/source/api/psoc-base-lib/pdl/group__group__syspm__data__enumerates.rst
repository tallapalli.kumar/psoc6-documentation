=================
Enumerated Types
=================

.. doxygengroup:: group_syspm_data_enumerates
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: