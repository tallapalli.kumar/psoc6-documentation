=================
Global Variables
=================

.. doxygengroup:: group_ctdac_globals
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: