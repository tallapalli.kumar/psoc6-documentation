==========================
Peripherals Clock Dividers
==========================

.. doxygengroup:: group_sysclk_clk_peripheral
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members:
   

API Reference
==============

.. toctree::

   group__group__sysclk__clk__peripheral__funcs.rst
   group__group__sysclk__clk__peripheral__enums.rst