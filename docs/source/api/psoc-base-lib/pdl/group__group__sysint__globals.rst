=================
Global variables
=================

.. doxygengroup:: group_sysint_globals
   :project: pdl
   :members:
   :protected-members:
   :private-members:
   :undoc-members: