====================
Introducing PSoC 6
====================

|image1|

INCREASING DEMANDS FOR THE INTERNET OF THINGS
=============================================

The IoT is exploding, with more that 30 billion devices projected to
be in service by 2020. At the heart of every IoT device lies a
mixed-signal embedded system that is taking on an increased amount of
data processing, fueled by cloud connectivity. Devices such as door
locks, factory machinery, and wearables are becoming smart — sensing,
connecting, learning and responding — to make life easier. With
connectivity, security becomes critical to protect users from
malicious activity. These next-generation IoT devices require
increased processing and security without incurring power and cost
penalties.

.. rst-class:: intro

+--------------------------------------+--------------------------------------+
|                                      || **Increased Data Processing**       |
| |image2|                             || Today’s IoT devices are collecting  |
|                                      |  and processing more data than ever. |
|                                      |  Even wearable devices can contain   |
|                                      |  more than a dozen sensors, including|
|                                      |  biometric, environmental, and motion|
|                                      |  sensors. Sensor data is collected   |
|                                      |  and processed in real-time requiring|
|                                      |  a processor capable of sensor       |
|                                      |  aggregation.                        |
+--------------------------------------+--------------------------------------+
|| **Flexible Connectivity**           | |image3|                             |
|| IoT devices connect to the cloud via|                                      |
|  various wired and wireless protocols|                                      |
|  including USB, Bluetooth Low Energy |                                      |
|  (BLE), Wi-Fi, and LPWAN. Having the |                                      |
|  flexibility to connect via different|                                      |
|  connectivity options is essential.  |                                      |
+--------------------------------------+--------------------------------------+
| |image4|                             || **Security**                        |
|                                      || Security and privacy are significant|
|                                      |  IoT concerns. Users must be confide\|
|                                      |  is always protected while their IoT |
|                                      |  devices are connected to the        |
|                                      |  Internet. Similarly, IoT developers |
|                                      |  must be able to ensure the authenti\|
|                                      |  city of their products to eliminate |
|                                      |  counterfeits and perform secure     |
|                                      |  over-the-air updates to keep their  |
|                                      |  products current.                   |
+--------------------------------------+--------------------------------------+
|| **Extended Battery Life**           | |image5|                             |
|| From wearables to remote sensors,   |                                      |
|  more and more IoT devices are runni |                                      |
|  ng on batteries. Extending the time |                                      |
|  between battery charges is a key    |                                      |
|  requirement for battery-powered IoT |                                      |
|  devices.                            |                                      |
+--------------------------------------+--------------------------------------+

PSoC\ :sup:`®` 6 MICROCONTROLLERS
===================================
Purpose-Built for the Internet of Things
-----------------------------------------

Cypress’ PSoC 6 MCU architecture is purpose-built for the IoT,
filling the gap between expensive, power-hungry applications
processors and low-performance MCUs. The ultra-low power PSoC 6 MCU
architecture offers the processing performance needed by IoT devices.
Security is built-in, enabling a single-chip solution. PSoC 6 enables
engineers to uniquely create innovative, next-generation IoT devices
leveraging the unique PSoC fabric with its easy-to-use,
software-defined peripherals.

|image6|

.. rst-class:: intro

+-----------+---------------------------------------------------------------------------+
||image7|   || **LOWEST POWER**                                                         |
|           || IoT devices are often portable, making battery life a critical factor.   |
|           |  The PSoC 6 MCU architecture is built on a cutting-edge, ultra-low-power, |
|           |  40-nm process technology with a dual Arm :sup:`®` Cortex :sup:`®`-M core |
|           |  architecture. Active power consumption is as low as 22- μA/MHz for the M4|
|           |  core, and 15-μA/MHz for the M0+ core. PSoC 6 delivers extended battery   |
|           |  life without sacrificing performance.                                    |
+-----------+---------------------------------------------------------------------------+
||image8|   || **MOST FLEXIBILITY**                                                     |
|           || The rapid growth of the IoT is sparking a need for innovation in IoT prod|
|           |  ucts. The PSoC 6 MCU architecture’s best-in-class flexibility enables the|
|           |  addition of new features, and addresses the need for unique IoT products |
|           |  with multiple connectivity options, such as USB and BLE. PSoC 6 also     |
|           |  offers software-defined peripherals to create custom analog and digital  |
|           |  circuits, and the industry’s best capacitive- sensing solution,          |
|           |  CapSense :sup:`®`. In addition, a flexible dual-core architecture is used|
|           |  to optimize for system power consumption and performance. The            |
|           |  possibilities are endless.                                               |
+-----------+---------------------------------------------------------------------------+
||image9|   || **BUILT-IN SECURITY**                                                    |
|           || With a growing number of devices connecting to the IoT, security must be |
|           |  established between hardware, cloud applications, and servers, and final |
|           |  ly users and services. The PSoC 6 MCU architecture supports multiple,    |
|           |  simultaneous secure environments without the need for external memories  |
|           |  or secure elements, and offers scalable secure memory for multiple,      |
|           |  independent user-defined security policies, preventing your IoT device   |
|           |  from becoming a security liability. PSoC 6 provides you with a new       |
|           |  standard for IoT security.                                               |
+-----------+---------------------------------------------------------------------------+

UNMATCHED SOLUTIONS FOR THE INTERNET OF THINGS
==============================================
The Oura Ring
-------------

|image10|

| The Oura Ring is the world’s most-advanced wearable that helps you
  achieve restorative sleep.

| This innovation is delivered through PSoC 6 MCUs, enabling the
  following features:

.. rst-class:: intro

+------------------------------+------------------------------+-------------------------------+
| **SMALL FORM FACTOR**        |                              | **EXTENDED BATTERY LIFE**     | 
|                              | |image11|                    |                               |
| Integration of several funct\|                              | PSoC 6’s ultra -low power was |
| ions into a single PSoc 6 MCU|                              | able to extend battery life   |
| helped reduce the size of the|                              | to seven days, 3X longer than |
| ring over the prior          |                              | the prior generation ring.    |
| generation by 5%.            |                              |                               |
+------------------------------+                              +-------------------------------+
| **DATA STORAGE**             |                              | **SENSOR FUSION**             |
|                              |                              |                               |
| PSoC 6’s on-chip flash memory|                              | PSoC 6, as the host processor,|
| enables storage of up to six |                              | connects different biometric  |
| weeks of user data.          |                              | sensors in the ring to collect|
|                              |                              | and process data.             |
+------------------------------+------------------------------+-------------------------------+
| **BLUETOOTH LOW ENERGY**     | |image12|                    | **DATA PROCESSING**           |
|                              |                              |                               |
| The built-in BLE wireless    |                              | The dual Cortex-M4 and Cortex\|
| connectivity in PSoC 6 provid|                              | -M0+ cores in PSoC 6 enabled  |
| es communication to the      |                              | 10x more processing capability|
| mobile app device.           |                              | vs. the prior-generation ring.|
+------------------------------+------------------------------+-------------------------------+

   "The Oura ring required long-lasting battery life, increased
   processing capacity, and the use of advanced sensors, all in a
   stylish and comfortable form factor. Choosing Cypress as our partner
   helped us meet these requirements and redefine what is possible for
   wearable health products.“

Petteri Lahtela

CEO and Co-founder at Oura Health


PSoC 6 MCU PORTFOLIO
====================

|image13|

PSoC 6 DUAL-CORE MCU ARCHITECTURE
---------------------------------

|image14|

.. rst-class:: intro-bgclr

+-----------------------------------------------------+-------------------------------------+
|| **WEARABLE EXAMPLE**                               | |image15|                           |
|| Modern wearables incorporate a growing number of   |                                     |
|  functions and capabilities while maximizing battery|                                     |
|  life. Functions such as sleek user interfaces with |                                     |
|  sliders and proximity sensors, voice command       |                                     |
|  support, seamless BLE connectivity, display        |                                     |
|  driving, and process-intensive sensor aggregation  |                                     |
|  can all be achieved in a single PSoC 6 MCU without |                                     |
|  sacrificing battery life.                          |                                     |
+-----------------------------------------------------+                                     |
| |image16|                                           |                                     |
+-----------------------------------------------------+-------------------------------------+

KEY FEATURES
------------

.. rst-class:: intro

+----------------+--------------------------------------------+----------------+--------------------------------------------+
| |image17|      | **LOW POWER**                              | |image18|      | **CAPSENSE**                               |
|                |                                            |                |                                            |
|                | PSoC 6 was designed for ultra- low power   |                | PSoC 6 leverages Cypress’ industry-leading |
|                | consumption. Starting with the dual-core   |                | CapSense solution for capacitive sensing   | 
|                | architecture, the Cortex- M4 core can be   |                | with state-of-the-art noise immunity and   |
|                | put to sleep to conserve power, while moni\|                | water rejection used for applications      |
|                | toring functions can be managed by the Cort|                | including touch buttons and sliders,       |
|                | ex-M0+ core. Dynamic voltage and frequency |                | proximity detection and liquid-level       |
|                | scaling can be controlled by the user to   |                | sensing.                                   |
|                | optimize power for different operating envi|                |                                            |
|                | ronments. With multiple sleep and hibernate|                |                                            |
|                | modes, users can fine-tune their power     |                |                                            |
|                | profiles.                                  |                |                                            |
+----------------+--------------------------------------------+----------------+--------------------------------------------+
| |image19|      | **BUILT-IN SECURITY**                      | |image20|      | **SOFTWARE-DEFINED PERIPHERALS**           |
|                |                                            |                |                                            |
|                | PSoC 6 is designed to provide a hardware-  |                | Digital Analog Cypress’ software-defined   |
|                | based root of trust for secure             |                | peripherals can be configured using pre-   |
|                | applications. A hardware-based root of     |                | built library functions or customized using|
|                | trust provides extra protection against    |                | the programmable architecture inside of    |
|                | unauthorized access to keys. In addition,  |                | PSoC 6 devices:                            |
|                | hardware accelerated cryptography and true |                |                                            |
|                | random-number generation are provided,     |                | Programmable analog blocks are composed of | 
|                | which are needed for secure boot,          |                | opamps, comparators, ADCs, and DACs,       |
|                | authentication, secure over-the-air updates|                | enabling complex analog signal flows.      |
|                | and other functions.                       |                |                                            |
+----------------+--------------------------------------------+                | Programmable digital blocks are composed of|
| |image21|      | **CONNECTIVITY**                           |                | Universal Digital Blocks (UDBs), Serial    |
|                |                                            |                | Communication Blocks (SCBs), and Timer /   |
|                | PSoC 6 supports multiple wired and wireless|                | Counter / Pulse Width Modulators (TCPWMs). |
|                | connectivity options including Bluetooth   |                | These blocks can be configured to set-up   |
|                | Low Energy, Wi-Fi, and USB.                |                | custom digital interfaces, state machines, |
|                |                                            |                | and custom logic functions.                |
+----------------+--------------------------------------------+----------------+--------------------------------------------+


.. rst-class:: intro-bgclr

+------------------------------------------------------+---------------------------------------+
|| **VIDEO DOORBELL EXAMPLE**                          | |image22|                             |
|| For a smart home application like a video doorbell, |                                       |
|  PSoC 6, with its dual Cortex- M4 and Cortex-M0+     |                                       |
|  cores, can transmit real-time video as well as      |                                       |
|  incorporate proximity detection, capture and play   |                                       |
|  audio, and interface to a Cypress fingerprint sensor|                                       |
|  module or Cypress WICED Wi-Fi chipset. The PSoC 6   |                                       |
|  MCU’s ultra-low-power profile enables smart         |                                       |
|  applications to run off batteries.                  |                                       |
+------------------------------------------------------+                                       |
| |image23|                                            |                                       |
+------------------------------------------------------+---------------------------------------+

ACCELERATE YOUR NEXT IOT DESIGN CYCLE
======================================

ModusToolbox™ Software Environment
----------------------------------

.. rst-class:: intro

+------------------------------------------------------------+----------------------------------------------+  
| ModusToolbox simplifies development for IoT designers,     | |image24|                                    |
| delivering easy-to -use tools and a familiar IDE, with     |                                              |
| support for Windows :sup:`®`, macOS :sup:`®`, and Linux. It|                                              |
| provides a powerful suite of tools for system and          |                                              |
| peripheral set- up. Code examples, documentation, RTOS and |                                              |
| Cloud service support, and technical support forums are    |                                              |
| available to help your IoT development process along.      |                                              |
+------------------------------------------------------------+----------------------------------------------+

Graphical Configuration Tools:
------------------------------

GUI-based configuration tools used to configure peripherals such as
Smart I/O, CapSense, and wireless connectivity to quickly design and
develop IoT systems.

|image25|

PSoC 6 MCU Peripherals Configurator

|image26|

CapSense Configurator Tool

To start designing with the ModusToolbox Software Environment, please visit www.cypress.com/ModusToolbox

PSoC 6 DEVELOPMENT KITS
=======================

Cypress offers award winning development kits along with hundreds of
code examples allowing you to evaluate, prototype and debug designs
with PSoC 6. The PSoC 6 development kits are pin-compatible with
various 3rd party expansion boards including Arduino™ shield boards
and Digilent® Pmod™ daughter cards.

|image27|

SOLVE YOUR IOT PROBLEM HERE
===========================

What’s your next game-changing application?
-------------------------------------------

.. rst-class:: intro

+--------------------------------------------------------+--------------------------------------+
| |image28|                                              | |image31|                            |
|                                                        |                                      |
|| Incorporate a slick graphics display to your next     |                                      |
|| IoT application with the use of the Universal         |                                      |
|| Digital Blocks (programmable logic).                  |                                      |
+--------------------------------------------------------+                                      |
| |image29|                                              |                                      |
|                                                        |                                      |
| Add an audio subsystem with an I2S interface and       |                                      |
| two Pulse Density Modulation (PDM) channels.           |                                      |
+--------------------------------------------------------+                                      |
| |image30|                                              |                                      |
|                                                        |                                      |
| Interface to analog sensors with the programmable      |                                      |
| analog block functions of PSoC 6, which includes       |                                      |
| op-amps, comparators, and a 12-bit SAR ADC. Digital    |                                      |
| sensors can easily be connected via the Serial         |                                      |
| Communication Blocks (SCBs).                           |                                      |
+--------------------------------------------------------+--------------------------------------+

.. rst-class:: intro

+--------------------------------------+--------------------------------------------------------+
| |image32|                            | |image33|                                              |
|                                      |                                                        |
|                                      | Replace traditional buttons with touch, add touch      |
|                                      | sliders, or add proximity detection with Cypress’      |
|                                      | CapSense technology. Use the TCPWM to drive motors,    |
|                                      | actuators or LEDs.                                     |
|                                      +--------------------------------------------------------+
|                                      | |image34|                                              |
|                                      |                                                        |
|                                      | Serial Communication Blocks (SCBs) can be configured   |
|                                      | to support serial interfaces such as UART, I2C and SPI.|
|                                      | USB 2.0 host and device support is available with      |
|                                      | PSoC 6.                                                |
|                                      +--------------------------------------------------------+
|                                      | |image35|                                              |
|                                      |                                                        |
|                                      | Add Bluetooth Low Energy connectivity with built-in BLE|
|                                      | radio and sub-system. Add Wi-Fi or Bluetooth with      |
|                                      | Cypress' WICED portfolio.                              |
+--------------------------------------+--------------------------------------------------------+

.. |image1| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_1.png
.. |image2| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_2.png
.. |image3| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_3.png
.. |image4| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_4.png
.. |image5| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_5.png
.. |image6| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_6.png
.. |image7| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_7.png
.. |image8| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_8.png
.. |image9| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_9.png
.. |image10| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_10.png
.. |image11| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_11.png
.. |image12| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_12.png
.. |image13| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_13.png
.. |image14| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_14.png
.. |image15| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_15.png
.. |image16| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_16.png
.. |image17| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_17.png
.. |image18| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_18.png
.. |image19| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_19.png
.. |image20| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_20.png
.. |image21| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_21.png
.. |image22| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_22.png
.. |image23| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_23.png
.. |image24| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_24.png
.. |image25| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_25.png
.. |image26| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_26.png
.. |image27| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_27.png
.. |image28| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_28.png
.. |image29| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_29.png
.. |image30| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_30.png
.. |image31| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_31.png
.. |image32| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_32.png
.. |image33| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_33.png
.. |image34| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_34.png
.. |image35| image:: ../../_static/image/api/psoc-base-lib/Introducing_PSoC6/psoc6_35.png


