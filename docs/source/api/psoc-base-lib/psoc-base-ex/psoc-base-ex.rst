=====================
PSoC 6 Base Examples
=====================

.. raw:: html

   <script type="text/javascript">
   window.location.href = "GPIO_Interrupt_Example.html"
   </script>

.. toctree::
   :hidden:

   GPIO_Interrupt_Example.rst
   Multi_Counter_Watchdog_Timer_Example.rst
   Watchdog_Timer_Example.rst
   Fault_Handling_Example.rst
   Real_Time_Clock_Example.rst
   PDM_PCM_Example.rst
   Square_Wave_PWM_using_TCPWM_Example.rst
   PWM_Ramp_using_SmartIO_Example.rst
   True_Random_Number_Generator_Example.rst
   SHA2_with_Cryptographic_Hardware_Block_Example.rst
   AES_Encryption_Decryption_Example.rst
   Dual_Core_IPC_Semephore_Example.rst
   Dual_Core_IPC_Pipes_Example.rst
   I2S_Audio_Codec_Example.rst
   PDM_to_I2S_Routing_Example.rst
   Serial_Communications_Block_SPI_with_DMA_Example.rst
   UART_using_DMA_Example.rst
   HAL_UART_Example.rst
   SPI_Master_Example.rst
   I2C_Master_Example.rst
   I2C_Slave_Callback_Example.rst
   I2C_Master_with_EzI2C_Client.rst
