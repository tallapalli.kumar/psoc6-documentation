================
Getting Started
================

Below are getting started info on AnyCloud.

----------
 Please follow below guides to browse through respective sections.
----------

.. toctree::

   gs_terms.rst
   creating_project.rst
   how_to_chose_set_of_libraries.rst
   how_to_configure_libraries.rst
