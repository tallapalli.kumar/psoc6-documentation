===========================
DMA (Direct Memory Access)
===========================

.. doxygengroup:: group_hal_impl_dma
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


.. toctree::
   

   group__group__hal__impl__dma__dmac.rst
   group__group__hal__impl__dma__dw.rst
   
      



