=====================
I2S (Inter-IC Sound)
=====================

.. doxygengroup:: group_hal_i2s
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__i2s.rst
   

