==================================
EZI2C (Inter-Integrated Circuit)
==================================

.. doxygengroup:: group_hal_ezi2c
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__ezi2c.rst
   

