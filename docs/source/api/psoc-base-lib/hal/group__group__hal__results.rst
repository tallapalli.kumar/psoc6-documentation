=============
Result Codes
=============

.. doxygengroup:: group_hal_results
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

.. toctree::
   :hidden:

   group__group__hal__results__adc.rst
   group__group__hal__results__clock.rst
   group__group__hal__results__comp.rst
   group__group__hal__results__crc.rst
   group__group__hal__results__dac.rst
   group__group__hal__results__dma.rst
   group__group__hal__results__ezi2c.rst
   group__group__hal__results__flash.rst
   group__group__hal__results__hwmgr.rst
   group__group__hal__results__i2c.rst
   group__group__hal__results__i2s.rst
   group__group__hal__results__interconnect.rst
   group__group__hal__results__lptimer..rst
   group__group__hal__results__opamp.rst
   group__group__hal__results__pdmpcm.rst
   group__group__hal__results__pwm.rst
   group__group__hal__results__qspi.rst
   group__group__hal__results__quaddec.rst
   group__group__hal__results__rtc.rst
   group__group__hal__results__sdhc.rst
   group__group__hal__results__sdio.rst
   group__group__hal__results__spi.rst
   group__group__hal__results__syspm.rst
   group__group__hal__results__timer.rst
   group__group__hal__results__trng.rst
   group__group__hal__results__uart.rst
   group__group__hal__results__usb__dev.rst
   group__group__hal__results__wdt.rst

   


   
