==================================
ADC (Analog to Digital Converter)
==================================

.. doxygengroup:: group_hal_adc
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__adc.rst
   

