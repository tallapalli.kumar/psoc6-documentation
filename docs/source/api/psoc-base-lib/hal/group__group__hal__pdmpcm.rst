======================================================================
PDM/PCM (Pulse-Density Modulation to Pulse-Code Modulation Converter)
======================================================================

.. doxygengroup:: group_hal_pdmpcm
   :project: hal
   :members:
   :protected-members:
   :private-members:
   :undoc-members:


   

.. toctree::
   :hidden:

   group__group__hal__results__pdmpcm.rst
   

