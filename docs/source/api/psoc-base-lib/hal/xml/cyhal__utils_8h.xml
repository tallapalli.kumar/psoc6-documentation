<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cyhal__utils_8h" kind="file" language="C++">
    <compoundname>cyhal_utils.h</compoundname>
    <includedby local="yes" refid="cyhal__gpio__impl_8h">cyhal_gpio_impl.h</includedby>
    <includedby local="yes" refid="cyhal__adc_8c">cyhal_adc.c</includedby>
    <includedby local="yes" refid="cyhal__clock_8c">cyhal_clock.c</includedby>
    <includedby local="yes" refid="cyhal__dac_8c">cyhal_dac.c</includedby>
    <includedby local="yes" refid="cyhal__dma__dmac_8c">cyhal_dma_dmac.c</includedby>
    <includedby local="yes" refid="cyhal__dma__dw_8c">cyhal_dma_dw.c</includedby>
    <includedby local="yes" refid="cyhal__ezi2c_8c">cyhal_ezi2c.c</includedby>
    <includedby local="yes" refid="cyhal__i2c_8c">cyhal_i2c.c</includedby>
    <includedby local="yes" refid="cyhal__i2s_8c">cyhal_i2s.c</includedby>
    <includedby local="yes" refid="cyhal__lptimer_8c">cyhal_lptimer.c</includedby>
    <includedby local="yes" refid="cyhal__pdmpcm_8c">cyhal_pdmpcm.c</includedby>
    <includedby local="yes" refid="cyhal__pwm_8c">cyhal_pwm.c</includedby>
    <includedby local="yes" refid="cyhal__qspi_8c">cyhal_qspi.c</includedby>
    <includedby local="yes" refid="cyhal__quaddec_8c">cyhal_quaddec.c</includedby>
    <includedby local="yes" refid="cyhal__sdhc_8c">cyhal_sdhc.c</includedby>
    <includedby local="yes" refid="cyhal__syspm_8c">cyhal_syspm.c</includedby>
    <includedby local="yes" refid="cyhal__tcpwm__common_8c">cyhal_tcpwm_common.c</includedby>
    <includedby local="yes" refid="cyhal__usb__dev_8c">cyhal_usb_dev.c</includedby>
    <includedby local="yes" refid="cyhal__utils_8c">cyhal_utils.c</includedby>
    <briefdescription>
<para>Provides utility functions for working with the PSoC 6 HAL implementation. </para>    </briefdescription>
    <detaileddescription>
<para><simplesect kind="copyright"><para>Copyright 2018-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="32"><highlight class="preprocessor">#pragma<sp />once</highlight><highlight class="normal" /></codeline>
<codeline lineno="33"><highlight class="normal" /></codeline>
<codeline lineno="34"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_result.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="35"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_hw_types.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="36"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cy_utils.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="37"><highlight class="normal" /></codeline>
<codeline lineno="38"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="39"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="40"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="41"><highlight class="normal" /></codeline>
<codeline lineno="42"><highlight class="normal" /><highlight class="preprocessor">#define<sp />_CYHAL_UTILS_NS_PER_SECOND<sp />(1000000000)</highlight><highlight class="normal" /></codeline>
<codeline lineno="43"><highlight class="normal" /></codeline>
<codeline lineno="44"><highlight class="normal" /><highlight class="preprocessor">#define<sp />_CYHAL_UTILS_IRQN_OFFSET<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(16U)<sp /></highlight></codeline>
<codeline lineno="45"><highlight class="preprocessor">#define<sp />_CYHAL_UTILS_GET_CURRENT_IRQN()<sp /><sp /><sp /><sp />((IRQn_Type)<sp />(__get_IPSR()<sp />-<sp />_CYHAL_UTILS_IRQN_OFFSET))<sp /></highlight></codeline>
<codeline lineno="62"><highlight class="preprocessor">#define<sp />_CYHAL_UTILS_TRY_ALLOC(pin,<sp />mappings)<sp />\</highlight></codeline>
<codeline lineno="63"><highlight class="preprocessor"><sp /><sp /><sp /><sp />_cyhal_utils_try_alloc(pin,<sp />mappings,<sp />sizeof(mappings)/sizeof(cyhal_resource_pin_mapping_t))</highlight><highlight class="normal" /></codeline>
<codeline lineno="64"><highlight class="normal" /></codeline>
<codeline lineno="72"><highlight class="preprocessor">#define<sp />_CYHAL_UTILS_GET_RESOURCE(pin,<sp />mappings)<sp />_cyhal_utils_get_resource(pin,<sp />mappings,<sp />sizeof(mappings)/sizeof(cyhal_resource_pin_mapping_t),<sp />NULL)</highlight><highlight class="normal" /></codeline>
<codeline lineno="73"><highlight class="normal" /></codeline>
<codeline lineno="79"><highlight class="keyword">static</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">inline</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__inst__t">cyhal_resource_inst_t</ref><sp />_cyhal_utils_get_gpio_resource(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />pin)</highlight></codeline>
<codeline lineno="80"><highlight class="normal">{</highlight></codeline>
<codeline lineno="81"><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__resource__inst__t">cyhal_resource_inst_t</ref><sp />rsc<sp />=<sp />{<sp /><ref kindref="member" refid="group__group__hal__impl__hw__types_1gga63b023ea7b5e73db59ddc0423c77975ba5f4259788e0fc887772496df602d7549">CYHAL_RSC_GPIO</ref>,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga015f256578abd5638668ff19b9dc89d5">CYHAL_GET_PORT</ref>(pin),<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1gae0af2ee8c5a2a2e6661962b368d1f2ba">CYHAL_GET_PIN</ref>(pin)<sp />};</highlight></codeline>
<codeline lineno="82"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />rsc;</highlight></codeline>
<codeline lineno="83"><highlight class="normal">}</highlight></codeline>
<codeline lineno="84"><highlight class="normal" /></codeline>
<codeline lineno="93"><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__pin__mapping__t">cyhal_resource_pin_mapping_t</ref><sp />*_cyhal_utils_get_resource(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />pin,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__pin__mapping__t">cyhal_resource_pin_mapping_t</ref>*<sp />mappings,<sp /></highlight><highlight class="keywordtype">size_t</highlight><highlight class="normal"><sp />count,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__inst__t">cyhal_resource_inst_t</ref>*<sp />block_res);</highlight></codeline>
<codeline lineno="94"><highlight class="normal" /></codeline>
<codeline lineno="105"><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__pin__mapping__t">cyhal_resource_pin_mapping_t</ref>*<sp />_cyhal_utils_try_alloc(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />pin,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__pin__mapping__t">cyhal_resource_pin_mapping_t</ref><sp />*mappings,<sp /></highlight><highlight class="keywordtype">size_t</highlight><highlight class="normal"><sp />count);</highlight></codeline>
<codeline lineno="106"><highlight class="normal" /></codeline>
<codeline lineno="112"><highlight class="normal"><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />_cyhal_utils_reserve_and_connect(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />pin,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__pin__mapping__t">cyhal_resource_pin_mapping_t</ref><sp />*mapping);</highlight></codeline>
<codeline lineno="113"><highlight class="normal" /></codeline>
<codeline lineno="118"><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />_cyhal_utils_disconnect_and_free(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />pin);</highlight></codeline>
<codeline lineno="119"><highlight class="normal" /></codeline>
<codeline lineno="125"><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />_cyhal_utils_release_if_used(<ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble_1ga707195ce0627016bf371643bdd9caa51">cyhal_gpio_t</ref><sp />*pin);</highlight></codeline>
<codeline lineno="126"><highlight class="normal" /></codeline>
<codeline lineno="139"><highlight class="keyword">static</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">inline</highlight><highlight class="normal"><sp />uint32_t<sp />_cyhal_utils_divider_value(uint32_t<sp />frequency,<sp />uint32_t<sp />frac_bits)</highlight></codeline>
<codeline lineno="140"><highlight class="normal">{</highlight></codeline>
<codeline lineno="141"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#if<sp />defined(COMPONENT_PSOC6HAL)</highlight><highlight class="normal" /></codeline>
<codeline lineno="142"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />((Cy_SysClk_ClkPeriGetFrequency()<sp />*<sp />(1<sp />&lt;&lt;<sp />frac_bits))<sp />+<sp />(frequency<sp />/<sp />2))<sp />/<sp />frequency;</highlight></codeline>
<codeline lineno="143"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#else<sp /></highlight><highlight class="comment">/*<sp />COMPONENT_PSOC4HAL<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="144"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />((Cy_SysClk_ClkSysGetFrequency()<sp />*<sp />(1<sp />&lt;&lt;<sp />frac_bits))<sp />+<sp />(frequency<sp />/<sp />2))<sp />/<sp />frequency;</highlight></codeline>
<codeline lineno="145"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="146"><highlight class="normal">}</highlight></codeline>
<codeline lineno="147"><highlight class="normal" /></codeline>
<codeline lineno="155"><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />_cyhal_utils_resources_equal(</highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__inst__t">cyhal_resource_inst_t</ref><sp />*resource1,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__inst__t">cyhal_resource_inst_t</ref><sp />*resource2);</highlight></codeline>
<codeline lineno="156"><highlight class="normal" /></codeline>
<codeline lineno="165"><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />_cyhal_utils_resources_equal_all(uint32_t<sp />count,<sp />...);</highlight></codeline>
<codeline lineno="166"><highlight class="normal" /></codeline>
<codeline lineno="178"><highlight class="normal">uint32_t<sp />_cyhal_utils_convert_flags(</highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />uint32_t<sp />map[],<sp />uint32_t<sp />count,<sp />uint32_t<sp />source_flags);</highlight></codeline>
<codeline lineno="179"><highlight class="normal" /></codeline>
<codeline lineno="185"><highlight class="normal">cy_en_syspm_callback_mode_t<sp />_cyhal_utils_convert_haltopdl_pm_mode(<ref kindref="member" refid="group__group__hal__syspm_1ga5a6a88eb2b6e35dbea25e36b027bf4ba">cyhal_syspm_callback_mode_t</ref><sp />mode);</highlight></codeline>
<codeline lineno="186"><highlight class="normal" /></codeline>
<codeline lineno="192"><highlight class="normal"><ref kindref="member" refid="group__group__hal__syspm_1ga5a6a88eb2b6e35dbea25e36b027bf4ba">cyhal_syspm_callback_mode_t</ref><sp />_cyhal_utils_convert_pdltohal_pm_mode(cy_en_syspm_callback_mode_t<sp />mode);</highlight></codeline>
<codeline lineno="193"><highlight class="normal" /></codeline>
<codeline lineno="200"><highlight class="keyword">static</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">inline</highlight><highlight class="normal"><sp /></highlight><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />_cyhal_utils_is_new_clock_format(</highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__clock__t">cyhal_clock_t</ref><sp />*clock)</highlight></codeline>
<codeline lineno="201"><highlight class="normal">{</highlight></codeline>
<codeline lineno="202"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#if<sp />defined(COMPONENT_PSOC6HAL)</highlight><highlight class="normal" /></codeline>
<codeline lineno="203"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />(((<ref kindref="member" refid="group__group__hal__impl__hw__types_1ga1968e793be1599de10dec9e2d48f66d6">cyhal_clock_block_t</ref>)clock-&gt;<ref kindref="member" refid="structcyhal__clock__t_1a12a2c8d12099bb43979094e9336ffec6">div_type</ref><sp />==<sp />clock-&gt;block)<sp />&amp;&amp;<sp />(clock-&gt;<ref kindref="member" refid="structcyhal__clock__t_1a621dd72458c30e3386ca3a60c5646a96">div_num</ref><sp />==<sp />clock-&gt;channel));</highlight></codeline>
<codeline lineno="204"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#else<sp /></highlight><highlight class="comment">/*<sp />COMPONENT_PSOC4HAL<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="205"><highlight class="normal"><sp /><sp /><sp /><sp />CY_UNUSED_PARAMETER(clock);</highlight></codeline>
<codeline lineno="206"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">;</highlight></codeline>
<codeline lineno="207"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="208"><highlight class="normal">}</highlight></codeline>
<codeline lineno="209"><highlight class="normal" /></codeline>
<codeline lineno="215"><highlight class="keyword">static</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">inline</highlight><highlight class="normal"><sp /></highlight><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />_cyhal_utils_update_clock_format(<ref kindref="compound" refid="structcyhal__clock__t">cyhal_clock_t</ref><sp />*clock)</highlight></codeline>
<codeline lineno="216"><highlight class="normal">{</highlight></codeline>
<codeline lineno="217"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#if<sp />defined(COMPONENT_PSOC6HAL)</highlight><highlight class="normal" /></codeline>
<codeline lineno="218"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal">(((<ref kindref="member" refid="group__group__hal__impl__hw__types_1ga1968e793be1599de10dec9e2d48f66d6">cyhal_clock_block_t</ref>)clock-&gt;<ref kindref="member" refid="structcyhal__clock__t_1a12a2c8d12099bb43979094e9336ffec6">div_type</ref><sp />!=<sp />clock-&gt;block)<sp />||<sp />(clock-&gt;<ref kindref="member" refid="structcyhal__clock__t_1a621dd72458c30e3386ca3a60c5646a96">div_num</ref><sp />!=<sp />clock-&gt;channel))</highlight></codeline>
<codeline lineno="219"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="220"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />clock-&gt;block<sp />=<sp />(<ref kindref="member" refid="group__group__hal__impl__hw__types_1ga1968e793be1599de10dec9e2d48f66d6">cyhal_clock_block_t</ref>)clock-&gt;<ref kindref="member" refid="structcyhal__clock__t_1a12a2c8d12099bb43979094e9336ffec6">div_type</ref>;</highlight></codeline>
<codeline lineno="221"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />clock-&gt;channel<sp />=<sp />clock-&gt;<ref kindref="member" refid="structcyhal__clock__t_1a621dd72458c30e3386ca3a60c5646a96">div_num</ref>;</highlight></codeline>
<codeline lineno="222"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="223"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#else</highlight><highlight class="normal" /></codeline>
<codeline lineno="224"><highlight class="normal"><sp /><sp /><sp /><sp />CY_UNUSED_PARAMETER(clock);</highlight></codeline>
<codeline lineno="225"><highlight class="normal" /><highlight class="preprocessor"><sp /><sp /><sp /><sp />#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="226"><highlight class="normal">}</highlight></codeline>
<codeline lineno="227"><highlight class="normal" /></codeline>
<codeline lineno="235"><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />_cyhal_utils_get_peri_clock_details(</highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__clock__t">cyhal_clock_t</ref><sp />*clock,<sp />cy_en_divider_types_t<sp />*div_type,<sp />uint32_t<sp />*div_num);</highlight></codeline>
<codeline lineno="236"><highlight class="normal" /></codeline>
<codeline lineno="245"><highlight class="normal">int32_t<sp />_cyhal_utils_calculate_tolerance(<ref kindref="member" refid="group__group__hal__clock_1gafb9a1aeacf13f74784613724596a040f">cyhal_clock_tolerance_unit_t</ref><sp />type,<sp />uint32_t<sp />desired_hz,<sp />uint32_t<sp />actual_hz);</highlight></codeline>
<codeline lineno="246"><highlight class="normal" /></codeline>
<codeline lineno="247"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(COMPONENT_PSOC6HAL)</highlight><highlight class="normal" /></codeline>
<codeline lineno="248"><highlight class="normal" /></codeline>
<codeline lineno="259"><highlight class="normal"><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />_cyhal_utils_find_hf_clk_div(uint32_t<sp />hz_src,<sp />uint32_t<sp />desired_hz,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__clock__tolerance__t">cyhal_clock_tolerance_t</ref><sp />*tolerance,</highlight></codeline>
<codeline lineno="260"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />only_below_desired,<sp />uint8_t<sp />*div);</highlight></codeline>
<codeline lineno="261"><highlight class="normal" /></codeline>
<codeline lineno="270"><highlight class="normal"><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />_cyhal_utils_allocate_clock(<ref kindref="compound" refid="structcyhal__clock__t">cyhal_clock_t</ref><sp />*clock,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__resource__inst__t">cyhal_resource_inst_t</ref><sp />*clocked_item,<sp /><ref kindref="member" refid="group__group__hal__impl__hw__types_1ga1968e793be1599de10dec9e2d48f66d6">cyhal_clock_block_t</ref><sp />div,<sp /></highlight><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />accept_larger);</highlight></codeline>
<codeline lineno="271"><highlight class="normal" /></codeline>
<codeline lineno="282"><highlight class="normal"><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />_cyhal_utils_set_clock_frequency(<ref kindref="compound" refid="structcyhal__clock__t">cyhal_clock_t</ref>*<sp />clock,<sp />uint32_t<sp />hz,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__clock__tolerance__t">cyhal_clock_tolerance_t</ref><sp />*tolerance);</highlight></codeline>
<codeline lineno="283"><highlight class="normal" /></codeline>
<codeline lineno="294"><highlight class="normal"><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />_cyhal_utils_set_clock_frequency2(<ref kindref="compound" refid="structcyhal__clock__t">cyhal_clock_t</ref><sp />*clock,<sp />uint32_t<sp />hz,<sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp /><ref kindref="compound" refid="structcyhal__clock__tolerance__t">cyhal_clock_tolerance_t</ref><sp />*tolerance);</highlight></codeline>
<codeline lineno="295"><highlight class="normal" /></codeline>
<codeline lineno="296"><highlight class="normal" /><highlight class="comment">/*<sp />Compatibility<sp />macros<sp />until<sp />other<sp />environments<sp />are<sp />updated<sp />to<sp />not<sp />use<sp />this<sp />anymore.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="297"><highlight class="normal" /><highlight class="preprocessor">#define<sp />CY_UTILS_GET_RESOURCE(pin,<sp />mappings)<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />_CYHAL_UTILS_GET_RESOURCE(pin,<sp />mappings)</highlight><highlight class="normal" /></codeline>
<codeline lineno="298"><highlight class="normal" /><highlight class="preprocessor">#define<sp />cyhal_utils_get_resource(pin,<sp />mappings,<sp />count)<sp /><sp />_cyhal_utils_get_resource(pin,<sp />mappings,<sp />count,<sp />NULL)</highlight><highlight class="normal" /></codeline>
<codeline lineno="299"><highlight class="normal" /><highlight class="preprocessor">#define<sp />cyhal_utils_get_gpio_resource(pin)<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />_cyhal_utils_get_gpio_resource(pin)</highlight><highlight class="normal" /></codeline>
<codeline lineno="300"><highlight class="normal" /><highlight class="preprocessor">#define<sp />CYHAL_SCB_BASE_ADDRESSES<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />_CYHAL_SCB_BASE_ADDRESSES</highlight><highlight class="normal" /></codeline>
<codeline lineno="301"><highlight class="normal" /><highlight class="preprocessor">#define<sp />CYHAL_TCPWM_DATA<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />_CYHAL_TCPWM_DATA</highlight><highlight class="normal" /></codeline>
<codeline lineno="302"><highlight class="normal" /></codeline>
<codeline lineno="303"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />defined(COMPONENT_PSOC6HAL)<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="304"><highlight class="normal" /></codeline>
<codeline lineno="305"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="306"><highlight class="normal">}</highlight></codeline>
<codeline lineno="307"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="308"><highlight class="normal" /></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/include/cyhal_utils.h" />
  </compounddef>
</doxygen>