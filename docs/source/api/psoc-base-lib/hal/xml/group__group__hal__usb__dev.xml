<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__hal__usb__dev" kind="group">
    <compoundname>group_hal_usb_dev</compoundname>
    <title>USB Device</title>
    <innergroup refid="group__group__hal__results__usbdev">USB Device HAL Results</innergroup>
    <innergroup refid="group__group__hal__usb__dev__endpoint">Endpoint</innergroup>
    <innergroup refid="group__group__hal__usb__dev__common">Common</innergroup>
    <innergroup refid="group__group__hal__usb__dev__ep0">EP0</innergroup>
    <briefdescription>
<para>High level interface for interacting with the USB Device. </para>    </briefdescription>
    <detaileddescription>
<para>This block supports one control endpoint (EP0) and one or more data endpoints. See the device datasheet for the number of data endpoints supported.</para><para>Four transfer types are supported (see <ref kindref="member" refid="group__group__hal__usb__dev__endpoint_1gaa050c9639ab34294735f8ef572b9e417">cyhal_usb_dev_ep_type_t</ref>):<itemizedlist>
<listitem><para>Bulk</para></listitem><listitem><para>Interrupt</para></listitem><listitem><para>Isochronous</para></listitem><listitem><para>Control</para></listitem></itemizedlist>
</para>
<para><heading level="1">Features</heading></para>
<para><itemizedlist>
<listitem><para>Complies with USB specification 2.0</para></listitem><listitem><para>Supports full-speed peripheral device operation with a signaling bit rate of 12 Mbps.</para></listitem><listitem><para>Configurable D+ AND D- pins using <ref kindref="member" refid="group__group__hal__types__implementation_1ga37579d500a1d05905c8f2bea4e907f99">cyhal_gpio_t</ref></para></listitem><listitem><para>Configurable Interrupt and Callback assignment on USB events like SOF, Bus Reset, EP0 Setup and EP0 transaction.</para></listitem><listitem><para>Configurable USB device address.</para></listitem><listitem><para>Configurable USB Endpoints (except for Endpoint 0)</para></listitem></itemizedlist>
</para>

<para><heading level="1">Quick Start</heading></para>
<para><ref kindref="member" refid="group__group__hal__usb__dev__common_1ga3fada34f2e7c54d8fae5def861f41083">cyhal_usb_dev_init</ref> can be used for initialization of USB by providing the USBDP and USBDM pins. See <ref kindref="member" refid="group__group__hal__usb__dev_1subsection_usb_dev_snippet_1">Snippet 1: USB Device Initialization</ref> for the initialization code snippet.</para>

<para><heading level="1">Code snippets</heading></para>

<para><bold>Snippet 1: USB Device Initialization</bold></para>
<para>The following section initializes the USB Device and assigns the USBDM and USBDP pins using <ref kindref="member" refid="group__group__hal__usb__dev__common_1ga3fada34f2e7c54d8fae5def861f41083">cyhal_usb_dev_init</ref>. The clock parameter <bold>clk</bold> is optional and need not be provided (NULL), to generate and use an available clock resource with a default frequency. The device can be made physically visible to the USB Host by using <ref kindref="member" refid="group__group__hal__usb__dev__common_1ga58083386748f9295037d7567330b9677">cyhal_usb_dev_connect</ref></para><para><programlisting filename="usb_dev.c"><codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="compound" refid="structcyhal__usb__dev__t">cyhal_usb_dev_t</ref><sp />usb_dev_obj;</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />rslt;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />USB,<sp />assign<sp />the<sp />USBDP<sp />and<sp />USBDM<sp />pins<sp />and<sp />assign<sp />a<sp />new<sp />clock<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__usb__dev__common_1ga3fada34f2e7c54d8fae5def861f41083">cyhal_usb_dev_init</ref>(&amp;usb_dev_obj,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble__usb_1ggadb1b30ad8667ff98ff7c39046c4d0741a91f4d44fb984bb1c20f9a34af8015642">USBDP</ref>,<sp /><ref kindref="member" refid="group__group__hal__impl__pin__package__psoc6__01__104__m__csp__ble__usb_1ggadb1b30ad8667ff98ff7c39046c4d0741a4ce5d83730a8b57610cdffe18cd54eff">USBDM</ref>,<sp />NULL);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Application<sp />code<sp />to<sp />register<sp />callback<sp />functions,<sp />enable<sp />events,<sp />add<sp />endpoints,<sp />etc.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Make<sp />the<sp />USB<sp />device<sp />physically<sp />visible<sp />to<sp />USB<sp />host<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__usb__dev__common_1ga58083386748f9295037d7567330b9677">cyhal_usb_dev_connect</ref>(&amp;usb_dev_obj);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para>

<para><bold>Snippet 2: Handling USB Event Completion</bold></para>
<para>USB events (see <ref kindref="member" refid="group__group__hal__usb__dev__common_1gab0df7903f398f360d71e8beb78ead49b">cyhal_usb_dev_event_t</ref>) like Bus Reset, EP0 transaction, EP0 Setup can be mapped to an interrupt and assigned a callback function. The callback function needs to be first registered using <ref kindref="member" refid="group__group__hal__usb__dev__common_1ga12a92ec4893a4eb384ecf1260ca9ea42">cyhal_usb_dev_register_event_callback</ref>. Use different callback functions to handle events individually.</para><para><programlisting filename="usb_dev.c"><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><ref kindref="compound" refid="structcyhal__usb__dev__t">cyhal_usb_dev_t</ref><sp />usb_dev_obj;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />usb_dev_event_handler()</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Handle<sp />USB<sp />Events<sp />here<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />snippet_cyhal_usb_dev_event()</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />USB<sp />as<sp />done<sp />in<sp />Snippet<sp />1<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Register<sp />callback<sp />handler<sp />for<sp />USB<sp />events<sp />EP0_OUT,<sp />EP0_IN,<sp />Bus<sp />Reset,<sp />EP0_SETUP<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__usb__dev__common_1ga12a92ec4893a4eb384ecf1260ca9ea42">cyhal_usb_dev_register_event_callback</ref>(&amp;usb_dev_obj,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />(<ref kindref="member" refid="group__group__hal__usb__dev__common_1gab0df7903f398f360d71e8beb78ead49b">cyhal_usb_dev_event_t</ref>)(<ref kindref="member" refid="group__group__hal__usb__dev__common_1ggab0df7903f398f360d71e8beb78ead49bac715be16269b7dfb4294a8fb13b649ab">CYHAL_USB_DEV_EVENT_EP0_OUT</ref><sp />|<sp /><ref kindref="member" refid="group__group__hal__usb__dev__common_1ggab0df7903f398f360d71e8beb78ead49baa1d503404e8c909e63c329dc4f74684b">CYHAL_USB_DEV_EVENT_EP0_SETUP</ref><sp />|</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__usb__dev__common_1ggab0df7903f398f360d71e8beb78ead49ba2dfe95c0891c7c37eda0c5e707e45229">CYHAL_USB_DEV_EVENT_EP0_IN</ref><sp />|<sp /><ref kindref="member" refid="group__group__hal__usb__dev__common_1ggab0df7903f398f360d71e8beb78ead49ba519b2fbe16ee25f7b19e30e1055a6532">CYHAL_USB_DEV_EVENT_BUS_RESET</ref>),</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />usb_dev_event_handler);</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para>

<para><bold>Snippet 3: Custom USB Interrupt Handler</bold></para>
<para>The following section illustrates how to set up the IRQ interrupt handler for USB device. Inside the handler <ref kindref="member" refid="group__group__hal__usb__dev__common_1ga437539361016b466626e103dd72a886d">cyhal_usb_dev_process_irq</ref> has been used to process the interrupts.</para><para><programlisting filename="usb_dev.c"><codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><ref kindref="compound" refid="structcyhal__usb__dev__t">cyhal_usb_dev_t</ref><sp />usb_dev_obj;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="comment">/*<sp />Interrupt<sp />handler<sp />callback<sp />function<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />usb_irq_handler()</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Calling<sp />the<sp />default<sp />USB<sp />handler*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__usb__dev__common_1ga437539361016b466626e103dd72a886d">cyhal_usb_dev_process_irq</ref>(&amp;usb_dev_obj);</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />snippet_cyhal_usb_dev_irq()</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />USB<sp />Device<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Configure<sp />USB<sp />Device<sp />event<sp />enablement<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__usb__dev__common_1gaa1cbb4285b35e80b57a6fce5127890ce">cyhal_usb_dev_irq_enable</ref>(&amp;usb_dev_obj,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Register<sp />the<sp />callback<sp />function<sp />to<sp />handle<sp />the<sp />events<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__usb__dev__common_1gaa5db167ff60bc74a118823bb876bcdd6">cyhal_usb_dev_register_irq_callback</ref>(&amp;usb_dev_obj,<sp />usb_irq_handler);</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para>

<para><bold>Snippet 4: Adding an Endpoint and Handling its Interrupts</bold></para>
<para>The following section shows how to add endpoint to the USB device and configure the endpoint using <ref kindref="member" refid="group__group__hal__usb__dev__endpoint_1ga19afd43bcec6dc1c3a54cc3d3620bdcf">cyhal_usb_dev_endpoint_add</ref>. The interrupts associated with the endpoints are handled by a callback function registered using <ref kindref="member" refid="group__group__hal__usb__dev__endpoint_1ga128a701f82f6250a3a907f417959182e">cyhal_usb_dev_register_endpoint_callback</ref>. The endpoint can also be configured using <ulink url="https://www.cypress.com/ModusToolboxUSBConfig">ModusToolbox USB Configurator</ulink></para><para><programlisting filename="usb_dev.c"><codeline><highlight class="normal"><ref kindref="member" refid="group__group__result_1gaca79700fcc701534ce61778a9bcf57d1">cy_rslt_t</ref><sp />rslt;</highlight></codeline>
<codeline><highlight class="normal"><ref kindref="compound" refid="structcyhal__usb__dev__t">cyhal_usb_dev_t</ref><sp />usb_dev_obj;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="comment">/*<sp />Declare<sp />USB<sp />Endpoint<sp />address<sp />that<sp />consists<sp />of<sp />endpoint<sp />number<sp />and<sp />direction<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="comment">/*<sp />EP1<sp />address<sp />value<sp />=<sp />0x81<sp />where<sp />bits<sp />6:0<sp />represent<sp />the<sp />endpoint<sp />number(0x01)<sp />and<sp />7th<sp />bit<sp />represent<sp />the<sp />(IN)<sp />direction<sp />(0x10).<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="comment">/*<sp />The<sp />decimal<sp />equivalent<sp />of<sp />which<sp />is<sp />129.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="comment">/*<sp />This<sp />value<sp />is<sp />obtained<sp />while<sp />configuring<sp />the<sp />endpoint<sp />using<sp />Modustoolbox<sp />USB<sp />Configurator<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keyword">const</highlight><highlight class="normal"><sp />uint8_t<sp />EP1<sp />=<sp />129;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="comment">/*<sp />Declare<sp />the<sp />maximum<sp />packet<sp />size<sp />that<sp />can<sp />be<sp />sent<sp />or<sp />received<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keyword">const</highlight><highlight class="normal"><sp />uint32_t<sp />max_packet_size<sp />=<sp />8;</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />endpoint_handler()</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Handle<sp />Endpoint<sp />related<sp />Interrupts<sp />here<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />snippet_cyhal_usb_dev_endpoint()</highlight></codeline>
<codeline><highlight class="normal">{</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Initialize<sp />the<sp />USB<sp />Device<sp />here<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Adding<sp />Endpoint<sp />1<sp />as<sp />Interrupt<sp />endpoint,<sp />with<sp />direction<sp />IN<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />rslt<sp />=<sp /><ref kindref="member" refid="group__group__hal__usb__dev__endpoint_1ga19afd43bcec6dc1c3a54cc3d3620bdcf">cyhal_usb_dev_endpoint_add</ref>(&amp;usb_dev_obj,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">,<sp /></highlight><highlight class="keyword">true</highlight><highlight class="normal">,<sp />EP1,<sp />max_packet_size,<sp />CYHAL_USB_DEV_EP_TYPE_INT<sp />);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Register<sp />callback<sp />handler<sp />for<sp />Endpoint<sp />1<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__usb__dev__endpoint_1ga128a701f82f6250a3a907f417959182e">cyhal_usb_dev_register_endpoint_callback</ref>(&amp;usb_dev_obj,</highlight></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp />(<ref kindref="member" refid="group__group__hal__usb__dev__common_1ga62a268fe35f5e68f1faf9b362ee6c307">cyhal_usb_dev_ep_t</ref>)<sp />EP1,<sp />(<ref kindref="member" refid="group__group__hal__usb__dev__common_1ga8e9b21021ff08b7d2d53aaaef5c8f1b6">cyhal_usb_dev_endpoint_callback_t</ref>)<sp />endpoint_handler);</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Enable<sp />added<sp />Endpoint<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline><highlight class="normal"><sp /><sp /><sp /><sp /><ref kindref="member" refid="group__group__hal__usb__dev__common_1ga93a43b761729183af402967f11c212ae">cyhal_usb_dev_set_configured</ref>(&amp;usb_dev_obj);</highlight></codeline>
<codeline><highlight class="normal">}</highlight></codeline>
<codeline><highlight class="normal" /></codeline>
</programlisting></para>

    </detaileddescription>
  </compounddef>
</doxygen>