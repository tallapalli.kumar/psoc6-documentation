<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cyhal__pwm__impl_8h" kind="file" language="C++">
    <compoundname>cyhal_pwm_impl.h</compoundname>
    <includes local="yes" refid="cyhal__tcpwm__common_8h">cyhal_tcpwm_common.h</includes>
    <briefdescription>
<para>Description: Provides a high level interface for interacting with the Cypress PWM. </para>    </briefdescription>
    <detaileddescription>
<para><simplesect kind="copyright"><para>Copyright 2019-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="25"><highlight class="preprocessor">#pragma<sp />once</highlight><highlight class="normal" /></codeline>
<codeline lineno="26"><highlight class="normal" /></codeline>
<codeline lineno="27"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cyhal_tcpwm_common.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="28"><highlight class="normal" /></codeline>
<codeline lineno="29"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(CY_IP_MXTCPWM_INSTANCES)<sp />||<sp />defined(CY_IP_M0S8TCPWM_INSTANCES)</highlight><highlight class="normal" /></codeline>
<codeline lineno="30"><highlight class="normal" /></codeline>
<codeline lineno="31"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="32"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="33"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />__cplusplus<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="34"><highlight class="normal" /></codeline>
<codeline lineno="35"><highlight class="normal">__STATIC_INLINE<sp />uint32_t<sp />_cyhal_pwm_convert_event(<ref kindref="member" refid="group__group__hal__pwm_1gabfbeebd93efd6491261d3eb6aefef554">cyhal_pwm_event_t</ref><sp />event)</highlight></codeline>
<codeline lineno="36"><highlight class="normal">{</highlight></codeline>
<codeline lineno="37"><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />pdl_event<sp />=<sp />0U;</highlight></codeline>
<codeline lineno="38"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(event<sp />&amp;<sp /><ref kindref="member" refid="group__group__hal__pwm_1ggabfbeebd93efd6491261d3eb6aefef554a6544b76f92d881a053531d723ed70104">CYHAL_PWM_IRQ_TERMINAL_COUNT</ref>)</highlight></codeline>
<codeline lineno="39"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="40"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />pdl_event<sp />|=<sp />CY_TCPWM_INT_ON_TC;</highlight></codeline>
<codeline lineno="41"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="42"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">if</highlight><highlight class="normal"><sp />(event<sp />&amp;<sp /><ref kindref="member" refid="group__group__hal__pwm_1ggabfbeebd93efd6491261d3eb6aefef554aff354daea767d054ef38516556bbf20b">CYHAL_PWM_IRQ_COMPARE</ref>)</highlight></codeline>
<codeline lineno="43"><highlight class="normal"><sp /><sp /><sp /><sp />{</highlight></codeline>
<codeline lineno="44"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />pdl_event<sp />|=<sp />CY_TCPWM_INT_ON_CC;</highlight></codeline>
<codeline lineno="45"><highlight class="normal"><sp /><sp /><sp /><sp />}</highlight></codeline>
<codeline lineno="46"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keywordflow">return</highlight><highlight class="normal"><sp />pdl_event;</highlight></codeline>
<codeline lineno="47"><highlight class="normal">}</highlight></codeline>
<codeline lineno="48"><highlight class="normal" /></codeline>
<codeline lineno="49"><highlight class="normal">__STATIC_INLINE<sp /></highlight><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />_cyhal_pwm_register_callback(<ref kindref="compound" refid="structcyhal__pwm__t">cyhal_pwm_t</ref><sp />*obj,<sp /><ref kindref="member" refid="group__group__hal__pwm_1gaaa08c1a2445ea5243c96ca657b37ec67">cyhal_pwm_event_callback_t</ref><sp />callback,<sp /></highlight><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />*callback_arg)</highlight></codeline>
<codeline lineno="50"><highlight class="normal">{</highlight></codeline>
<codeline lineno="51"><highlight class="normal"><sp /><sp /><sp /><sp />_cyhal_tcpwm_register_callback(&amp;obj-&gt;tcpwm.resource,<sp />(cy_israddress)<sp />callback,<sp />callback_arg);</highlight></codeline>
<codeline lineno="52"><highlight class="normal">}</highlight></codeline>
<codeline lineno="53"><highlight class="normal" /></codeline>
<codeline lineno="54"><highlight class="normal" /><highlight class="preprocessor">#define<sp />cyhal_pwm_register_callback(obj,<sp />callback,<sp />callback_arg)<sp />\</highlight></codeline>
<codeline lineno="55"><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />_cyhal_pwm_register_callback(obj,<sp />callback,<sp />callback_arg)</highlight><highlight class="normal" /></codeline>
<codeline lineno="56"><highlight class="normal" /></codeline>
<codeline lineno="57"><highlight class="normal">__STATIC_INLINE<sp /></highlight><highlight class="keywordtype">void</highlight><highlight class="normal"><sp />_cyhal_pwm_enable_event(<ref kindref="compound" refid="structcyhal__pwm__t">cyhal_pwm_t</ref><sp />*obj,<sp /><ref kindref="member" refid="group__group__hal__pwm_1gabfbeebd93efd6491261d3eb6aefef554">cyhal_pwm_event_t</ref><sp />event,<sp />uint8_t<sp />intr_priority,<sp /></highlight><highlight class="keywordtype">bool</highlight><highlight class="normal"><sp />enable)</highlight></codeline>
<codeline lineno="58"><highlight class="normal">{</highlight></codeline>
<codeline lineno="59"><highlight class="normal"><sp /><sp /><sp /><sp />uint32_t<sp />converted<sp />=<sp />_cyhal_pwm_convert_event(event);</highlight></codeline>
<codeline lineno="60"><highlight class="normal"><sp /><sp /><sp /><sp />_cyhal_tcpwm_enable_event(obj-&gt;tcpwm.base,<sp />&amp;obj-&gt;tcpwm.resource,<sp />converted,<sp />intr_priority,<sp />enable);</highlight></codeline>
<codeline lineno="61"><highlight class="normal">}</highlight></codeline>
<codeline lineno="62"><highlight class="normal" /></codeline>
<codeline lineno="63"><highlight class="normal" /><highlight class="preprocessor">#define<sp />cyhal_pwm_enable_event(obj,<sp />event,<sp />intr_priority,<sp />enable)<sp />\</highlight></codeline>
<codeline lineno="64"><highlight class="preprocessor"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />_cyhal_pwm_enable_event(obj,<sp />event,<sp />intr_priority,<sp />enable)</highlight><highlight class="normal" /></codeline>
<codeline lineno="65"><highlight class="normal" /></codeline>
<codeline lineno="66"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="67"><highlight class="normal">}</highlight></codeline>
<codeline lineno="68"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />__cplusplus<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="69"><highlight class="normal" /></codeline>
<codeline lineno="70"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />defined(CY_IP_MXTCPWM_INSTANCES)<sp />*/</highlight><highlight class="preprocessor" /></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_DEPRECATED/psoc6hal/COMPONENT_PSOC6HAL/include/cyhal_pwm_impl.h" />
  </compounddef>
</doxygen>