<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="indexpage" kind="group">
    <compoundname>index</compoundname>
    <title>CLib FreeRTOS Support</title>
    <briefdescription>
    </briefdescription>
    <detaileddescription>
<para><heading level="2">Overview</heading>
</para><para>The CLib FreeRTOS Support Library provides the necessary hooks to make C library functions such as malloc and free thread safe. This implementation is specific to FreeRTOS and requires it to be present to build. For details on what this library provides see the toolchain specific documentation at:<itemizedlist>
<listitem><para>ARM: <ulink url="https://developer.arm.com/docs/100073/0614/the-arm-c-and-c-libraries/multithreaded-support-in-arm-c-libraries/management-of-locks-in-multithreaded-applications">https://developer.arm.com/docs/100073/0614/the-arm-c-and-c-libraries/multithreaded-support-in-arm-c-libraries/management-of-locks-in-multithreaded-applications</ulink></para></listitem><listitem><para>GCC: <ulink url="https://sourceware.org/newlib/libc.html#g_t_005f_005fmalloc_005flock">https://sourceware.org/newlib/libc.html#g_t_005f_005fmalloc_005flock</ulink></para></listitem><listitem><para>IAR: <ulink url="http://supp.iar.com/filespublic/updinfo/011261/arm/doc/EWARM_DevelopmentGuide.ENU.pdf">http://supp.iar.com/filespublic/updinfo/011261/arm/doc/EWARM_DevelopmentGuide.ENU.pdf</ulink></para></listitem></itemizedlist>
</para><para>The startup code must call cy_toolchain_init (for GCC and IAR). This must occur after static data initialization and before static constructors. This is done automatically for PSoC devices. See the PSoC startup files for an example.</para><para>To enable Thread Local Storage, configUSE_NEWLIB_REENTRANT must be enabled.</para><para>While this is specific to FreeRTOS, it can be used as a basis for supporting other RTOSes as well.</para><para><heading level="2">Requirements</heading>
</para><para>To use this library, the following configuration options must be enabled in FreeRTOSConfig.h:<itemizedlist>
<listitem><para>configUSE_MUTEXES</para></listitem><listitem><para>configUSE_RECURSIVE_MUTEXES</para></listitem><listitem><para>configSUPPORT_STATIC_ALLOCATION</para></listitem></itemizedlist>
</para><para>When building with IAR, the '<ndash />threaded_lib' argument must also be provided when linking. This is done automatically with psoc6make 1.3.1 and later.</para><para><heading level="2">Features</heading>
</para><para><itemizedlist>
<listitem><para>GCC Newlib implementations for:<itemizedlist>
<listitem><para>_sbrk</para></listitem><listitem><para>__malloc_lock</para></listitem><listitem><para>__malloc_unlock</para></listitem><listitem><para>__env_lock</para></listitem><listitem><para>__env_unlock</para></listitem></itemizedlist>
</para></listitem><listitem><para>ARM C library implementations for:<itemizedlist>
<listitem><para>_platform_post_stackheap_init</para></listitem><listitem><para>__user_perthread_libspace</para></listitem><listitem><para>_mutex_initialize</para></listitem><listitem><para>_mutex_acquire</para></listitem><listitem><para>_mutex_release</para></listitem><listitem><para>_mutex_free</para></listitem><listitem><para>_sys_exit</para></listitem><listitem><para>$Sub$$_sys_open</para></listitem><listitem><para>_ttywrch</para></listitem><listitem><para>_sys_command_string</para></listitem></itemizedlist>
</para></listitem><listitem><para>IAR C library implementations for:<itemizedlist>
<listitem><para>__aeabi_read_tp</para></listitem><listitem><para>_reclaim_reent</para></listitem><listitem><para>__iar_system_Mtxinit</para></listitem><listitem><para>__iar_system_Mtxlock</para></listitem><listitem><para>__iar_system_Mtxunlock</para></listitem><listitem><para>__iar_system_Mtxdst</para></listitem><listitem><para>__iar_file_Mtxinit</para></listitem><listitem><para>__iar_file_Mtxlock</para></listitem><listitem><para>__iar_file_Mtxunlock</para></listitem><listitem><para>__iar_file_Mtxdst</para></listitem><listitem><para>__iar_Initdynamiclock</para></listitem><listitem><para>__iar_Lockdynamiclock</para></listitem><listitem><para>__iar_Unlockdynamiclock</para></listitem><listitem><para>__iar_Dstdynamiclock</para></listitem><listitem><para>__close</para></listitem><listitem><para>__lseek</para></listitem><listitem><para>remove</para></listitem></itemizedlist>
</para></listitem><listitem><para>C++ implementations for:<itemizedlist>
<listitem><para>__cxa_guard_acquire</para></listitem><listitem><para>__cxa_guard_abort</para></listitem><listitem><para>__cxa_guard_release</para></listitem></itemizedlist>
</para></listitem></itemizedlist>
</para><para><heading level="2">More information</heading>
</para><para><itemizedlist>
<listitem><para><ulink url="http://www.cypress.com">Cypress Semiconductor, an Infineon Technologies Company</ulink></para></listitem><listitem><para><ulink url="https://github.com/cypresssemiconductorco">Cypress Semiconductor GitHub</ulink></para></listitem><listitem><para><ulink url="https://www.cypress.com/products/modustoolbox-software-environment">ModusToolbox</ulink> <hruler />
 © Cypress Semiconductor Corporation, 2020. </para></listitem></itemizedlist>
</para>    </detaileddescription>
  </compounddef>
</doxygen>