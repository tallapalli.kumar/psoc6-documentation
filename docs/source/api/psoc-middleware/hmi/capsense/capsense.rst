=========
CapSense
=========

.. raw:: html

   <script type="text/javascript">
   window.location.href = "CapSense Middleware/CapSense_Middleware.html"
   </script>

.. toctree::
   :hidden:

   CapSense Middleware/CapSense_Middleware.rst
   ModusToolbox_CapSense_Configurator_Guide.rst
   ModusToolbox_CapSense_Tuner_Guide.rst
   CapSense_Custom_Scanning_Example.rst