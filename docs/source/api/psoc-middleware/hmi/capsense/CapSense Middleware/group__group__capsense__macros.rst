=======
Macros
=======

.. doxygengroup:: group_capsense_macros
   :project: capsense-hmi

.. toctree::

   group__group__capsense__macros__general.rst
   group__group__capsense__macros__settings.rst
   group__group__capsense__macros__pin.rst
   group__group__capsense__macros__process.rst
   group__group__capsense__macros__touch.rst
   group__group__capsense__macros__gesture.rst
   group__group__capsense__macros__miscellaneous.rst
   group__group__capsense__macros__bist.rst
   
   


