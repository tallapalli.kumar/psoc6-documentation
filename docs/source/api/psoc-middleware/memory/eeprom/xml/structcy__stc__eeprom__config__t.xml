<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="structcy__stc__eeprom__config__t" kind="struct" language="C++" prot="public">
    <compoundname>cy_stc_eeprom_config_t</compoundname>
    <includes local="no">cy_em_eeprom.h</includes>
      <sectiondef kind="public-attrib">
      <memberdef id="structcy__stc__eeprom__config__t_1ad41b3c5ce4619d484a648a5a4bcc0bad" kind="variable" mutable="no" prot="public" static="no">
        <type>uint32_t</type>
        <definition>uint32_t cy_stc_eeprom_config_t::eepromSize</definition>
        <argsstring />
        <name>eepromSize</name>
        <briefdescription>
<para>The size of data in bytes to store in the Em_EEPROM. </para>        </briefdescription>
        <detaileddescription>
<para>The size is rounded up to a full Em_EEPROM row size. The row size is specific for a device family. Refer to the specific PSoC device datasheet.<linebreak />
 Note this size is often smaller than the total amount of flash used for the Em_EEPROM storage. The Em_EEPROM storage size depends on the Em_EEPROM configuration and can be bigger because increasing flash endurance (wear-leveling) and restoring corrupted data from a redundant copy. Refer to the <ref kindref="member" refid="index_1section_em_eeprom_location">Em_EEPROM Location</ref> section for size calculation equations. For convenience, use the <ref kindref="member" refid="group__group__em__eeprom__macros_1gaedb7ebd5ea6efe79173c6582003420fb">CY_EM_EEPROM_GET_PHYSICAL_SIZE</ref> macro to get the needed storage size depending on the configuration. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" bodystart="823" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="823" />
      </memberdef>
      <memberdef id="structcy__stc__eeprom__config__t_1a69bd4219df4e0b5563332c8b271cfa73" kind="variable" mutable="no" prot="public" static="no">
        <type>uint32_t</type>
        <definition>uint32_t cy_stc_eeprom_config_t::simpleMode</definition>
        <argsstring />
        <name>simpleMode</name>
        <briefdescription>
<para>Simple mode, when enabled (1 - enabled, 0 - disabled), means no additional service information is stored by the Em_EEPROM middleware like checksums, headers, a number of writes, etc. </para>        </briefdescription>
        <detaileddescription>
<para>Data is stored directly by the specified address. The size of Em_EEPROM storage is equal to the number of byte specified in the eepromSize parameter rounded up to a full row size <ref kindref="member" refid="group__group__em__eeprom__macros_1gafcfe832fb11038d572c8616962459e51">CY_EM_EEPROM_FLASH_SIZEOF_ROW</ref>. The wear leveling and redundant copy features are disabled, i.e. wearLevelingFactor and redundantCopy parameters are ignored. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" bodystart="836" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="836" />
      </memberdef>
      <memberdef id="structcy__stc__eeprom__config__t_1a19b32e2183e33f98f02e15e2bce4f207" kind="variable" mutable="no" prot="public" static="no">
        <type>uint32_t</type>
        <definition>uint32_t cy_stc_eeprom_config_t::wearLevelingFactor</definition>
        <argsstring />
        <name>wearLevelingFactor</name>
        <briefdescription>
<para>The higher the factor is, the more flash is used, but a higher number of erase/write cycles can be done on Em_EEPROM. </para>        </briefdescription>
        <detaileddescription>
<para>Multiply this number by the datasheet write endurance spec to determine the max of write cycles.<linebreak />
 The amount of wear leveling from 1 to 10. 1 means no wear leveling is used. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" bodystart="845" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="845" />
      </memberdef>
      <memberdef id="structcy__stc__eeprom__config__t_1ad6d96546ec661d1600f92bbeb9f3fb5d" kind="variable" mutable="no" prot="public" static="no">
        <type>uint8_t</type>
        <definition>uint8_t cy_stc_eeprom_config_t::redundantCopy</definition>
        <argsstring />
        <name>redundantCopy</name>
        <briefdescription>
<para>If enabled (1 - enabled, 0 - disabled), a checksum (stored in a row) is calculated on each row of data, while a redundant copy of Em_EEPROM is stored in another location. </para>        </briefdescription>
        <detaileddescription>
<para>When data is read, first the checksum is checked. If that checksum is bad, and the redundant copy's checksum is good, the copy is restored. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" bodystart="854" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="854" />
      </memberdef>
      <memberdef id="structcy__stc__eeprom__config__t_1a8f6acdb1ccef76931219fdf19032188d" kind="variable" mutable="no" prot="public" static="no">
        <type>uint8_t</type>
        <definition>uint8_t cy_stc_eeprom_config_t::blockingWrite</definition>
        <argsstring />
        <name>blockingWrite</name>
        <briefdescription>
<para>If enabled (1 - enabled, 0 - disabled), the blocking writes to flash are used in the design. </para>        </briefdescription>
        <detaileddescription>
<para>Otherwise, non-blocking flash writes are used. From the user's perspective, the behavior of blocking and non-blocking writes are the same - the difference is that the non-blocking writes do not block the interrupts. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" bodystart="863" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="863" />
      </memberdef>
      <memberdef id="structcy__stc__eeprom__config__t_1a686ebc626fadcdce3ffc40406f707f49" kind="variable" mutable="no" prot="public" static="no">
        <type>uint32_t</type>
        <definition>uint32_t cy_stc_eeprom_config_t::userFlashStartAddr</definition>
        <argsstring />
        <name>userFlashStartAddr</name>
        <briefdescription>
<para>The address of the flash storage. </para>        </briefdescription>
        <detaileddescription>
<para>The storage start address is filled to the Emulated EEPROM configuration structure and then the structure is passed to the <ref kindref="member" refid="group__group__em__eeprom__functions_1gab9f608d788f1a854b10c652c3bb00fdd">Cy_Em_EEPROM_Init()</ref> function. </para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="-1" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" bodystart="871" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="871" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>Em_EEPROM configuration structure. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
    <location bodyend="872" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" bodystart="808" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-emeeprom/cy_em_eeprom.h" line="809" />
    <listofallmembers>
      <member prot="public" refid="structcy__stc__eeprom__config__t_1a8f6acdb1ccef76931219fdf19032188d" virt="non-virtual"><scope>cy_stc_eeprom_config_t</scope><name>blockingWrite</name></member>
      <member prot="public" refid="structcy__stc__eeprom__config__t_1ad41b3c5ce4619d484a648a5a4bcc0bad" virt="non-virtual"><scope>cy_stc_eeprom_config_t</scope><name>eepromSize</name></member>
      <member prot="public" refid="structcy__stc__eeprom__config__t_1ad6d96546ec661d1600f92bbeb9f3fb5d" virt="non-virtual"><scope>cy_stc_eeprom_config_t</scope><name>redundantCopy</name></member>
      <member prot="public" refid="structcy__stc__eeprom__config__t_1a69bd4219df4e0b5563332c8b271cfa73" virt="non-virtual"><scope>cy_stc_eeprom_config_t</scope><name>simpleMode</name></member>
      <member prot="public" refid="structcy__stc__eeprom__config__t_1a686ebc626fadcdce3ffc40406f707f49" virt="non-virtual"><scope>cy_stc_eeprom_config_t</scope><name>userFlashStartAddr</name></member>
      <member prot="public" refid="structcy__stc__eeprom__config__t_1a19b32e2183e33f98f02e15e2bce4f207" virt="non-virtual"><scope>cy_stc_eeprom_config_t</scope><name>wearLevelingFactor</name></member>
    </listofallmembers>
  </compounddef>
</doxygen>