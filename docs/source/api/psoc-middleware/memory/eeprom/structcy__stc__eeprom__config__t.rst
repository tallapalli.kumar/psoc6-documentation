==============================
cy_stc_eeprom_config_t Struct
==============================

.. doxygenstruct:: cy_stc_eeprom_config_t
   :project: eeprom

Public Attribute
-----------------

.. doxygenvariable:: eepromSize
   :project: eeprom

.. doxygenvariable:: simpleMode
   :project: eeprom

.. doxygenvariable:: wearLevelingFactor
   :project: eeprom

.. doxygenvariable:: redundantCopy
   :project: eeprom

.. doxygenvariable:: blockingWrite
   :project: eeprom

.. doxygenvariable:: userFlashStartAddr
   :project: eeprom
