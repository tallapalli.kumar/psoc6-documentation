==============================================
ModusToolbox Device Firmware Update Host Tool
==============================================


Introduction
============

The Device Firmware Update (DFU) Host tool is a stand-alone program
provided with the ModusToolbox software. This tool is used to
communicate with a PSoC\ :sup:`®` 6 MCU that has already been programmed
with an application that includes device firmware update capability.
This tool is provided as a `graphical user interface
(GUI) <#gui-description>`__ and a `command-line interface
(CLI) <#cli-description>`__.

.. note::
   The process of initializing the device, and the two CPUs
   therein, as well as executing code in the SROM and supervisory flash, is
   more accurately referred to as “bootloading.” The process of installing
   and updating applications in the field, using standard communication
   channels (UART, I\ :sup:`2`\ C, USB, etc.) to download the new
   application from a host, is more accurately referred to as “device
   firmware update.”

The DFU Host tool allows you to:

-  Program new application data onto a PSoC 6 MCU

-  Verify the program data that is already contained on the device

-  Erase the application from the device

-  Select a \*.cyacd2 file output from ModusToolbox for programming

-  Abort an operation

.. note::
   This operation leaves the device in whatever state it is in
   when the abort message is acted upon.

The DFU Host tool supports communicating via UART, I\ :sup:`2`\ C, and
SPI. For UART, communication can be done directly from the PC simply by
connecting an appropriate cable. For I\ :sup:`2`\ C and SPI, PSoC 6 kits
include the KitProg3 firmware module, which implements USB-UART,
USB-I\ :sup:`2`\ C, and USB-SPI bridges. You will be able to see all
devices available for a connection, configure their settings, and view
the status of all programming.

Walkthrough
===========

This section contains a basic walkthrough using the DFU Host tool.

1. First, make sure you have a PSoC MCU that has been programmed with an
   application that includes DFU capability.

2. `Launch the DFU Host Tool GUI <#launch-the-dfu-host-tool-gui>`__.

3. In the `graphical DFU Host Tool <#main-window>`__, click **File >
   Open** to browse to the location of your application file (*.cyacd).

4. Connect a hardware port that supports the communication selected in
   your bootloader project.

5. Wire the hardware port pins to the corresponding pins on the target
   device.

6. Update the `port configuration <#port-configuration>`__. These values
   are set from the communication component used by the bootloader
   component.

After updating configuration information, click **Program** to load the
new application.

Launch the DFU Host Tool GUI
============================

You can launch the DFU Host tool as a GUI with or without the Eclipse
IDE for ModusToolbox. You can also run the tool from the command line.

Launch without the Eclipse IDE
------------------------------

To run the DFU Host tool GUI without the Eclipse IDE, navigate to the
install location and run the executable. On Windows, the default install
location for the DFU Host tool is:

*[user_home]/ModusToolbox_<version>/tools_<version>/dfuh-tool*

For other operating systems, the installation directory will vary, based
on how the software was installed.

Launch with the Eclipse IDE
---------------------------

If your Eclipse IDE application is suitable to use the DFU Host tool,
right-click on the appropriate project and select **ModusToolbox >
Device Firmware Update Tool**.

|image0|

From the Command Line
---------------------

Refer to `CLI Description <#cli-description>`__ to run the *dfuh-cli*
tool. You can also run the DFU Host Tool GUI executable using the
following command line arguments:

+--------------------+------------------------------------------------+
| -?, -h, --help     | Displays information about all valid           |
|                    | command-line arguments and exits.              |
+--------------------+------------------------------------------------+
| -v, --version      | Displays version information and exits.        |
+--------------------+------------------------------------------------+
| --debug <filename> | Appends detailed debugging information to the  |
|                    | specified file.                                |
+--------------------+------------------------------------------------+

GUI Description
===============

This section describes the various parts of the DFU Host tool GUI.

Main window
===========

|image1|

File Selection
--------------

Use this section to select the .cyacd2 file that contains the DFU image.

Ports
-----

This is a list of all the ports attached to the computer that can be
used to communicate with the bootloader. Based on which item is
selected, different options will be available for the Port
Configuration.

Filters
-------

The **Filters** button allows for restricting which ports are displayed
in the ports list.

|image2|

Port Configuration
------------------

This section allows for configuring the interface-specific options for
communicating with the DFU system. This is necessary to ensure both the
DFU and host computer are configured the same.

Refer to the appropriate probe documentation for a list of supported
modes.

I\ :sup:`2`\ C
~~~~~~~~~~~~~~

For I\ :sup:`2`\ C communication, there are two pieces of required
information:

|image3|

-  The address of the I\ :sup:`2`\ C-based target DFU system with which
   the host is communicating. The range for valid addresses is from 8
   -120.

-  The I\ :sup:`2`\ C SCK signal frequency.

SPI
~~~

For SPI communication, there are three pieces of required information:

|image4|

-  The SPI SCLK signal frequency.

-  The bit ordering of transferred data.

-  The SPI operating mode.

UART
~~~~

For UART communication, there are four pieces of required information:

|image5|

-  The baud (bit) rate at which data is transferred.

-  The number of data bits per byte.

-  The number of stop bits indicating the termination of a byte.

-  The parity bit that is added to a byte.

Log
---

The log displays a history of what has happened so far while the Host
has been open. It displays information about operations initiated by the
user, and when items started/completed. It also displays the error
messages in case of an error during an operation.

Errors
------

Any errors for various fields show as a red X in the field containing
the error, and it contains a tooltip when you hover the mouse cursor on
it.

CLI Description
===============

In addition to the dfuh-tool GUI executable, there is also a dfuh-cli
executable. The CLI allows programming, verifying, and erasing of
devices from a command-line prompt or from within batch files or shell
scripts. The exit code for the dfuh-cli executable is zero if the
operation is successful, or non-zero if the operation encounters an
error.

In order to use the dfuh-cli executable, you must provide exactly one of
the following flags:

+--------------------------------+------------------------------------+
| --program-device <cyacd2_file> | Program the device with the        |
|                                | specified file and exit.           |
+--------------------------------+------------------------------------+
| --verify-device <cyacd2_file>  | Verify the programming of the      |
|                                | device with the specified file and |
|                                | exit.                              |
+--------------------------------+------------------------------------+
| --erase-device <cyacd2_file>   | Erase the specified program from   |
|                                | the device and exit.               |
+--------------------------------+------------------------------------+

If there is more than one device connected to the host, use the
following flag to specify which device to use:

+-----------------+---------------------------------------------------+
| --hwid <string> | Specifies the ID of the hardware to               |
|                 | program/verify/erase. If this option is skipped,  |
|                 | the first appropriate device found will be used.  |
+-----------------+---------------------------------------------------+

In addition, you must provide the appropriate configuration values for
exactly one of the following protocols:

The **I2C** flags are:

+---------------------+-----------------------------------------------+
| --i2c-address <int> | Sets the address for the I2C protocol. Valid  |
|                     | values are between 8 (0x08) and 120 (0x78).   |
+---------------------+-----------------------------------------------+
| --i2c-speed <int>   | Sets the speed for the I2C protocol in kHz.   |
|                     | Common values are 50, 100, 400 and 1000.      |
+---------------------+-----------------------------------------------+

The **SPI** flags are:

+--------------------------+------------------------------------------+
| --spi-clockspeed <float> | Sets the clock speed for the SPI         |
|                          | protocol in MHz.                         |
+--------------------------+------------------------------------------+
| --spi-mode <int>         | Sets the mode for the SPI protocol in    |
|                          | binary. Valid values are 00, 01, 10 and  |
|                          | 11.                                      |
+--------------------------+------------------------------------------+
| --spi-lsb-first          | Specifies that the least-significant bit |
|                          | should be sent first for the SPI         |
|                          | protocol. Otherwise, the                 |
|                          | most-significant bit will be sent first. |
+--------------------------+------------------------------------------+

The **UART** flags are:

+----------------------------+----------------------------------------+
| --uart-baudrate <int>      | Sets the baud rate for the UART        |
|                            | protocol.                              |
+----------------------------+----------------------------------------+
| --uart-databits <int>      | Sets the number of data bits for the   |
|                            | UART protocol.                         |
+----------------------------+----------------------------------------+
| --uart-paritytype <string> | Sets the parity type for the UART      |
|                            | protocol. Valid strings are “None”,    |
|                            | “Odd” and “Even”.                      |
+----------------------------+----------------------------------------+
| --uart-stopbits <float>    | Sets the stop bits for the UART        |
|                            | protocol. Valid values are 1, 1.5 and  |
|                            | 2.                                     |
+----------------------------+----------------------------------------+

Command-Line Flags
------------------

The following flags change the overall functioning of the tool:

+----------------+----------------------------------------------------+
| -?, -h, --help | Displays information about all valid command-line  |
|                | arguments and exits.                               |
+----------------+----------------------------------------------------+
| -v, --version  | Displays version information and exits.            |
+----------------+----------------------------------------------------+
| --debug        | Outputs debugging information to the terminal      |
|                | running the CLI tool during programming, verifying |
|                | or erasing.                                        |
+----------------+----------------------------------------------------+
| --display-hw   | Outputs all compatible hardware attached to the    |
|                | computer and exits.                                |
+----------------+----------------------------------------------------+

CLI Example
-----------

The following shows a simple example for using the dfu-cli executable

dfuh-cli --program-device test_app.cyacd2 --i2c-address 8 --i2c-speed
100

Troubleshooting
===============

+----------------------------------+----------------------------------+
|    **Problem**                   |    **Workaround**                |
+==================================+==================================+
| On common Linux distributions,   | An easy way to allow the current |
| the serial UART ports (usually   | user access to the Linux         |
| /dev/ttySx or /dev/ttyUSBx       | machine's serial ports is by     |
| devices) belong to the root user | adding the user to the dialout   |
| and to the dialout group.        | group. This can be done using    |
| Standard users are not allowed   | the following command:           |
| to access these devices.         |                                  |
|                                  | $sudo usermod -a -G dialout      |
|                                  | $USER                            |
|                                  |                                  |
|                                  | .. note::                        |
|                                  |                                  |
|                                  |    For this command to           |
|                                  |    take effect, you must log out |
|                                  |    and then log back in.         |
+----------------------------------+----------------------------------+
| On Linux, attempts to set up or  | To enable DFU Host tool          |
| start DFU Host tool              | communication under Linux,       |
| communication causes the tool to | install the udev rules for       |
| close without any messages.      | KitProg3:                        |
|                                  |                                  |
|                                  | Disconnect the KitProg device.   |
|                                  |                                  |
|                                  | Execute in the terminal (root    |
|                                  | access required):                |
|                                  |                                  |
|                                  |    sh                            |
|                                  |    $CYSDK/tools/fw-lo            |
|                                  | ader/udev_rules/install_rules.sh |
|                                  |                                  |
|                                  | Reconnect the KitProg device.    |
+----------------------------------+----------------------------------+
| On common Linux distributions,   | Refer to the “Installation       |
| the DFU Host tool forbids        | Procedure on Ubuntu Linux (x64)” |
| communication protocol selection | section in the *Cypress          |
| after re-plugging KitProg during | Programmer 2.1 CLI User Guide*.  |
| communication.                   |                                  |
+----------------------------------+----------------------------------+
| KitProg3 UART is accessible but  | -  Use a third-party UART to USB |
| not able to read data on Linux   |    bridge.                       |
| kernel 4.15 and above or Mac OS  |                                  |
| X 10.13 and above.               | -  Update the KitProg3 firmware  |
|                                  |    to version 1.11.243 or above. |
+----------------------------------+----------------------------------+

References
==========

Refer to the following documents for more information, as needed:

-  Device Datasheets

-  Device Technical Reference Manuals

-  KitProg3 User Guide

Version Changes
===============

This section lists and describes the changes for each version of this
tool.

+---------+-----------------------------------------------------------+
| Version | Change Descriptions                                       |
+=========+===========================================================+
| 1.0     | New tool.                                                 |
+---------+-----------------------------------------------------------+
| 1.1     | Added Notice List.                                        |
|         |                                                           |
|         | Added command-line interface (CLI) and handling of        |
|         | invalid command line arguments.                           |
|         |                                                           |
|         | Added logging for firmware update process.                |
+---------+-----------------------------------------------------------+
| 1.2     | Removed Notice List.                                      |
+---------+-----------------------------------------------------------+

.. |image0| image:: ../../../_static/image/api/psoc-middleware/memory/ModusToolbox-DFU-Host-Tool/dfu-host1.png
.. |image1| image:: ../../../_static/image/api/psoc-middleware/memory/ModusToolbox-DFU-Host-Tool/dfu-host2.png
.. |image2| image:: ../../../_static/image/api/psoc-middleware/memory/ModusToolbox-DFU-Host-Tool/dfu-host3.png
.. |image3| image:: ../../../_static/image/api/psoc-middleware/memory/ModusToolbox-DFU-Host-Tool/dfu-host4.png
.. |image4| image:: ../../../_static/image/api/psoc-middleware/memory/ModusToolbox-DFU-Host-Tool/dfu-host5.png
.. |image5| image:: ../../../_static/image/api/psoc-middleware/memory/ModusToolbox-DFU-Host-Tool/dfu-host6.png
