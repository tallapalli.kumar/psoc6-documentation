<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.14" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="cy__serial__flash__prog_8c" kind="file" language="C++">
    <compoundname>cy_serial_flash_prog.c</compoundname>
    <includes local="no">stdint.h</includes>
    <incdepgraph>
      <node id="0">
        <label>cy_serial_flash_prog.c</label>
        <link refid="cy__serial__flash__prog_8c" />
        <childnode refid="1" relation="include">
        </childnode>
      </node>
      <node id="1">
        <label>stdint.h</label>
      </node>
    </incdepgraph>
    <briefdescription>
<para>Provides variables necessary to inform programming tools how to program the attached serial flash memory. </para>    </briefdescription>
    <detaileddescription>
<para>The variables used here must be placed at specific locations in order to be detected and used by programming tools to know that there is an attached memory and its characteristics. Uses the configuration provided as part of BSP.</para><para><simplesect kind="copyright"><para>Copyright 2018-2020 Cypress Semiconductor Corporation SPDX-License-Identifier: Apache-2.0</para></simplesect>
Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at <verbatim>http://www.apache.org/licenses/LICENSE-2.0
</verbatim></para><para>Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License. </para>    </detaileddescription>
    <programlisting>
<codeline lineno="1"><highlight class="comment">/***************************************************************************/</highlight></codeline>
<codeline lineno="42"><highlight class="preprocessor">#include<sp />&lt;stdint.h&gt;</highlight><highlight class="normal" /></codeline>
<codeline lineno="43"><highlight class="normal" /></codeline>
<codeline lineno="44"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="45"><highlight class="normal" /><highlight class="keyword">extern</highlight><highlight class="normal"><sp /></highlight><highlight class="stringliteral">"C"</highlight><highlight class="normal"><sp />{</highlight></codeline>
<codeline lineno="46"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight><highlight class="normal" /></codeline>
<codeline lineno="47"><highlight class="normal" /></codeline>
<codeline lineno="48"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(CY_ENABLE_XIP_PROGRAM)</highlight><highlight class="normal" /></codeline>
<codeline lineno="49"><highlight class="normal" /></codeline>
<codeline lineno="50"><highlight class="normal" /><highlight class="preprocessor">#include<sp />"cycfg_qspi_memslot.h"</highlight><highlight class="normal" /></codeline>
<codeline lineno="51"><highlight class="normal" /></codeline>
<codeline lineno="52"><highlight class="normal" /><highlight class="keyword">typedef</highlight><highlight class="normal"><sp /></highlight><highlight class="keyword">struct</highlight><highlight class="normal" /></codeline>
<codeline lineno="53"><highlight class="normal">{</highlight></codeline>
<codeline lineno="54"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />cy_stc_smif_block_config_t<sp />*<sp />smifCfg;<sp /></highlight><highlight class="comment">/*<sp />Pointer<sp />to<sp />SMIF<sp />top-level<sp />configuration<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="55"><highlight class="normal"><sp /><sp /><sp /><sp /></highlight><highlight class="keyword">const</highlight><highlight class="normal"><sp />uint32_t<sp />null_t;<sp /></highlight><highlight class="comment">/*<sp />NULL<sp />termination<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="56"><highlight class="normal">}<sp />stc_smif_ipblocks_arr_t;</highlight></codeline>
<codeline lineno="57"><highlight class="normal" /></codeline>
<codeline lineno="58"><highlight class="normal" /><highlight class="comment">/*</highlight></codeline>
<codeline lineno="59"><highlight class="comment"><sp />*<sp />This<sp />data<sp />can<sp />be<sp />placed<sp />anywhere<sp />in<sp />the<sp />internal<sp />memory,<sp />but<sp />it<sp />must<sp />be<sp />at<sp />a<sp />location<sp />that</highlight></codeline>
<codeline lineno="60"><highlight class="comment"><sp />*<sp />can<sp />be<sp />determined<sp />and<sp />used<sp />for<sp />the<sp />calculation<sp />of<sp />the<sp />CRC16<sp />checksum<sp />in<sp />the<sp />cyToc<sp />below.<sp />There</highlight></codeline>
<codeline lineno="61"><highlight class="comment"><sp />*<sp />are<sp />multiple<sp />ways<sp />this<sp />can<sp />be<sp />accomplished<sp />including:</highlight></codeline>
<codeline lineno="62"><highlight class="comment"><sp />*<sp />1)<sp />Placing<sp />it<sp />in<sp />a<sp />dedicated<sp />memory<sp />block<sp />with<sp />a<sp />known<sp />address.<sp />(as<sp />done<sp />here)</highlight></codeline>
<codeline lineno="63"><highlight class="comment"><sp />*<sp />2)<sp />Placing<sp />it<sp />at<sp />an<sp />absolute<sp />location<sp />via<sp />a<sp />the<sp />linker<sp />script</highlight></codeline>
<codeline lineno="64"><highlight class="comment"><sp />*<sp />3)<sp />Using<sp />'cymcuelftool<sp />-S'<sp />to<sp />recompute<sp />the<sp />checksum<sp />and<sp />patch<sp />the<sp />elf<sp />file<sp />after<sp />linking</highlight></codeline>
<codeline lineno="65"><highlight class="comment"><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="66"><highlight class="normal">CY_SECTION(</highlight><highlight class="stringliteral">".cy_sflash_user_data"</highlight><highlight class="normal">)<sp />__attribute__(<sp />(used)<sp />)</highlight></codeline>
<codeline lineno="67"><highlight class="normal">const<sp />stc_smif_ipblocks_arr_t<sp />smifIpBlocksArr<sp />=<sp />{&amp;smifBlockConfig,<sp />0x00000000};</highlight></codeline>
<codeline lineno="68"><highlight class="normal" /></codeline>
<codeline lineno="69"><highlight class="normal" /><highlight class="comment">/*</highlight></codeline>
<codeline lineno="70"><highlight class="comment"><sp />*<sp />This<sp />data<sp />is<sp />used<sp />to<sp />populate<sp />the<sp />table<sp />of<sp />contents<sp />part<sp />2.<sp />When<sp />present,<sp />it<sp />is<sp />used<sp />by<sp />the<sp />boot</highlight></codeline>
<codeline lineno="71"><highlight class="comment"><sp />*<sp />process<sp />and<sp />programming<sp />tools<sp />to<sp />determine<sp />key<sp />characteristics<sp />about<sp />the<sp />memory<sp />usage<sp />including</highlight></codeline>
<codeline lineno="72"><highlight class="comment"><sp />*<sp />where<sp />the<sp />boot<sp />process<sp />should<sp />start<sp />the<sp />application<sp />from<sp />and<sp />what<sp />external<sp />memories<sp />are<sp />connected</highlight></codeline>
<codeline lineno="73"><highlight class="comment"><sp />*<sp />(if<sp />any).<sp />This<sp />must<sp />consume<sp />a<sp />full<sp />row<sp />of<sp />flash<sp />memory<sp />row.<sp />The<sp />last<sp />entry<sp />is<sp />a<sp />checksum<sp />of<sp />the</highlight></codeline>
<codeline lineno="74"><highlight class="comment"><sp />*<sp />other<sp />values<sp />in<sp />the<sp />ToC<sp />which<sp />must<sp />be<sp />updated<sp />if<sp />any<sp />other<sp />value<sp />changes.<sp />This<sp />can<sp />be<sp />done<sp />manually</highlight></codeline>
<codeline lineno="75"><highlight class="comment"><sp />*<sp />or<sp />by<sp />running<sp />'cymcuelftool<sp />-S'<sp />to<sp />recompute<sp />the<sp />checksum.</highlight></codeline>
<codeline lineno="76"><highlight class="comment"><sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="77"><highlight class="normal">CY_SECTION(</highlight><highlight class="stringliteral">".cy_toc_part2"</highlight><highlight class="normal">)<sp />__attribute__(<sp />(used)<sp />)</highlight></codeline>
<codeline lineno="78"><highlight class="normal">const<sp />uint32_t<sp />cyToc[128]<sp />=</highlight></codeline>
<codeline lineno="79"><highlight class="normal">{</highlight></codeline>
<codeline lineno="80"><highlight class="normal"><sp /><sp /><sp /><sp />0x200-4,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Offset=0x0000:<sp />Object<sp />Size,<sp />bytes<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="81"><highlight class="normal"><sp /><sp /><sp /><sp />0x01211220,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Offset=0x0004:<sp />Magic<sp />Number<sp />(TOC<sp />Part<sp />2,<sp />ID)<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="82"><highlight class="normal"><sp /><sp /><sp /><sp />0,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Offset=0x0008:<sp />Key<sp />Storage<sp />Address<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="83"><highlight class="normal"><sp /><sp /><sp /><sp />(int)&amp;smifIpBlocksArr,<sp /><sp /></highlight><highlight class="comment">/*<sp />Offset=0x000C:<sp />This<sp />points<sp />to<sp />a<sp />null<sp />terminated<sp />array<sp />of<sp />SMIF<sp />structures.<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="84"><highlight class="normal"><sp /><sp /><sp /><sp />0x10000000u,<sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Offset=0x0010:<sp />App<sp />image<sp />start<sp />address<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="85"><highlight class="normal"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Offset=0x0014-0x01F7:<sp />Reserved<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="86"><highlight class="normal"><sp /><sp /><sp /><sp />[126]<sp />=<sp /><sp />0x000002C2,<sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Offset=0x01F8:<sp />Bits[<sp />1:<sp />0]<sp />CLOCK_CONFIG<sp />(0=8MHz,<sp />1=25MHz,<sp />2=50MHz,<sp />3=100MHz)</highlight></codeline>
<codeline lineno="87"><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Bits[<sp />4:<sp />2]<sp />LISTEN_WINDOW<sp />(0=20ms,<sp />1=10ms,<sp />2=1ms,<sp />3=0ms,<sp />4=100ms)</highlight></codeline>
<codeline lineno="88"><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Bits[<sp />6:<sp />5]<sp />SWJ_PINS_CTL<sp />(0/1/3=Disable<sp />SWJ,<sp />2=Enable<sp />SWJ)</highlight></codeline>
<codeline lineno="89"><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Bits[<sp />8:<sp />7]<sp />APP_AUTHENTICATION<sp />(0/2/3=Enable,<sp />1=Disable)</highlight></codeline>
<codeline lineno="90"><highlight class="comment"><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp /><sp />Bits[10:<sp />9]<sp />FB_BOOTLOADER_CTL:<sp />UNUSED<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="91"><highlight class="normal"><sp /><sp /><sp /><sp />[127]<sp />=<sp /><sp />0x3BB30000<sp /><sp /><sp /><sp /><sp /></highlight><highlight class="comment">/*<sp />Offset=0x01FC:<sp />CRC16-CCITT<sp />(the<sp />upper<sp />2<sp />bytes<sp />contain<sp />the<sp />CRC<sp />and<sp />the<sp />lower<sp />2<sp />bytes<sp />are<sp />0)<sp />*/</highlight><highlight class="normal" /></codeline>
<codeline lineno="92"><highlight class="normal">};</highlight></codeline>
<codeline lineno="93"><highlight class="normal" /></codeline>
<codeline lineno="94"><highlight class="normal" /><highlight class="preprocessor">#endif<sp /></highlight><highlight class="comment">/*<sp />defined(CY_ENABLE_XIP_PROGRAM)<sp />*/</highlight><highlight class="preprocessor" /><highlight class="normal" /></codeline>
<codeline lineno="95"><highlight class="normal" /></codeline>
<codeline lineno="96"><highlight class="normal" /><highlight class="preprocessor">#if<sp />defined(__cplusplus)</highlight><highlight class="normal" /></codeline>
<codeline lineno="97"><highlight class="normal">}</highlight></codeline>
<codeline lineno="98"><highlight class="normal" /><highlight class="preprocessor">#endif</highlight></codeline>
    </programlisting>
    <location file="output/libs/COMPONENT_SERIAL_FLASH/serial-flash/cy_serial_flash_prog.c" />
  </compounddef>
</doxygen>