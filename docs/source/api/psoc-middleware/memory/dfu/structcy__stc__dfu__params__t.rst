=====================================
cy_stc_dfu_params_t Struct Reference
=====================================

Public Attributes
-----------------


.. doxygenvariable:: dataBuffer
   :project: dfu

.. doxygenvariable:: dataOffset
   :project: dfu

.. doxygenvariable:: packetBuffer
   :project: dfu

.. doxygenvariable:: timeout
   :project: dfu

.. doxygenvariable:: appId
   :project: dfu

.. doxygenvariable:: appVerified
   :project: dfu

.. doxygenvariable:: initCtl
   :project: dfu

.. doxygenstruct:: cy_stc_dfu_params_t
   :project: dfu


