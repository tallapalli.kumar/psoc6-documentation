=====================
Sensing
=====================

.. raw:: html

   <script type="text/javascript">
   window.location.href = "thermistor/thermistor.html"
   </script>

.. toctree::
   :hidden:

   thermistor/thermistor.rst
   capsense/CapSense.rst  
   csdadc/csdadc.rst
   light-sensor/index.rst
   csdidac/csdidac.rst
   sensor-motion-bmi160/sensor-motion-bmi160.rst
   sensor-atmo-bme680/sensor-atmo-bme680.rst 