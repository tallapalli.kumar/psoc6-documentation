============================================
BLE Common Definitions and Data Structures
============================================

.. doxygengroup:: group_ble_common_api_definitions_section
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__common__macros__error.rst  
   group__group__ble__common__api__macros__section.rst
   group__group__ble__common__api__data__struct__section.rst
   
   