<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ble__service__api___b_c_s__client" kind="group">
    <compoundname>group_ble_service_api_BCS_client</compoundname>
    <title>BCS Client Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___b_c_s__client_1gad0a2743c95f78874784b269a3e439ca8" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_BCSC_GetCharacteristicValue</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_bcs_char_index_t charIndex)</argsstring>
        <name>Cy_BLE_BCSC_GetCharacteristicValue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1ga6ca7f1f0e332c7464fab8ae62fcf775f">cy_en_ble_bcs_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <briefdescription>
<para>This function is used to read a characteristic value, which is a value identified by charIndex, from the server. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a Body Composition service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1ga6ca7f1f0e332c7464fab8ae62fcf775f">cy_en_ble_bcs_char_index_t</ref>. The valid values are,<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1gga6ca7f1f0e332c7464fab8ae62fcf775faa65e6b55c6eb94bac2bd9aa7d8350c82">CY_BLE_BCS_BODY_COMPOSITION_FEATURE</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1gga6ca7f1f0e332c7464fab8ae62fcf775fa874119d845801a3a41b992eb64e7ec85">CY_BLE_BCS_BODY_COMPOSITION_MEASUREMENT</ref></para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>If execution is successful (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>), these events can appear: <linebreak />
If a BCS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___b_c_s__server__client_1gabb6351313978c2bd0bd466693dabcc5d">Cy_BLE_BCS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a05933b6061b46a53c589a7266228d54b">CY_BLE_EVT_BCSC_READ_CHAR_RESPONSE</ref> - If the requested attribute is successfully read on the peer device, the details (charIndex , value, etc.) are provided with an event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__bcs__char__value__t">cy_stc_ble_bcs_char_value_t</ref>.</para></listitem></itemizedlist>
</para></simplesect>
Otherwise (if a BCS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - If the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with the event parameters structure (<ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>).</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with the event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1048" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bcs.c" bodystart="1011" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bcs.h" line="169" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___b_c_s__client_1gada9cc19e68ddf3dc88999a0eb4bc4e1c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_BCSC_SetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_bcs_char_index_t charIndex, cy_en_ble_bcs_descr_index_t descrIndex, uint8_t attrSize, uint8_t *attrValue)</argsstring>
        <name>Cy_BLE_BCSC_SetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1ga6ca7f1f0e332c7464fab8ae62fcf775f">cy_en_ble_bcs_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1gacf02ee6ae663720e6b8ad0511688bb56">cy_en_ble_bcs_descr_index_t</ref></type>
          <declname>descrIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>attrSize</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>attrValue</declname>
        </param>
        <briefdescription>
<para>This function is used to write the characteristic descriptor to the server identified by charIndex and descrIndex. </para>        </briefdescription>
        <detaileddescription>
<para>Internally, a Write Request is sent to the GATT server and on successful execution of the request on the server side, the following events can be generated:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a04afb4c9a9b7307f993fb794d80015b7">CY_BLE_EVT_BCSS_INDICATION_ENABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51ab923d19e1642a3a1ec92522fcfb77a27">CY_BLE_EVT_BCSS_INDICATION_DISABLED</ref></para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a Body Composition service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1ga6ca7f1f0e332c7464fab8ae62fcf775f">cy_en_ble_bcs_char_index_t</ref>. The valid values are,<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1gga6ca7f1f0e332c7464fab8ae62fcf775faa65e6b55c6eb94bac2bd9aa7d8350c82">CY_BLE_BCS_BODY_COMPOSITION_FEATURE</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1gga6ca7f1f0e332c7464fab8ae62fcf775fa874119d845801a3a41b992eb64e7ec85">CY_BLE_BCS_BODY_COMPOSITION_MEASUREMENT</ref> </para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic descriptor of type <ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1gacf02ee6ae663720e6b8ad0511688bb56">cy_en_ble_bcs_descr_index_t</ref>. The valid value is,<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1ggacf02ee6ae663720e6b8ad0511688bb56a6777cd549fb1b78f10452b3afcf98e75">CY_BLE_BCS_CCCD</ref> </para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the characteristic value attribute. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrValue</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the characteristic descriptor value data that should be sent to the server device.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The state is not valid. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>If execution is successful (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>), these events can appear: <linebreak />
If a BCS service-specific callback is registered (with <ref kindref="member" refid="group__group__ble__service__api___b_c_s__server__client_1gabb6351313978c2bd0bd466693dabcc5d">Cy_BLE_BCS_RegisterAttrCallback</ref>):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a26c043d147a423731f52d6d9d7035a8f">CY_BLE_EVT_BCSC_WRITE_DESCR_RESPONSE</ref> - If the requested attribute is successfully written on the peer device, the details (charIndex, descrIndex etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__bcs__descr__value__t">cy_stc_ble_bcs_descr_value_t</ref>.</para></listitem></itemizedlist>
</para></simplesect>
Otherwise (if a BCS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9a5f078b557f3991ed1356d08139b094">CY_BLE_EVT_GATTC_WRITE_RSP</ref> - If the requested attribute is successfully written on the peer device.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with an event parameter structure ( <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>). </para></listitem></itemizedlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1152" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bcs.c" bodystart="1106" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bcs.h" line="172" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___b_c_s__client_1gaf25bb65e3c191afe9f11fbf9a10a1c23" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_BCSC_GetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_bcs_char_index_t charIndex, cy_en_ble_bcs_descr_index_t descrIndex)</argsstring>
        <name>Cy_BLE_BCSC_GetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1ga6ca7f1f0e332c7464fab8ae62fcf775f">cy_en_ble_bcs_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1gacf02ee6ae663720e6b8ad0511688bb56">cy_en_ble_bcs_descr_index_t</ref></type>
          <declname>descrIndex</declname>
        </param>
        <briefdescription>
<para>Sends a request to get the characteristic descriptor of the specified characteristic of the service. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a Body Composition service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1ga6ca7f1f0e332c7464fab8ae62fcf775f">cy_en_ble_bcs_char_index_t</ref>. The valid values are,<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1gga6ca7f1f0e332c7464fab8ae62fcf775faa65e6b55c6eb94bac2bd9aa7d8350c82">CY_BLE_BCS_BODY_COMPOSITION_FEATURE</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1gga6ca7f1f0e332c7464fab8ae62fcf775fa874119d845801a3a41b992eb64e7ec85">CY_BLE_BCS_BODY_COMPOSITION_MEASUREMENT</ref> </para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic descriptor of type <ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1gacf02ee6ae663720e6b8ad0511688bb56">cy_en_ble_bcs_descr_index_t</ref>. The valid value is,<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api___b_c_s__definitions_1ggacf02ee6ae663720e6b8ad0511688bb56a6777cd549fb1b78f10452b3afcf98e75">CY_BLE_BCS_CCCD</ref></para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;This operation is not permitted on the specified attribute. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The state is not valid. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>If execution is successful (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>), these events can appear: <linebreak />
If a BCS service-specific callback is registered (with <ref kindref="member" refid="group__group__ble__service__api___b_c_s__server__client_1gabb6351313978c2bd0bd466693dabcc5d">Cy_BLE_BCS_RegisterAttrCallback</ref>):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51acbd7549397728fe99ce9b5be2381e4ce">CY_BLE_EVT_BCSC_READ_DESCR_RESPONSE</ref> - If the requested attribute is successfully read on the peer device, the details (charIndex, descrIndex, value, etc.) are provided with an event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__bcs__descr__value__t">cy_stc_ble_bcs_descr_value_t</ref>.</para></listitem></itemizedlist>
</para></simplesect>
Otherwise (if a BCS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - If the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with the event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with the event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1242" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bcs.c" bodystart="1202" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bcs.h" line="177" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>API unique to BCS designs configured as a GATT Client role. </para>    </briefdescription>
    <detaileddescription>
<para>A letter 'c' is appended to the API name: Cy_BLE_BCSC_ </para>    </detaileddescription>
  </compounddef>
</doxygen>