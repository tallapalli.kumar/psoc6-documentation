<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ble__service__api___b_a_s__client" kind="group">
    <compoundname>group_ble_service_api_BAS_client</compoundname>
    <title>BAS Client Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___b_a_s__client_1ga5216baab36e7e6a08c6e842fcf56e854" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_BASC_GetCharacteristicValue</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, uint8_t serviceIndex, cy_en_ble_bas_char_index_t charIndex)</argsstring>
        <name>Cy_BLE_BASC_GetCharacteristicValue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>serviceIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___b_a_s__definitions_1gabf7f92662570bc150b786e9a483f03e2">cy_en_ble_bas_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <briefdescription>
<para>This function is used to read the characteristic value from the server identified by charIndex. </para>        </briefdescription>
        <detaileddescription>
<para>This function call can result in generation of the following events based on a response from the server device:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a3e2874660d469de56f8fe13419e6c479">CY_BLE_EVT_BASC_READ_CHAR_RESPONSE</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref></para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The BLE peer device connection handle.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>serviceIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service instance; e.g. if two Battery services are supported in your design, then the first service will be identified by serviceIndex of 0 and the second service by serviceIndex of 1.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___b_a_s__definitions_1gabf7f92662570bc150b786e9a483f03e2">cy_en_ble_bas_char_index_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The Read Request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>If execution is successful (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>), these events can appear: <linebreak />
 If the BAS service-specific callback is registered (with <ref kindref="member" refid="group__group__ble__service__api___b_a_s__server__client_1ga27afae675db5f9d2fc33392af3fd9037">Cy_BLE_BAS_RegisterAttrCallback</ref>):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a3e2874660d469de56f8fe13419e6c479">CY_BLE_EVT_BASC_READ_CHAR_RESPONSE</ref> - If the requested attribute is successfully read on the peer device, the details (charIndex, value, etc.) are provided with an event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__bas__char__value__t">cy_stc_ble_bas_char_value_t</ref>.</para></listitem></itemizedlist>
</para></simplesect>
Otherwise (if a BAS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - If the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with the event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with the event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="796" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bas.c" bodystart="755" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bas.h" line="171" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___b_a_s__client_1gadf672c32644d1032545ceaebfcb1528d" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_BASC_SetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, uint8_t serviceIndex, cy_en_ble_bas_char_index_t charIndex, cy_en_ble_bas_descr_index_t descrIndex, uint8_t attrSize, uint8_t *attrValue)</argsstring>
        <name>Cy_BLE_BASC_SetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>serviceIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___b_a_s__definitions_1gabf7f92662570bc150b786e9a483f03e2">cy_en_ble_bas_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___b_a_s__definitions_1ga7e2dc8a450ffbc696df656b46a1b0a51">cy_en_ble_bas_descr_index_t</ref></type>
          <declname>descrIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>attrSize</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>attrValue</declname>
        </param>
        <briefdescription>
<para>Sends a request to set a characteristic descriptor of the specified Battery service characteristic on the server device. </para>        </briefdescription>
        <detaileddescription>
<para>Internally, a Write Request is sent to the GATT Server and on successful execution of the request on the server side, the following events can be generated:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a3719af364201788465711c263599528d">CY_BLE_EVT_BASS_NOTIFICATION_ENABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a21571d04639e008aedcfa499905c1fff">CY_BLE_EVT_BASS_NOTIFICATION_DISABLED</ref></para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The BLE peer device connection handle.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>serviceIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service instance; e.g. if two Battery services are supported in your design, then the first service will be identified by serviceIndex of 0 and the second service by serviceIndex of 1.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___b_a_s__definitions_1gabf7f92662570bc150b786e9a483f03e2">cy_en_ble_bas_char_index_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service characteristic descriptor of type <ref kindref="member" refid="group__group__ble__service__api___b_a_s__definitions_1ga7e2dc8a450ffbc696df656b46a1b0a51">cy_en_ble_bas_descr_index_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the characteristic descriptor attribute.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrValue</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the characteristic descriptor value data that should be sent to the server device.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para><para><simplesect kind="par"><title>Events</title><para>If execution is successful (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>), these events can appear: <linebreak />
 If a BAS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___b_a_s__server__client_1ga27afae675db5f9d2fc33392af3fd9037">Cy_BLE_BAS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51ab5426d85d8037bd799d97db13fdbc77d">CY_BLE_EVT_BASC_WRITE_DESCR_RESPONSE</ref> - If the requested attribute is successfully written on the peer device, the details (charIndex, descrIndex, etc.) are provided with an event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__bas__descr__value__t">cy_stc_ble_bas_descr_value_t</ref>.</para></listitem></itemizedlist>
</para></simplesect>
Otherwise (if a BAS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9a5f078b557f3991ed1356d08139b094">CY_BLE_EVT_GATTC_WRITE_RSP</ref> - If the requested attribute is successfully written on the peer device.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with an event parameter structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="909" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bas.c" bodystart="862" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bas.h" line="174" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___b_a_s__client_1gafa54e1f2f2dc41097ed52421bd1dce40" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_BASC_GetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, uint8_t serviceIndex, cy_en_ble_bas_char_index_t charIndex, cy_en_ble_bas_descr_index_t descrIndex)</argsstring>
        <name>Cy_BLE_BASC_GetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>serviceIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___b_a_s__definitions_1gabf7f92662570bc150b786e9a483f03e2">cy_en_ble_bas_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___b_a_s__definitions_1ga7e2dc8a450ffbc696df656b46a1b0a51">cy_en_ble_bas_descr_index_t</ref></type>
          <declname>descrIndex</declname>
        </param>
        <briefdescription>
<para>Sends a request to get a characteristic descriptor of the specified Battery service characteristic from the server device. </para>        </briefdescription>
        <detaileddescription>
<para>This function call can result in generation of the following events based on a response from the server device:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a0575fdc2a77d45de208d30c8da7b9c42">CY_BLE_EVT_BASC_READ_DESCR_RESPONSE</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref></para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The BLE peer device connection handle.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>serviceIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service instance; e.g. if two Battery services are supported in your design, then the first service will be identified by serviceIndex of 0 and the second service by serviceIndex of 1.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___b_a_s__definitions_1gabf7f92662570bc150b786e9a483f03e2">cy_en_ble_bas_char_index_t</ref>.</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of a service characteristic descriptor of type <ref kindref="member" refid="group__group__ble__service__api___b_a_s__definitions_1ga7e2dc8a450ffbc696df656b46a1b0a51">cy_en_ble_bas_descr_index_t</ref>.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>If execution is successful (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>), these events can appear: <linebreak />
 If a BAS service-specific callback is registered (with <ref kindref="member" refid="group__group__ble__service__api___b_a_s__server__client_1ga27afae675db5f9d2fc33392af3fd9037">Cy_BLE_BAS_RegisterAttrCallback</ref>):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a0575fdc2a77d45de208d30c8da7b9c42">CY_BLE_EVT_BASC_READ_DESCR_RESPONSE</ref> - If the requested attribute is successfully read on the peer device, the details (charIndex, descrIndex, value, etc.) are provided with an event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__bas__descr__value__t">cy_stc_ble_bas_descr_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if a BAS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - If the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with the event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with the event parameters structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1011" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bas.c" bodystart="967" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_bas.h" line="179" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>API unique to BAS designs configured as a GATT Client role. </para>    </briefdescription>
    <detaileddescription>
<para>A letter 'c' is appended to the API name: Cy_BLE_BASC_ </para>    </detaileddescription>
  </compounddef>
</doxygen>