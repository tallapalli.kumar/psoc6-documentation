<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="page_ble_general" kind="group">
    <compoundname>page_ble_general</compoundname>
    <title>General Description</title>
    <detaileddescription>
<para>The Bluetooth Low Energy (BLE) middleware contains a comprehensive API to configure the BLE Stack and the underlying chip hardware.</para><para>The standalone BT Configurator is shipped in ModusToolbox to make it easy to configure PSoC 6 BLE Middleware.</para><para>Include cy_ble_common.h and cy_ble_event_handler.h to get access to all functions and other declarations in this library. If you are using the ModusToolbox BT Configurator, you can include cycfg_ble.h only.</para><para>The PSoC 6 BLE Middleware incorporates a Bluetooth Core Specification v5.0 compliant protocol stack. You may access the GAP, GATT and L2CAP layers of the stack using the API.</para><para>The API are broadly categorized as follows:<itemizedlist>
<listitem><para><ref kindref="compound" refid="group__group__ble__common__api">BLE Common API</ref></para></listitem><listitem><para><ref kindref="compound" refid="group__group__ble__service__api">BLE Service-Specific API</ref></para></listitem></itemizedlist>
</para><para><bold>Features:</bold><itemizedlist>
<listitem><para>Multi-link supports for up to four simultaneous connections in any combination of roles</para></listitem><listitem><para>Bluetooth v5.0 compliant protocol stack<itemizedlist>
<listitem><para>LE 2 Mbps</para></listitem></itemizedlist>
</para></listitem><listitem><para>Generic Access Profile (GAP) features<itemizedlist>
<listitem><para>Broadcaster, Observer, Peripheral and Central roles</para></listitem><listitem><para>User-defined advertising data</para></listitem><listitem><para>Bonding support for up to 128 devices</para></listitem><listitem><para>Security modes 1 and 2</para></listitem></itemizedlist>
</para></listitem><listitem><para>Generic Attribute Profile (GATT) features<itemizedlist>
<listitem><para>GATT Client and Server</para></listitem><listitem><para>16-, 32-, and 128-bit UUIDs</para></listitem></itemizedlist>
</para></listitem><listitem><para>Special Interest Group (SIG) adopted GATT-based Profiles and Services, and quick prototype of new profile design through intuitive BT Configurator Custom Profile development;</para></listitem><listitem><para>Security Manager features<itemizedlist>
<listitem><para>Pairing methods: Just works, Passkey Entry, Out of Band, Numeric Comparison (LE Secure connections)</para></listitem><listitem><para>Authenticated man-in-the-middle (MITM) protection and data signing</para></listitem></itemizedlist>
</para></listitem><listitem><para>Logical Link Control and Adaption Protocol (L2CAP) connection-oriented channel</para></listitem><listitem><para>Link Layer (LL) features<itemizedlist>
<listitem><para>Master and Slave roles</para></listitem><listitem><para>128-bit AES encryption</para></listitem><listitem><para>Low Duty Cycle Advertising</para></listitem><listitem><para>LE Ping</para></listitem><listitem><para>Privacy 1.2</para></listitem><listitem><para>Data length extension (DLE)</para></listitem></itemizedlist>
</para></listitem></itemizedlist>
</para>
<para><heading level="1">When to use the BLE</heading></para>
<para>BLE is used in very low-power network and Internet of Things (IoT) solutions using low-cost battery operated devices that can quickly connect and form simple wireless links. Target applications include HID, remote controls, sports and fitness monitors, portable medical devices and smart phone accessories, among many others.</para>

<para><heading level="1">SIG adopted Profiles and Services</heading></para>
<para>The BLE resource supports numerous SIG-adopted GATT-based Profiles and Services. Each of these can be configured for either a GATT Client or GATT Server. It generates all the necessary code for a particular Profile/Service operation, as configured in the BLE Configurator.</para>

<para><heading level="1">Custom Profiles</heading></para>
<para>You can create custom Profiles that use existing Services, and you can create custom Services with custom Characteristics and Descriptors.</para>

<para><heading level="1">Debug Support</heading></para>
<para>For testing and debugging, the PSoC 6 BLE Middleware can be configured to use Host Controller Interface (HCI) mode through a UART. Refer to Initialize and Enable PSoC 6 BLE Middleware in Controller-only Mode (HCI over Software API).</para><para>For over-the-air verification, the Cypress CySmart Central Emulation Tool can be used for generic Bluetooth host stack emulation.</para>

<para><heading level="1">PSoC 6 BLE Middleware Architecture</heading></para>
<para> </para><para>PSoC 6 BLE Middleware consists of the BLE Stack, BLE Profile, Hardware Abstraction Layer (HAL), and the Link Layer. The following figure shows the high-level architecture of the PSoC 6 BLE Middleware, illustrating the relationship between each of the layers and how the application interacts with the PSoC 6 BLE Middleware. Note that the application uses callback functions to handle BLE events, which you can use to build your state machine.</para><para>The following block diagram shows the architecture of PSoC 6 BLE Middleware under Complete BLE Protocol mode. <image name="ble_architecture_single_cpu_mode.png" type="html" />
</para><para>The following block diagram shows the architecture of PSoC 6 BLE Middleware under Controller Only mode (HCI over Software API). <image name="ble_architecture_hci_mode.png" type="html" />
</para><para>The following sub-sections give an overview of each of these layers.</para>
<para><bold>BLE Stack</bold></para>
<para /><para>The BLE stack implements the core BLE functionality as defined in Bluetooth Core Specification 5.0. The stack is included as a pre-compiled library in the PSoC 6 BLE Middleware library. The BLE Stack implements the layered architecture of the BLE protocol stack as shown in the following figure.</para><para><image name="ble_stack.png" type="html" />
</para>
<para><bold>Generic Access Profile (GAP)</bold></para>
<para>The Generic Access Profile defines the procedures related to discovery of Bluetooth devices and the link- management aspects of connecting to Bluetooth devices. In addition, this profile includes common format requirements for parameters accessible on the user interface level.</para><para>The Generic Access Profile defines the following roles when operating over the LE physical channel:<itemizedlist>
<listitem><para><bold>Broadcaster role</bold>: A device operating in the Broadcaster role can send advertising events. It is referred to as a Broadcaster. It has a transmitter and may have a receiver.</para></listitem><listitem><para><bold>Observer role</bold>: A device operating in the Observer role is a device that receives advertising events. It is referred to as an Observer. It has a receiver and may have a transmitter.</para></listitem><listitem><para><bold>Peripheral role</bold>: A device that accepts an LE physical link using any of the connection establishment procedures is said to have a Peripheral role. A device operating in the Peripheral role will be in the "Slave role" in the Link Layer Connection State. A device operating in the Peripheral role is referred to as a Peripheral. A Peripheral has both a transmitter and a receiver.</para></listitem><listitem><para><bold>Central role</bold>: A device that supports the Central role initiates the establishment of a physical connection. A device operating in the "Central role" will be in the "Master role" in the Link Layer Connection. A device operating in the Central role is referred to as a Central. A Central has a transmitter and a receiver.</para></listitem></itemizedlist>
</para>

<para><bold>Generic Attribute Profile (GATT)</bold></para>
<para>The Generic Attribute Profile defines a generic service framework using the ATT protocol layer. This framework defines the procedures and formats of services and their characteristics. It defines the procedures for Service, Characteristic, and Descriptor discovery, reading, writing, notifying, and indicating Characteristics, as well as configuring the broadcast of Characteristics. GATT roles are:</para><para><itemizedlist>
<listitem><para><bold>GATT Client</bold>: This is the device that wants data. It initiates commands and requests to the GATT Server. It can receive responses, indications, and notifications data sent by the GATT Server.</para></listitem><listitem><para><bold>GATT Server</bold>: This is the device that has the data and accepts incoming commands and requests from the GATT Client. It sends responses, indications, and notifications to a GATT Client. The BLE Stack can support both roles simultaneously.</para></listitem></itemizedlist>
</para>

<para><bold>Attribute Protocol (ATT)</bold></para>
<para>The Attribute Protocol layer defines a Client/Server architecture above the BLE logical transport channel. The attribute protocol allows a device referred to as the GATT Server to expose a set of attributes and their associated values to a peer device referred to as the GATT Client. Attributes exposed by the GATT Server can be discovered, read, and written by a GATT Client, and can be indicated and notified by the GATT Server. All the transactions on attributes are atomic.</para>

<para><bold>Security Manager Protocol (SMP)</bold></para>
<para>The Security Manager Protocol defines the procedures and behavior to manage pairing, authentication, and encryption between the devices. These include:<itemizedlist>
<listitem><para>Encryption and Authentication</para></listitem><listitem><para>Pairing and Bonding<itemizedlist>
<listitem><para>Pass Key and Out of Band bonding</para></listitem></itemizedlist>
</para></listitem><listitem><para>Key Generation for a device identity resolution, data signing, and encryption</para></listitem><listitem><para>Pairing method selection based on the IO capability of the GAP central and GAP peripheral device.</para></listitem></itemizedlist>
</para>

<para><bold>Logical Link Control Adaptation Protocol (L2CAP)</bold></para>
<para>L2CAP provides a connectionless data channel. LE L2CAP provides the following features:<itemizedlist>
<listitem><para>Channel multiplexing, which manages three fixed channels. Two channels are dedicated for higher protocol layers like ATT, SMP. One channel is used by the LE-L2CAP protocol signalling channel for its own use.</para></listitem><listitem><para>Segmentation and reassembly of packets whose size is up to the BLE Controller managed maximum packet size.</para></listitem><listitem><para>Connection-oriented channel over a specific application registered using the PSM (protocol service multiplexer) channel. It implements credit-based flow control between two LE L2CAP entities. This feature can be used for BLE applications that require transferring large chunks of data.</para></listitem></itemizedlist>
</para>

<para><bold>Host Controller Interface (HCI)</bold></para>
<para>The HCI layer implements a command, event, and data interface to allow link layer access from upper layers such as GAP, L2CAP, and SMP.</para>

<para><bold>Link Layer (LL)</bold></para>
<para>The Link Layer implements a command, event, and data interface to allow link layer access from upper layers such as GAP, L2CAP, and SMP. The LL protocol manages the physical BLE connections between devices. It supports all LL states such as Advertising, Scanning, Initiating, and Connecting (Master and Slave). It implements all the key link control procedures such as LE Encryption, LE Connection Update, LE Channel Update, and LE Ping.</para><para>The Link Layer is a hardware-firmware co-implementation, where the key time critical LL functions are implemented in the LL hardware. The LL firmware maintains and controls the key LL procedure state machines. It supports all the BLE chip-specific low-power modes. The BLE Stack is a pre-compiled library. The appropriate configuration of the BLE Stack library is linked during a build process based on the application. The BLE Stack libraries are Arm Embedded Application Binary Interface (EABI) compliant and are compiled using Arm compiler version 5.03.</para>


<para><bold>Profile Layer</bold></para>
<para /><para>In BLE, data is organized into concepts called Profiles, Services, and Characteristics.</para><para><itemizedlist>
<listitem><para>A <bold>Profile</bold> describes how devices connect to each other to find and use Services. The Profile describes the type of application and the general expected behavior of that device. See the <bold>Parameter Configuration</bold> section of ModusToolbox Bluetooth Configurator Guide for configuring the PSoC 6 BLE Middleware.</para></listitem><listitem><para>A <bold>Service</bold> is a collection of data entities called Characteristics. A Service is used to define a certain function in a Profile. A Service may also define its relationship to other Services. A Service is assigned a Universally Unique Identifier (UUID). This is 16 bits for SIG adopted Services and 128 bits for custom Services. See the <bold>GATT Settings Tab</bold> section of ModusToolbox Bluetooth Configurator Guide for information about adding Services to a Profile.</para></listitem><listitem><para>A <bold>Characteristic</bold> contains a Value and the Descriptor that describes a Characteristic Value. It is an attribute type for a specific piece of information within a Service. Like a Service, each Characteristic is designated with a UUID; 16 bits for SIG adopted Characteristics and 128 bits for custom Characteristics. See the <bold>GATT Settings Tab</bold> section of ModusToolbox Bluetooth Configurator Guide for information about adding Characteristics and Descriptors. The following diagram shows the relationship between Profiles, Services, and Characteristics in a sample BLE heart rate monitor application using a Heart Rate Profile.</para></listitem></itemizedlist>
</para><para><image name="ble_profiles.png" type="html" />
</para><para>The Heart Rate Profile contains a Heart Rate Service and a Device Information Service. Within the Heart Rate Service, there are three Characteristics, each containing different information. The device in the diagram is configured in the Sensor role, meaning that in the context of the Heart Rate Profile, the device is a GAP Peripheral and a GATT Server. These concepts are explained in the <ref kindref="member" refid="page_ble_general_1group_ble_stack">BLE Stack</ref> description.</para><para>The Profile layer is provided by PSoC 6 BLE Middleware using the parameter configurations specified in the Bluetooth Configurator. The Profile implements the Profile-specific attribute database and functions required for the application. You can choose to configure the standard SIG adopted Profile and generate a design, or define a Custom Profile required by an application.</para>

<para><bold>Hardware Abstraction Layer (HAL)</bold></para>
<para>The HAL implements the interface between the BLE stack and the underlying hardware. This layer is meant for the stack only and it is not advisable to modify it. </para>

    </detaileddescription>
  </compounddef>
</doxygen>