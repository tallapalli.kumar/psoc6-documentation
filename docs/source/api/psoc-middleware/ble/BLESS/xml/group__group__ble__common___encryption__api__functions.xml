<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ble__common___encryption__api__functions" kind="group">
    <compoundname>group_ble_common_Encryption_api_functions</compoundname>
    <title>AES Engine API</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ble__common___encryption__api__functions_1gaab640e0317548c6bf4c1351142f0cc77" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_GenerateRandomNumber</definition>
        <argsstring>(cy_stc_ble_stack_random_num_param_t *param)</argsstring>
        <name>Cy_BLE_GenerateRandomNumber</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__stack__random__num__param__t">cy_stc_ble_stack_random_num_param_t</ref> *</type>
          <declname>param</declname>
        </param>
        <briefdescription>
<para>This function generates 8-byte random number that complies with pseudo random number generation in accordance with [FIPS PUB 140-2]. </para>        </briefdescription>
        <detaileddescription>
<para>Random number generation function is used during security procedures documented in Bluetooth 5.0 core specification, Volume 3, Part H. The function accepts an application-specific seed for the DRBG (Deterministic Random number generator) function.</para><para>Generated random number is informed through event 'CY_BLE_EVT_RANDOM_NUM_GEN_COMPLETE'</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>param</parametername>
</parameternamelist>
<parameterdescription>
<para>parameter is of type '<ref kindref="compound" refid="structcy__stc__ble__stack__random__num__param__t">cy_stc_ble_stack_random_num_param_t</ref>' param-&gt;seed: Seed for DRBG. Setting the seed to zero is functionally equivalent to not setting the application-specific seed.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_en_ble_api_result_t : Return value indicates whether the function succeeded or failed. Following are the possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Errors codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;If Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INSUFFICIENT_RESOURCES &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;If BLE Stack resources are unavailable. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack_host_main.h" line="2142" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common___encryption__api__functions_1ga22c36883181ed9f987d1d00d040db77a" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_AesEncrypt</definition>
        <argsstring>(cy_stc_ble_aes_encrypt_info_t *param)</argsstring>
        <name>Cy_BLE_AesEncrypt</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__aes__encrypt__info__t">cy_stc_ble_aes_encrypt_info_t</ref> *</type>
          <declname>param</declname>
        </param>
        <briefdescription>
<para>This function does AES encryption on the given plain text data using the given 128-bit key. </para>        </briefdescription>
        <detaileddescription>
<para>The output of AES processing is copied to encryptedData buffer. Refer to Bluetooth 5.0 core specification, Volume 3, Part H, section 2.2 for more details on usage of AES key.</para><para>Completion of the function is informed through the 'CY_BLE_EVT_AES_ENCRYPT_COMPLETE' event. A (uint8_t*) type parameter is returned along with the event. This is the pointer to the encrypted data (128-bit).</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>param</parametername>
</parameternamelist>
<parameterdescription>
<para>Parameter of type '<ref kindref="compound" refid="structcy__stc__ble__aes__encrypt__info__t">cy_stc_ble_aes_encrypt_info_t</ref>'. param-&gt;plainData: Pointer to the data containing plain text (128-bit) that is to be encrypted.</para></parameterdescription>
</parameteritem>
</parameterlist>
param-&gt;aesKey: Pointer to the AES Key (128-bit) that is to be used for AES encryption.</para><para><simplesect kind="return"><para>cy_en_ble_api_result_t : Return value indicates whether the function succeeded or failed. Following are the possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Errors codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On specifying NULL as input parameter. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;If Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INSUFFICIENT_RESOURCES &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;AES engine is being used internally or BLE Stack resources are unavailable. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack_host_main.h" line="2177" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common___encryption__api__functions_1ga0660b291935b6bceb24cf8412aed126c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_AesCcmEncrypt</definition>
        <argsstring>(cy_stc_ble_aes_ccm_encrypt_info_t *param)</argsstring>
        <name>Cy_BLE_AesCcmEncrypt</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__aes__ccm__encrypt__info__t">cy_stc_ble_aes_ccm_encrypt_info_t</ref> *</type>
          <declname>param</declname>
        </param>
        <briefdescription>
<para>This function does AES-CCM encryption on the given data using the given 16-bit key and 13-bit nonce. </para>        </briefdescription>
        <detaileddescription>
<para>Encrypted data is informed through the (cy_stc_ble_aes_ccm_param_t*) type parameter returned along with the 'CY_BLE_EVT_AES_CCM_ENCRYPT_COMPLETE' event. This parameter consists of the MIC value generated during encryption along with a pointer to the encrypted data.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>param</parametername>
</parameternamelist>
<parameterdescription>
<para>Parameter is of type '<ref kindref="compound" refid="structcy__stc__ble__aes__ccm__encrypt__info__t">cy_stc_ble_aes_ccm_encrypt_info_t</ref> '</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_en_ble_api_result_t: Return value indicates whether the function succeeded or failed. Following are the possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Errors codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;One of the inputs is a null pointer. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;If Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INSUFFICIENT_RESOURCES &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;If BLE Stack resources are unavailable. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack_host_main.h" line="2208" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common___encryption__api__functions_1ga4b2d3e4920f8dbd95a3e6b47e5b3ee68" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_AesCcmDecrypt</definition>
        <argsstring>(cy_stc_ble_aes_ccm_decrypt_info_t *param)</argsstring>
        <name>Cy_BLE_AesCcmDecrypt</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__aes__ccm__decrypt__info__t">cy_stc_ble_aes_ccm_decrypt_info_t</ref> *</type>
          <declname>param</declname>
        </param>
        <briefdescription>
<para>This function decrypts the given AES-CCM encrypted data. </para>        </briefdescription>
        <detaileddescription>
<para>It requires the 16-bit key, and 13-bit nonce along with the 4-bit MIC value generated during AES CCM encryption.</para><para>Decrypted data is informed through the (cy_stc_ble_aes_ccm_param_t*) type parameter returned along with the 'CY_BLE_EVT_AES_CCM_DECRYPT_COMPLETE' event.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>param</parametername>
</parameternamelist>
<parameterdescription>
<para>Parameter is of type '<ref kindref="compound" refid="structcy__stc__ble__aes__ccm__decrypt__info__t">cy_stc_ble_aes_ccm_decrypt_info_t</ref> '</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_en_ble_api_result_t: Return value indicates whether the function succeeded or failed. Following are the possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;One of the inputs is a NULL pointer. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INSUFFICIENT_RESOURCES &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;BLE Stack resources are unavailable. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack_host_main.h" line="2238" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__common___encryption__api__functions_1ga295e57c36affb3dd429c2ccd4200b47c" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_GenerateAesCmac</definition>
        <argsstring>(cy_stc_ble_aes_cmac_generate_param_t *cmacGenParam)</argsstring>
        <name>Cy_BLE_GenerateAesCmac</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__aes__cmac__generate__param__t">cy_stc_ble_aes_cmac_generate_param_t</ref> *</type>
          <declname>cmacGenParam</declname>
        </param>
        <briefdescription>
<para>This function enables the application to generate the AES CMAC of 16 bytes for a given variable length message and CMAC Key. </para>        </briefdescription>
        <detaileddescription>
<para>If the function call resulted in CY_BLE_SUCCESS, then, CY_BLE_EVT_AES_CMAC_GEN_COMPLETE event indicates completion of cmac generation.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>cmacGenParam</parametername>
</parameternamelist>
<parameterdescription>
<para>Parameter is of type '<ref kindref="compound" refid="structcy__stc__ble__aes__cmac__generate__param__t">cy_stc_ble_aes_cmac_generate_param_t</ref> ' cmacGenParam-&gt;mac: is a pointer to the generated cmac value. Application can get the generated cmac value from this location after completion of CMAC generation. </para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>cy_en_ble_api_result_t: Return value indicates whether the function succeeded or failed. Following are the possible error codes.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;On successful operation. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;One of the inputs is a NULL pointer, the 'buffer' can be NULL only if 'size' is zero. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Application has already initiated AES CMAC operation that is pending completion. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim></para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_stack_host_main.h" line="2267" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>BLE sub system AES Engine is exposed through this API. </para>    </briefdescription>
    <detaileddescription>
    </detaileddescription>
  </compounddef>
</doxygen>