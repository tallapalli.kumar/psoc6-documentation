<doxygen xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.8.13" xsi:noNamespaceSchemaLocation="compound.xsd">
  <compounddef id="group__group__ble__service__api___c_g_m_s__client" kind="group">
    <compoundname>group_ble_service_api_CGMS_client</compoundname>
    <title>CGMS Client Functions</title>
      <sectiondef kind="func">
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_g_m_s__client_1gaf65927c05aee7a72c467857da9616a4e" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CGMSC_SetCharacteristicValue</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cgms_char_index_t charIndex, uint8_t attrSize, uint8_t *attrValue)</argsstring>
        <name>Cy_BLE_CGMSC_SetCharacteristicValue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1gace05588f3d5103a95036db6fc9db696a">cy_en_ble_cgms_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>attrSize</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>attrValue</declname>
        </param>
        <briefdescription>
<para>This function is used to write the characteristic (which is identified by charIndex) value attribute in the server. </para>        </briefdescription>
        <detaileddescription>
<para>As a result a Write Request is sent to the GATT Server and on successful execution of the request on the server side, the <ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51ab41225eeab17ea2238db2836ec927523">CY_BLE_EVT_CGMSS_WRITE_CHAR</ref> event is generated. On successful request execution on the server side, the Write Response is sent to the client.</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1gace05588f3d5103a95036db6fc9db696a">cy_en_ble_cgms_char_index_t</ref>. The valid values are<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aafabb2ecedcc6dbe4709c8870e6ecd2ce">CY_BLE_CGMS_CGMT</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aaceda1d84e6ea0efebc4566e938e8a06d">CY_BLE_CGMS_CGFT</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aab28e1d800290bf91f0b1d7ea6c8b7155">CY_BLE_CGMS_CGST</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa4f57f0b4d891f3362b4b295b73ca8890">CY_BLE_CGMS_SSTM</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa903529f4eeda8618ab6aa814bcef40b1">CY_BLE_CGMS_SRTM</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa674855f309377bfd2f269397f551e5a6">CY_BLE_CGMS_RACP</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa9ca85049063417bfd963f4f69dda990f">CY_BLE_CGMS_SOCP</ref> </para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the characteristic value attribute. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrValue</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the characteristic value data that should be sent to the server device.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the CGMS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__server__client_1gab298f6cc6a31558f29fdaf6ae4680504">Cy_BLE_CGMS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51aa242168958d2b40afa4223b014ff5b0a">CY_BLE_EVT_CGMSC_WRITE_CHAR_RESPONSE</ref> - In case if the requested attribute is successfully written on the peer device, the details (charIndex , value, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cgms__char__value__t">cy_stc_ble_cgms_char_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if an CGMS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - If the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with an event parameter structure <ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with an event parameter structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="953" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cgms.c" bodystart="908" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cgms.h" line="193" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_g_m_s__client_1gae82b8f864bab53dab75e0cc4a424f773" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CGMSC_GetCharacteristicValue</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cgms_char_index_t charIndex)</argsstring>
        <name>Cy_BLE_CGMSC_GetCharacteristicValue</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1gace05588f3d5103a95036db6fc9db696a">cy_en_ble_cgms_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <briefdescription>
<para>This function is used to read the characteristic value from a server identified by charIndex. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1gace05588f3d5103a95036db6fc9db696a">cy_en_ble_cgms_char_index_t</ref>. The valid values are<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aafabb2ecedcc6dbe4709c8870e6ecd2ce">CY_BLE_CGMS_CGMT</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aaceda1d84e6ea0efebc4566e938e8a06d">CY_BLE_CGMS_CGFT</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aab28e1d800290bf91f0b1d7ea6c8b7155">CY_BLE_CGMS_CGST</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa4f57f0b4d891f3362b4b295b73ca8890">CY_BLE_CGMS_SSTM</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa903529f4eeda8618ab6aa814bcef40b1">CY_BLE_CGMS_SRTM</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa674855f309377bfd2f269397f551e5a6">CY_BLE_CGMS_RACP</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa9ca85049063417bfd963f4f69dda990f">CY_BLE_CGMS_SOCP</ref></para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the CGMS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__server__client_1gab298f6cc6a31558f29fdaf6ae4680504">Cy_BLE_CGMS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a98339dd0892fa03f2e7824395a855bef">CY_BLE_EVT_CGMSC_READ_CHAR_RESPONSE</ref> - In case if the requested attribute is successfully read on the peer device, the details (charIndex , value, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cgms__char__value__t">cy_stc_ble_cgms_char_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if an CGMS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - If the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with an event parameter structure <ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with an event parameter structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1049" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cgms.c" bodystart="1007" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cgms.h" line="197" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_g_m_s__client_1ga60982a3cfc4f73c43478e58aa32d0279" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CGMSC_SetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cgms_char_index_t charIndex, cy_en_ble_cgms_descr_index_t descrIndex, uint8_t attrSize, uint8_t *attrValue)</argsstring>
        <name>Cy_BLE_CGMSC_SetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1gace05588f3d5103a95036db6fc9db696a">cy_en_ble_cgms_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ga1ad127d87a318d913ea32b5a420dfc79">cy_en_ble_cgms_descr_index_t</ref></type>
          <declname>descrIndex</declname>
        </param>
        <param>
          <type>uint8_t</type>
          <declname>attrSize</declname>
        </param>
        <param>
          <type>uint8_t *</type>
          <declname>attrValue</declname>
        </param>
        <briefdescription>
<para>Sends a request to the peer device to set the characteristic descriptor of the specified characteristic of Continuous Glucose Monitoring service. </para>        </briefdescription>
        <detaileddescription>
<para>Internally, Write Request is sent to the GATT Server and on successful execution of the request on the server side, the following events can be generated:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a99b07092256b737142c64c4bbdabd64d">CY_BLE_EVT_CGMSS_INDICATION_ENABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a5fcf8d5e51aa0eaa1a4d6b7bacde40c0">CY_BLE_EVT_CGMSS_INDICATION_DISABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a971ea383a7a5d30999135e18ec41e7b4">CY_BLE_EVT_CGMSS_NOTIFICATION_ENABLED</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a8d572ac03554fa67f93a1dc725f68c40">CY_BLE_EVT_CGMSS_NOTIFICATION_DISABLED</ref></para></listitem></itemizedlist>
</para><para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1gace05588f3d5103a95036db6fc9db696a">cy_en_ble_cgms_char_index_t</ref>. The valid values are<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aafabb2ecedcc6dbe4709c8870e6ecd2ce">CY_BLE_CGMS_CGMT</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aaceda1d84e6ea0efebc4566e938e8a06d">CY_BLE_CGMS_CGFT</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aab28e1d800290bf91f0b1d7ea6c8b7155">CY_BLE_CGMS_CGST</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa4f57f0b4d891f3362b4b295b73ca8890">CY_BLE_CGMS_SSTM</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa903529f4eeda8618ab6aa814bcef40b1">CY_BLE_CGMS_SRTM</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa674855f309377bfd2f269397f551e5a6">CY_BLE_CGMS_RACP</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa9ca85049063417bfd963f4f69dda990f">CY_BLE_CGMS_SOCP</ref> </para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic descriptor of type <ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ga1ad127d87a318d913ea32b5a420dfc79">cy_en_ble_cgms_descr_index_t</ref>. The valid value is,<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1gga1ad127d87a318d913ea32b5a420dfc79a184b577535fa6d6e918bfeb6604fee33">CY_BLE_CGMS_CCCD</ref> </para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrSize</parametername>
</parameternamelist>
<parameterdescription>
<para>The size of the characteristic descriptor value attribute. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>attrValue</parametername>
</parameternamelist>
<parameterdescription>
<para>The pointer to the characteristic descriptor value data that should be sent to the server device.</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the CGMS service-specific callback is registered with <ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__server__client_1gab298f6cc6a31558f29fdaf6ae4680504">Cy_BLE_CGMS_RegisterAttrCallback()</ref>:<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a6afa0d77eceb9622ac022bdb842ed2cf">CY_BLE_EVT_CGMSC_WRITE_DESCR_RESPONSE</ref> - In case if the requested attribute is successfully written on the peer device, the details (charIndex, descr index etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cgms__descr__value__t">cy_stc_ble_cgms_descr_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if an CGMS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9a5f078b557f3991ed1356d08139b094">CY_BLE_EVT_GATTC_WRITE_RSP</ref> - If the requested attribute is successfully written on the peer device.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with an event parameter structure ( <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>). </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1162" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cgms.c" bodystart="1115" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cgms.h" line="200" />
      </memberdef>
      <memberdef const="no" explicit="no" id="group__group__ble__service__api___c_g_m_s__client_1ga67660698cf4d435f77852ba62492d262" inline="no" kind="function" prot="public" static="no" virt="non-virtual">
        <type><ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref></type>
        <definition>cy_en_ble_api_result_t Cy_BLE_CGMSC_GetCharacteristicDescriptor</definition>
        <argsstring>(cy_stc_ble_conn_handle_t connHandle, cy_en_ble_cgms_char_index_t charIndex, cy_en_ble_cgms_descr_index_t descrIndex)</argsstring>
        <name>Cy_BLE_CGMSC_GetCharacteristicDescriptor</name>
        <param>
          <type><ref kindref="compound" refid="structcy__stc__ble__conn__handle__t">cy_stc_ble_conn_handle_t</ref></type>
          <declname>connHandle</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1gace05588f3d5103a95036db6fc9db696a">cy_en_ble_cgms_char_index_t</ref></type>
          <declname>charIndex</declname>
        </param>
        <param>
          <type><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ga1ad127d87a318d913ea32b5a420dfc79">cy_en_ble_cgms_descr_index_t</ref></type>
          <declname>descrIndex</declname>
        </param>
        <briefdescription>
<para>Sends a request to the peer device to get the characteristic descriptor of the specified characteristic of Continuous Glucose Monitoring service. </para>        </briefdescription>
        <detaileddescription>
<para><parameterlist kind="param"><parameteritem>
<parameternamelist>
<parametername>connHandle</parametername>
</parameternamelist>
<parameterdescription>
<para>The connection handle. </para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>charIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic of type <ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1gace05588f3d5103a95036db6fc9db696a">cy_en_ble_cgms_char_index_t</ref>. The valid values are<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aafabb2ecedcc6dbe4709c8870e6ecd2ce">CY_BLE_CGMS_CGMT</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aaceda1d84e6ea0efebc4566e938e8a06d">CY_BLE_CGMS_CGFT</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aab28e1d800290bf91f0b1d7ea6c8b7155">CY_BLE_CGMS_CGST</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa4f57f0b4d891f3362b4b295b73ca8890">CY_BLE_CGMS_SSTM</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa903529f4eeda8618ab6aa814bcef40b1">CY_BLE_CGMS_SRTM</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa674855f309377bfd2f269397f551e5a6">CY_BLE_CGMS_RACP</ref></para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ggace05588f3d5103a95036db6fc9db696aa9ca85049063417bfd963f4f69dda990f">CY_BLE_CGMS_SOCP</ref> </para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
<parameteritem>
<parameternamelist>
<parametername>descrIndex</parametername>
</parameternamelist>
<parameterdescription>
<para>The index of the service characteristic descriptor of type <ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1ga1ad127d87a318d913ea32b5a420dfc79">cy_en_ble_cgms_descr_index_t</ref>. The valid value is,<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__definitions_1gga1ad127d87a318d913ea32b5a420dfc79a184b577535fa6d6e918bfeb6604fee33">CY_BLE_CGMS_CCCD</ref> </para></listitem></itemizedlist>
</para></parameterdescription>
</parameteritem>
</parameterlist>
<simplesect kind="return"><para>A return value of type <ref kindref="member" refid="group__group__ble__common__api__definitions_1ga63f6be7f53f5ad7d82c394e4cdfa619e">cy_en_ble_api_result_t</ref>.</para></simplesect>
<verbatim>embed:rst 
.. raw:: html

  &lt;table class="docutils align-default"&gt;&lt;tr&gt;&lt;td class="head"&gt;&lt;p&gt;Error Codes &lt;/p&gt;&lt;/td&gt;&lt;td class="head"&gt;&lt;p&gt;Description  &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_SUCCESS &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The request was sent successfully. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_PARAMETER &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Validation of the input parameter failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_OPERATION &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Operation is invalid for this characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_INVALID_STATE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Connection with the server is not established. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_MEMORY_ALLOCATION_FAILED &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;Memory allocation failed. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;&lt;p&gt;CY_BLE_ERROR_GATT_DB_INVALID_ATTR_HANDLE &lt;/p&gt;&lt;/td&gt;&lt;td&gt;&lt;p&gt;The peer device doesn't have the particular characteristic. &lt;/p&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</verbatim><simplesect kind="par"><title>Events</title><para>In case of successful execution (return value = <ref kindref="member" refid="group__group__ble__common__api__definitions_1gga63f6be7f53f5ad7d82c394e4cdfa619ea26e476ee364252903ce212deeb6ec064">CY_BLE_SUCCESS</ref>) the following events can appear: <linebreak />
 If the CGMS service-specific callback is registered (with <ref kindref="member" refid="group__group__ble__service__api___c_g_m_s__server__client_1gab298f6cc6a31558f29fdaf6ae4680504">Cy_BLE_CGMS_RegisterAttrCallback</ref>):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__service__api__events_1ggada4b6b6d080ad4380e3a2e3cab4f2d51a3698e497ecd4c626856fcb6fd2248491">CY_BLE_EVT_CGMSC_READ_DESCR_RESPONSE</ref> - In case if the requested attribute is successfully read on the peer device, the details (charIndex, descrIndex, value, etc.) are provided with event parameter structure of type <ref kindref="compound" refid="structcy__stc__ble__cgms__descr__value__t">cy_stc_ble_cgms_descr_value_t</ref>.</para></listitem></itemizedlist>
Otherwise (if an CGMS service-specific callback is not registered):<itemizedlist>
<listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a9f66de88a47e0eb5cd2026421ffcea24">CY_BLE_EVT_GATTC_READ_RSP</ref> - If the requested attribute is successfully read on the peer device, the details (handle, value, etc.) are provided with an event parameter structure <ref kindref="compound" refid="structcy__stc__ble__gattc__read__rsp__param__t">cy_stc_ble_gattc_read_rsp_param_t</ref>.</para></listitem><listitem><para><ref kindref="member" refid="group__group__ble__common__api__events_1gga7c1ef7e6d246e0f3eb6c883e23d82b86a89dd75cdee5a4cf9131a12ad37363fd3">CY_BLE_EVT_GATTC_ERROR_RSP</ref> - If an error occurred with the requested attribute on the peer device, the details are provided with an event parameter structure <ref kindref="compound" refid="structcy__stc__ble__gatt__err__param__t">cy_stc_ble_gatt_err_param_t</ref>. </para></listitem></itemizedlist>
</para></simplesect>
</para>        </detaileddescription>
        <inbodydescription>
        </inbodydescription>
        <location bodyend="1258" bodyfile="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cgms.c" bodystart="1218" column="1" file="/var/tmp/gitlab-runner1/builds/mV2grwDe/0/repo/middleware-bless/cy_ble_cgms.h" line="205" />
      </memberdef>
      </sectiondef>
    <briefdescription>
<para>API unique to CGMS designs configured as a GATT Client role. </para>    </briefdescription>
    <detaileddescription>
<para>A letter 'c' is appended to the API name: Cy_BLE_CGMSC_ </para>    </detaileddescription>
  </compounddef>
</doxygen>