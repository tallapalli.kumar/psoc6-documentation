=================
GATT Functions
=================

.. doxygengroup:: group_ble_common_api_gatt_functions_section
   :project: bless


.. toctree::
   :hidden:

   group__group__ble__common__api__gatt__functions.rst
   group__group__ble__common__api__gatt__client__functions.rst
   group__group__ble__common__api__gatt__server__functions.rst