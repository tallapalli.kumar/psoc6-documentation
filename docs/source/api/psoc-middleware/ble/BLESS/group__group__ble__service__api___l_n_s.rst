=======================================
Location and Navigation Service (LNS)
=======================================

.. doxygengroup:: group_ble_service_api_LNS
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__service__api___l_n_s__server__client.rst
   group__group__ble__service__api___l_n_s__server.rst
   group__group__ble__service__api___l_n_s__client.rst
   group__group__ble__service__api___l_n_s__definitions.rst