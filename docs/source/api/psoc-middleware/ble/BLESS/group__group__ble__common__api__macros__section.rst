========
Macros
========

.. doxygengroup:: group_ble_common_api_macros_section
   :project: bless
   
   
.. toctree::
   :hidden:

   group__group__ble__common__api__macros.rst
   group__group__ble__common__api__macros__gatt__db.rst
   group__group__ble__common__api__macros__gatt__uuid__services.rst
   group__group__ble__common__api__macros__gatt__uuid__char__gatt__type.rst
   group__group__ble__common__api__macros__gatt__uuid__char__desc.rst
   group__group__ble__common__api__macros__gatt__uuid__char__type.rst
   group__group__ble__common__api__macros__appearance__values.rst