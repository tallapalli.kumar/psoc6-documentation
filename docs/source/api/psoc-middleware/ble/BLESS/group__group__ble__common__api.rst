===============
BLE Common API
===============

.. doxygengroup:: group_ble_common_api
   :project: bless
   
.. toctree::
   :hidden:

   group__group__ble__common__api__functions.rst
   group__group__ble__common__api__gap__functions__section.rst
   group__group__ble__common__api__gatt__functions__section.rst
   group__group__ble__common__api__l2cap__functions.rst
   group__group__ble__common__api__events.rst
   group__group__ble__common__api__definitions__section.rst
   group__group__ble__common__api__global__variables.rst