================================
ModusToolbox Installation Guide
================================

This guide provides instructions for installing the ModusToolbox
software version 2.1 – a set of tools that enable you to integrate
Cypress devices into your existing development methodology. Refer to
earlier revisions of this guide for instructions to install previous
ModusToolbox software versions.

System Requirements
===================

The ModusToolbox software consumes approximately 2 GB of disk space.
Like most modern software, it requires both free disk space and memory
to run effectively. Cypress recommends a system configuration with a
PassMark CPU score > 2000
`(cpubenchmark.net <http://cpubenchmark.net/>`__), at least 25 GB of
free disk space, and 8 GB of RAM. The product will operate with fewer
resources; however, performance may be degraded.

ModusToolbox software is **not** supported on 32-bit operating systems.
It has been tested on the following:

   Windows 7 64-bit / Windows 10 64-bit macOS 10.x

   Ubuntu Linux 18.04 LTS (**Note** ModusToolbox software requires
   libssl1.0.0, which is not provided in Ubuntu 20.04 for example.)

.. note::
   For Linux and macOS, ModusToolbox software requires the
   following Unix packages (with minimum versions) to work properly:

- make (v3.81)

- mktemp (v8.25) 
   
- perl (v5.18.2)

- cmp (v2.8.1) 

- git (2.17.0)

Some versions of Ubuntu Linux do not include 'make' by default. Use the
following command to install it:

   sudo apt-get install make

Installing with Previous Versions
=================================

ModusToolbox version 2.1 installs alongside previous versions of the
software (version 1.0, 1.1, and 2.0); therefore, all versions can be
used independently. However, refer to the “Production Versioning”
section in the `ModusToolbox User
Guide <http://www.cypress.com/ModusToolboxUserGuide>`__ to understand
how versions interact with each other.

If you installed a version 2.1 Beta release, you need to uninstall it
before installing this production release.

-  **Windows**: The current release installer will prompt you to
   uninstall a previous version 2.1 installation. You can also use the
   Windows Control Panel.

-  **Linux**: Go to the directory where you extracted the tar.gz
   installer. Delete only the tools_2.1 and ide_2.1 directories from the
   "ModusToolbox" directory.

-  **macOS**: The current release installer contains a check box to
   uninstall a previous version 2.1 installation.

.. note::
   If you plan to use the Eclipse IDE included with the
   installation, be aware that uninstalling the ModusToolbox software does
   not remove any Eclipse IDE workspaces you may have previously created.
   You should manually delete these workspaces or move them to another
   location. See also `Step 3: Run the Eclipse IDE <#step-3-run-the-eclipse-ide>`__
   for more  details about workspaces.

Step 1: Download the Software
=============================

Go to the Cypress ModusToolbox website
`(www.cypress.com/modustoolbox) <http://www.cypress.com/modustoolbox>`__
and download the appropriate software for your platform:

-  **Windows**: ModusToolbox_2.1.0.<build>-windows-install.exe

-  **Linux**: ModusToolbox_2.1.0.<build>-linux-install.tar.gz

-  **macOS**: ModusToolbox_2.1.0.<build>-macos-install.pkg

.. note::
   If you plan to use the SEGGER J-link debugger, you must
   download and install the appropriate software pack for your OS. It is
   not included with the ModusToolbox software. Use version 6.35 or later.
   For Linux, if you install this using the tar.gz file, make sure you
   install J-Link in a common location. Otherwise, you must configure the
   Eclipse IDE to specify the location, as follows:

   **Window > Preferences > MCU > Global SEGGER J-Link Path**

   	-  **Executable**: *JLinkGDBServerCLExe*

   	-  **Folder**: *<J-Link_extracted_location>*

Step 2: Install ModusToolbox Software
=====================================

.. note::
   Do not use spaces in the installation directory name. Various
   tools, such as Make, do not support spaces.
   Also, do not use common illegal characters, such as:

   \* . “ ‘ / \\ [ ] : ; \| = ,

.. note::
   If your user home directory contains spaces, see `Installing
   with Spaces in User Home Directory. <#installing-with-spaces-in-user-home-directory>`__

.. note::
   If you install ModusToolbox in a non-default location, you will
   need to set the CY_TOOLS_PATHS environment variable for your system to
   point to the *<install_path>/ModusToolbox/tools_2.1* folder, or set that
   variable in each Makefile. You must use forward slashes in the
   variable’s path, even in Windows. Refer to the “Production Versioning”
   section in the `ModusToolbox User
   Guide. <http://www.cypress.com/ModusToolboxUserGuide>`__

Windows
-------

Run the *ModusToolbox_2.1.0.<build>-windows-install.exe* installer
program. By default, it is installed here:

   *C:\Users\<user_name>\ModusToolbox*

.. note::
   If you have not installed ModusToolbox software previously, you
   may be prompted to restart your computer due to installation of
   Microsoft Visual C++ redistributable files.

Linux
-----

Extract the *ModusToolbox_2.1.0.<build>-linux-install.tar.gz* file to
your *<user_home>* directory. The extraction process will create a
"ModusToolbox" directory there, if there is not one there already.

After extracting, you must run the following scripts before running
ModusToolbox software on your machine:

-  OpenOCD:
   <user_home>/ModusToolbox/tools_2.1/openocd/udev_rules/install_rules.sh

-  WICED Bluetooth Boards:
   <user_home>/ModusToolbox/tools_2.1/driver_media/install_rules.sh

-  Firmware Loader:
   <user_home>/ModusToolbox/tools_2.1/fw-loader/udev_rules/install_rules.sh

-  Post-Install Script:
   <user_home>/ModusToolbox/tools_2.1/modus-shell/postinstall

For the ModusToolbox software to work correctly on Linux, you must
install an additional "libusb-0.1-4" package for the Ubuntu 18.xx OS. To
do that, run the following command in the Terminal window:
::

   $ sudo apt-get install libusb-0.1-4

macOS
-----

Double-click the downloaded *ModusToolbox_2.1.0.<build>-osx-install.pkg*
file and follow the wizard.

.. note::
   On the Catalina version of macOS, you may see a dialog similar
   to the following:

|image1|

If this happens, click **OK** to close the dialog. Then, right-click on
the pkg file and select **Open**. When this dialog displays again, it
will include an **Open** option.

The ModusToolbox software will be installed under the **Applications**
folder in the volume you select in the wizard.

.. note::
   The ModusToolbox package installer installs the WICED USB
   driver for use with future versions of ModusToolbox. It may pop up a
   "System Extension Blocked" dialog. In this case, go to **Security
   Preference** and click **Allow** for the driver to be installed.

In order for ModusToolbox to work correctly on macOS, you must install
an additional Xcode package from the Mac App Store if you don’t already
have it installed. You can also install it using the following command
in a terminal window:
::

   xcode-select –-install

Step 3: Run the Eclipse IDE
===========================

The ModusToolbox software includes an optional Eclipse IDE. To run the
IDE:

-  **Windows**: Navigate to <user_home>/ModusToolbox/ide_2.1/eclipse and
   run *ModusToolbox.exe*. 
  
   .. note::
      You may be asked to grant access by the Firewall manager.

-  **Linux**: Navigate to <user_home>/ModusToolbox/ide_2.1/eclipse and
   run *ModusToolbox*.

-  **macOS**: Navigate to the directory where you moved the
   *ModusToolbox.app* file (for example,
   */Applications/ModusToolbox/ide_2.1*) and run *ModusToolbox.app*.

When the Eclipse IDE runs for the first time, a dialog opens to specify
the Workspace location. The default location for the workspace is:
*<user_home>/mtw*.

.. note::
   Be aware that the default Eclipse IDE workspace location
   (*<user_home>/mtw*) is the same for all versions of ModusToolbox
   software. If you plan to use more than one version, you must specify
   different workspace names for each one.

|image2|

Enter the workspace location and name and click **Launch** to open the
IDE.

.. note::
   If you change the workspace location or name, do not use spaces
   or illegal characters anywhere in the path.

After the IDE opens for the first time, the Cypress End User License
Agreement (EULA) displays.

|image3|

Read the EULA and click **Accept** to proceed. If you click **Decline**,
the IDE will close.

Next Steps
==========

Refer to the `ModusToolbox User
Guide <http://www.cypress.com/ModusToolboxUserGuide>`__ for a
description of the software and instructions to get started.

If you plan to use the Eclipse IDE included with the ModusToolbox
software:

-  Refer to the `Eclipse IDE for ModusToolbox Quick Start
   Guide <http://www.cypress.com/ModusToolboxQSG>`__ for brief
   instructions to create, build, and program applications.

-  Refer to the `Eclipse IDE for ModusToolbox User
   Guide <http://www.cypress.com/MTBEclipseIDEUserGuide>`__ for more
   detailed information about using the IDE. 

These documents are also available from the Eclipse IDE **Help** menu.

Installing with Spaces in User Home Directory
=============================================

The ModusToolbox installer tries to install in your user home directory
by default. However, it prevents you from installing into a directory
that contains spaces.

|image4|

If possible, create a new user account and user home directory that
doesn’t contain spaces. If you cannot create a new user home directory
without spaces, then you must perform some extra manual installation
steps.

.. note::
   Even though this process is shown for Windows, these steps
   apply in general to macOS and Linux as well.

Step 1: Install at a custom path.
---------------------------------

1. Select an alternate installation path that does not include spaces.
   For example: 

      	  *C:\Cypress\ModusToolbox*

   Any path without spaces will work.

2. After installation is complete, create a directory to store your
   workspaces. For example: 

      	  *C:\Cypress\mtb-projects*

   You can choose any path as long as it doesn’t contain spaces.

3. Also, create a hidden “dot” directory named “\ **.**\ modustoolbox”
   to store the cache, offline content, and manifest.loc file
   discussed later in this section. For example:

      	  *C:\Cypress\\\ *\ **.**\ *\ modustoolbox*

Step 2: Create a system variable to specify the path to Tools.
--------------------------------------------------------------

Because you are installing ModusToolbox into a non-default location, you
need to specify the path to your “tools” directory using a System
Variable. Open the Environment Variables dialog, and create a new System
Variable. For example:

        CY_TOOLS_PATHS = C:/Cypress/ModusToolbox/tools_2.1/

|image5|

.. note::
   Use a Windows-style path (not Cygwin-style, like /cygdrive/c/).
   Also, use forward slashes.

Step 3: Create a system variable to specify the path to cache.
--------------------------------------------------------------

The ModusToolbox make system clones all the repos needed for your
project, directly into your project. So the resulting project is
self-contained. It uses cache to speed up the clone operations.
Normally, the make system would create and use cache directory at:

	*C:\Users\<user_name>\.modustoolbox\\*

However, you need to fix this for the new install location for
ModusToolbox by changing the location where the make system keeps the
cache. Open the Environment Variables dialog, and create a new System
Variable. For example:

	CY_GETLIBS_CACHE_PATH = C:/Cypress/.modustoolbox/cache/

|image6|

.. note::
   Use a Windows-style path (not Cygwin-style, like /cygdrive/c/).
   Also, use forward slashes.

Alternately, you can disable the caching. The downside is that this will
slow down the clone operation and overall project creation, as well as
the library update experience. To disable the cache, create a User
Variable:

	CY_GETLIBS_NO_CACHE = 1

Step 4: Specify the custom path to use for Offline Content and manifest.loc.
----------------------------------------------------------------------------

Although you may not use these features, dependencies require that you
set them up while installing the software.

Offline Content Path
~~~~~~~~~~~~~~~~~~~~

Specify the non-default location to the “offline” directory with a
System Variable. For example:

	CY_GETLIBS_OFFLINE_PATH = C:/Cypress/.modustoolbox/offline/

manifest.loc 
~~~~~~~~~~~~

Likewise, create a System Variable to specify the non-default location
of the *manifest.loc* file. For example:

	CyManifestLocOverride = C:/Cypress/.modustoolbox/manifest.loc


Step 5: Configure Eclipse Workspace
-----------------------------------

If using the Eclipse IDE, you must specify a path to the workspace that
does not include spaces. For example:

	*C:\Cypress\mtb-projects\mtb-wksp1*

|image7|


.. |image1| image:: ../_static/image/Modustoolbox/Installing-ModusToolbox/image1.png
.. |image2| image:: ../_static/image/Modustoolbox/Installing-ModusToolbox/image2.png
.. |image3| image:: ../_static/image/Modustoolbox/Installing-ModusToolbox/image3.png
.. |image4| image:: ../_static/image/Modustoolbox/Installing-ModusToolbox/image4.png
.. |image5| image:: ../_static/image/Modustoolbox/Installing-ModusToolbox/image5.png
.. |image6| image:: ../_static/image/Modustoolbox/Installing-ModusToolbox/image6.png
.. |image7| image:: ../_static/image/Modustoolbox/Installing-ModusToolbox/image7.png
