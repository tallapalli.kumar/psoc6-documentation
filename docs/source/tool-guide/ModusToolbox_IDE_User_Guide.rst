============================
ModusToolbox IDE User Guide
============================


Contents
========

`1. Introduction <#introduction>`__

	`Overview <#overview>`__

	`References <#references>`__

	`Document Conventions <#document-conventions>`__

	`Revision History <#revision-history>`__

`2. Getting Started <#getting-started>`__

	`Launch Eclipse IDE <#launch-eclipse-ide>`__

	`Open Project Creator Tool <#open-project-creator-tool>`__

	`Create PSoC 6 MCU Application <#create-psoc-6-mcu-application>`__

	`Create WICED Bluetooth Application
	<#create-wiced-bluetooth-application>`__

	`Build Application <#build-application>`__

	`Program Application <#program-application>`__
	
	`Download/Import Code Examples <#download-import-code-examples>`__

`3. Mbed OS to ModusToolbox Flow <#mbed-os-to-modustoolbox-flow>`__

	`Install and Configure Mbed CLI <#install-and-configure-mbed-cli>`__

	`Switch Kit to DAPLink Mode <#switch-kit-to-daplink-mode>`__

	`Create Application in Mbed CLI <#create-application-in-mbed-cli>`__

	`Compile Application in Mbed CLI (Optional)
	<#compile-application-in-mbed-cli-optional>`__

	`Export Application from Mbed CLI
	<#export-application-from-mbed-cli>`__

	`Import Application into Eclipse IDE
	<#import-application-into-eclipse-ide>`__

	`Configure Eclipse IDE <#configure-eclipse-ide>`__

	`Build, Program, Debug Application
	<#build-program-debug-application>`__

	`Using MiniProg4 in DAPLink Mode
	<#using-miniprog4-in-daplink-mode>`__

`4. IDE Description <#ide-description>`__

	`Overview <#overview-1>`__

	`Project Explorer <#project-explorer>`__

	`Quick Panel <#quick-panel>`__

`5. Configure Applications <#configure-applications>`__

	`Modify Code <#modify-code>`__

	`Use Configurators <#use-configurators>`__

	`Use Tools <#use-tools>`__

	`Rename Application <#rename-application>`__

`6. Build Applications <#build-applications>`__

	`Build with Make <#build-with-make>`__

	`Build with IDE <#build-with-ide>`__

	`GCC Version <#gcc-version>`__

`7. Program and Debug <#program-and-debug>`__

	`PSoC 6 Programming/Debugging <#psoc-6-programming-debugging>`__

	`Bluetooth Programming/Debugging <#bluetooth-programming-debugging>`__

	`Mbed OS Programming/Debugging <#mbed-os-programming-debugging>`__

Introduction
============

Overview
--------

ModusToolbox™ software is a set of tools that enable you to integrate
Cypress devices into your existing development methodology. One of the
tools is a multi-platform, Eclipse-based Integrated Development
Environment (IDE) that supports application configuration and
development.

The ModusToolbox ecosystem includes IoT development platforms like Mbed™
OS and Amazon FreeRTOS™, in addition to the software installed on your
computer and available on GitHub. For a more comprehensive description
of the ModusToolbox software, refer to the *ModusToolbox User Guide*.

This document provides information about creating applications as well
as building, programming, and debugging them. This guide primarily
covers the Eclipse IDE aspects of these concepts. It also covers various
aspects of the software installed along with the IDE, where applicable.
For information about using Mbed OS, refer to the `Mbed OS to
ModusToolbox Flow <#mbed-os-to-modustoolbox-flow>`__ chapter in this
document. For information about using Amazon FreeRTOS, refer to the
`their website <https://aws.amazon.com/freertos/>`__.

References
----------

The following documents should be referenced as needed for more
information about a particular topic.

-  `ModusToolbox Installation
   Guide <http://www.cypress.com/ModusToolboxInstallGuide>`__

-  `ModusToolbox User
   Guide <http://www.cypress.com/ModusToolboxUserGuide>`__

-  Configurator Documentation (open from a particular Configurator)

-  Eclipse Documentation (available from Eclipse)

-  `Eclipse Survival
   Guide <https://community.cypress.com/docs/DOC-15617>`__

-  `KitProg3 User
   Guide <https://www.cypress.com/documentation/development-kitsboards/kitprog-user-guide>`__

Document Conventions
--------------------

The following table lists the conventions used throughout this guide:

+----------------------------+----------------------------------------+
| Convention                 | Usage                                  |
+============================+========================================+
| Courier New                | Displays file locations and source     |
|                            | code:                                  |
|                            |                                        |
|                            | C:\\ …cd\\icc\\, user entered text     |
+----------------------------+----------------------------------------+
| *Italics*                  | Displays file names and reference      |
|                            | documentation:                         |
|                            |                                        |
|                            | *sourcefile.hex*                       |
+----------------------------+----------------------------------------+
| [**bracketed, bold**]      | Displays keyboard commands in          |
|                            | procedures:                            |
|                            |                                        |
|                            | [**Enter**] or [**Ctrl**] [**C**]      |
+----------------------------+----------------------------------------+
| **File > New Application** | Represents menu paths:                 |
|                            |                                        |
|                            | **File > New Application > Clone**     |
+----------------------------+----------------------------------------+
| **Bold**                   | Displays commands, menu paths and      |
|                            | selections, and icon names in          |
|                            | procedures:                            |
|                            |                                        |
|                            | Click the **Debugger** icon, and then  |
|                            | click **Next**.                        |
+----------------------------+----------------------------------------+
| Text in gray boxes         | Displays cautions or functionality     |
|                            | unique to ModusToolbox software.       |
+----------------------------+----------------------------------------+

Revision History
----------------

+--------------------------------------------------------------------+
| Document Title: Eclipse IDE for ModusToolbox User Guide            |
|                                                                    |
| Document Number: 002-24375                                         |
+============================+==========+============================+
| Revision                   | Date     | Description of Change      |
+----------------------------+----------+----------------------------+
| \*\*                       | 11/21/18 | New document.              |
+----------------------------+----------+----------------------------+
| \*A                        | 2/22/19  | Updates for ModusToolbox   |
|                            |          | version 1.1.               |
+----------------------------+----------+----------------------------+
| \*B                        | 10/15/19 | Updates for ModusToolbox   |
|                            |          | version 2.0.               |
+----------------------------+----------+----------------------------+
| \*C                        | 10/29/19 | Edits to various screen    |
|                            |          | captures and related text. |
+----------------------------+----------+----------------------------+
| \*D                        | 3/20/20  | Updates for ModusToolbox   |
|                            |          | version 2.1.               |
+----------------------------+----------+----------------------------+
| \*E                        | 4/1/20   | Fix broken links.          |
+----------------------------+----------+----------------------------+

Getting Started
===============

This section provides a basic walkthrough for how to create a couple
applications using the IDE, selecting a BSP. It also covers how to build
and program them using the IDE and basic launch configurations supplied
for the applications.

-  `Launch Eclipse IDE <#launch-eclipse-ide>`__

-  `Open New Application Dialog <#open-project-creator-tool>`__

-  `Create PSoC 6 MCU Application <#create-psoc-6-mcu-application>`__

-  `Create WICED Bluetooth
   Application <#create-wiced-bluetooth-application>`__

-  `Build Application <#build-application>`__

-  `Program Application <#program-application>`__

-  `Download/Import Code Examples <#download-import-code-examples>`__

Launch Eclipse IDE
------------------

The Eclipse IDE is installed in the following directory in Windows, by
default:

	*<user_home>\\ModusToolbox\\ide_2.1\\eclipse\\*

For other operating systems, the installation directory will vary, based
on where the software was installed.

.. note::

   If the software is not installed in the default location, you
   will need to set an environment variable. Refer to the `ModusToolbox
   Installation Guide <http://www.cypress.com/ModusToolboxInstallGuide>`__
   for details.

Run the "ModusToolbox" executable file to launch the IDE as applicable
for your operating system.

When launching the Eclipse IDE, it provides an option to select the
workspace location on your machine. This location is used by the IDE for
creating and storing the files as part of application creation for a
particular platform. The default workspace location is a folder called
"mtw" in your home directory. You may add additional folders under the
"mtw" folder or to choose any other location for each workspace.

Open Project Creator Tool
-------------------------

Click the **New Application** link in the Eclipse IDE Quick Panel.

|image1|

You can also select **File > New > ModusToolbox Application**.

|image2|

These commands launch the Project Creator tool, which provides several
applications for use with different development kits. The kits available
may change over time.

|image3|

For more details about using this tool, refer to the `Project Creator
Guide <http://www.cypress.com/ModusToolboxProjectCreator>`__.

Create PSoC 6 MCU Application
-----------------------------

This section provides a walkthrough for creating a PSoC 6 MCU
application.

Choose Board Support Package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Project Creator tool displays a list of boards, showing the Kit
Name, MCU, and Connectivity Device (if applicable). As you select each
of the kits shown, the description for that kit displays on the right.
For this example, select the CY8CKIT-062-WIFI-BT kit.

|image4|

Select Application
~~~~~~~~~~~~~~~~~~

Click **Next >** to open the Select Application page. This page lists
various applications available for the selected kit. As you select an
application, a description displays on the right.

|image5|

For this example:

-  Select the "Hello World" application from the list.

-  Type a name for your application. Do not use spaces in the
   application name. In this case, use "Hello_World" as the
   **Application name**.

.. note::

   You can use the **Import** button |image6| to select other
   examples you downloaded from the web or received from a colleague. In
   the Open dialog, select only examples that are supported by the BSP you
   selected for this application. Then, the example will be shown in the
   dialog with all the other applications. See also `Import Code
   Examples <#import-code-examples>`__ for additional details.

Create Application
~~~~~~~~~~~~~~~~~~

Click **Create** to begin the project creation process.

|image7|

.. note::

   The application creation process performs a git clone
   operation, and downloads the selected application from the Cypress
   GitHub website. Depending on the selected application, this process can
   take several minutes.

When complete, click **Close** to close the Project Creator tool. In the
IDE, a message displays about importing the project:

|image8|

After several moments, the application opens with the *Hello_World* in
the `Project Explorer <#project-explorer>`__, and the *README.md* file
opens in the file viewer.

|image9|

Create WICED Bluetooth Application
----------------------------------

This section provides a walkthrough for creating a WICED Bluetooth
application.

.. note::

   For Bluetooth applications, you must first create a separate
   project called wiced_btsdk, which contains the SDK, BSPs, and libraries
   that are shared among all Bluetooth applications in a workspace. This
   application only needs to be created once per workspace.


Choose Board Support Package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Project Creator tool displays a list of boards, showing the Kit
Name, MCU, and Connectivity Device (if applicable). As you select each
of the kits shown, the description for that kit displays on the right.
For this example, select the CYW920819EVB-02 Evaluation kit.

|image10|

.. note::

   When creating the wiced_btsdk project, it doesn't matter which
   board you select because this project supports all WICED boards;
   however, you should select the board you want for the desired Bluetooth
   application.

Select wiced_btsdk Application
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Click **Next >** to open the Select Application page. This page lists
various applications available for the selected kit. As you select an
application, a description displays on the right.

|image11|

For this example:

-  Select the ”wiced_btsdk” application from the list.

-  Do not change the name of this project. Bluetooth applications use
   the project name "wiced_btsdk" in the makefile paths.

Click **Create** to begin the project creation process.

|image12|

.. note::

   The application creation process performs a git clone
   operation, and downloads the selected application from the Cypress
   GitHub website. Depending on the selected application, this process can
   take several minutes.

Select Specific Application
~~~~~~~~~~~~~~~~~~~~~~~~~~~

When project creation completes for the wiced_btsdk project, select the
specific WICED Bluetooth application you wish to create.

|image13|

For this example:

-  Select the "Audio-20819EVB02" application from the list.

-  Type a name for your application. Do not use spaces in the
   application name. In this case, use "Audio-20819EVB02" as the
   **Application name**.

Click **Create** again to begin the project creation process for the
desired application.

|image14|

When complete, click **Close** to close the Project Creator tool. In the
IDE, a message displays about importing the projects:

|image15|

After a few moments, the application opens in the `Project
Explorer <#project-explorer>`__, and the *README.md* file opens in the
file viewer.

|image16|

.. note::

   Many Bluetooth applications contain multiple projects. For
   example, the BLE-20819EVB02 application contains projects for BLE
   services such as anc, ans, bac, bas, beacon, etc.

Build Application
-----------------

After loading an application, build it to generate the necessary files.
Select a project. Then, in the **Quick Panel**, click the "Build
<project> Application" link.

====================== ===========================
PSoC 6 MCU Application WICED Bluetooth Application
====================== ===========================
|image17|              |image18|
====================== ===========================

Messages display in the Console, indicating whether the build was
successful or not. For more details about building applications and the
various Consoles available, see the `Build
Applications <#build-applications>`__ chapter.

Program Application
-------------------

There are many more details about programming an application. This
section only covers it briefly. For more detailed information, see the
`Program and Debug <#program-and-debug>`__ chapter.

In the Project Explorer, select the desired project. Then, in the
**Quick Panel**, click the "<app-name> Program (KitProg3)" link for a
PSoC 6 MCU application, and "<app-name> Program" for a WICED Bluetooth
application.

====================== ===========================
PSoC 6 MCU Application WICED Bluetooth Application
====================== ===========================
|image19|              |image20|
====================== ===========================

Download/Import Code Examples
-----------------------------

Cypress provides many code examples. These examples allow you to explore
the capabilities provided by the SDK, create applications based on them,
examine the source code demonstrated in them, and read their associated
documentation.

Download from GitHub
~~~~~~~~~~~~~~~~~~~~

The **Quick Panel** provides a link to access online code examples.
Click the "Search Online for Code Examples" link.

|image21|

This opens a web browser to the GitHub repository to select and download
the appropriate examples.

|image22|

Import Code Examples
~~~~~~~~~~~~~~~~~~~~

Whether you’ve downloaded an example, or received one from a colleague,
Cypress recommends one of the following methods to import the example
into the Eclipse IDE:

-  Use the Project Creator to create a new application, and in that
   process select the **Import** button |image23|\ to select a folder
   that contains the application to import.

-  If you have an Eclipse-ready code example (for example, a project
   exported from Eclipse) that you want to import into the Eclipse IDE,
   use the standard **File > Import** process. Note the following:

-  On the Import dialog, select **General > Existing Projects into
   Workspace**.

|image24|

-  On the next page, enable the **Copy projects into Workspace** check
   box.

|image25|

.. note::

   There are various ways to import examples into Eclipse. If you
   prefer a different method, make sure that all of the project files are
   copied into the workspace directory.

Mbed OS to ModusToolbox Flow
============================

For ModusToolbox 2.0 and later, Cypress has enabled several kits to be
used in the Mbed ecosystem. For more information, refer to the Cypress
webpage on the Mbed OS website: https://os.mbed.com/teams/Cypress/.

These kits support various connectivity applications that you cannot
create from the Eclipse IDE. You must use the Mbed OS flow for these
types of applications. This section provides the necessary steps to
import, build, program, and debug Mbed OS applications in the Eclipse
IDE. The main steps include:

-  `Install and Configure Mbed CLI <#install-and-configure-mbed-cli>`__

-  `Switch Kit to DAPLink Mode <#switch-kit-to-daplink-mode>`__

-  `Create Application in Mbed CLI <#switch-kit-to-daplink-mode>`__

-  `Compile Application in Mbed CL
   (Optional) <#compile-application-in-mbed-cli-optional>`__

-  `Export Application from Mbed
   CLI <#export-application-from-mbed-cli>`__

-  `Import Application into Eclipse
   IDE <#import-application-into-eclipse-ide>`__

-  `Configure Eclipse IDE <#configure-eclipse-ide>`__

-  `Build, Program, Debug
   Application <#build-program-debug-application>`__

Install and Configure Mbed CLI 
------------------------------

You should be familiar with Arm Mbed OS and the command line interface
(CLI). Refer to the official Mbed OS instructions:

   `Mbed OS
   Website <https://os.mbed.com/docs/mbed-os/v5.15/tools/installation-and-setup.html>`__

**For Windows and macOS**, download and use the installer rather than
manually installing it. The "see documentation" link on the Mbed webpage
is for manual installation.

**For macOS**, you will get an error that it is from an unidentified
developer.

|image26|

If you get this message, right-click on the MBEDCLI application and
select **Open**. Then, the message will look like the following:

|image27|

Click **Open**.

.. note::

   You only have to do this once; macOS will remember the
   decision, and the app will open normally from then on.

Install/Update PyOCD
~~~~~~~~~~~~~~~~~~~~

In order for Mbed applications to work with ModusToolbox, make sure the
Mbed Python packages are installed and updated to versions compatible
with Cypress kits. See https://github.com/mbedmicro/pyOCD#installing for
more details, including information about libusb installation.

**For macOS**, run the MBEDCLI application to start an mbed-capable
terminal before installing pyocd. This ensures pyocd installs in the
self-contained mbed environment. PyOCD is directly supported by the
Python bundled with the MBEDCLI application.

Run the following command from a terminal window:
::

   pyocd --version

The reply should be (or later):
::

   0.16.1

If you don’t have the correct version, run:
:: 

   pip install -U pyocd

Configure Compilers 
~~~~~~~~~~~~~~~~~~~

Install and configure the appropriate compilers for Mbed CLI. Refer to
the Mbed OS for website supported compilers:

https://os.mbed.com/docs/mbed-os/latest/tools/after-installation-configuring-mbed-cli.html

To use the GCC 7.2.1 distribution bundled with ModusToolbox, run the
mbed config command as appropriate for you operating system:

For Windows and Linux:
^^^^^^^^^^^^^^^^^^^^^^
::

   mbed config -G GCC_ARM_PATH ~/ModusToolbox/tools_2.1/gcc-7.2.1/bin

For macOS:
^^^^^^^^^^
::

   mbed config -G GCC_ARM_PATH /Applications/ModusToolbox/tools_2.1/gcc-7.2.1/bin

To use another compiler, run the mbed config command and enter the
appropriate path. The following are a few typical paths for compilers on
Windows:
:: 

  mbed config -G GCC_ARM_PATH "C:\Program Files (x86)\GNU Tools ARM
  Embedded\6 2017-q2-update\bin"
  mbed config -G ARM_PATH "C:\Program Files(x86)\ARM_Compiler_5.06u6"
  mbed config -G ARMC6_PATH "C:\Program Files\ARMCompiler6.11\bin"
  mbed config -G IAR_PATH "C:\Program Files (x86)\IAR Systems\EmbeddedWorkbench 7.5\arm"

To use the ARM v5/v6 compilers bundled with Keil MDK v5, use one of
these commands as appropriate:
::

   mbed config -G ARM_PATH "C:\Keil_v5\ARM\ARMCC"
   mbed config -G ARMC6_PATH "C:\Keil_v5\ARM\ARMCLANG\bin"

Switch Kit to DAPLink Mode
--------------------------

Use the fw-loader tool included with ModusToolbox software to upgrade
the kit firmware using the following instructions. Refer also to the
*KitProg3 User Guide* for more information. The tool is located in the
following directory, by default:

	*<install_dir>/tools_2.1/fw-loader/bin/*

.. note::

   For Windows 7 only, the Mbed serial port driver must be
   installed on your system with the connected kit. This driver should be
   installed with the Mbed CLI. If not, the driver can be found at:
   https://os.mbed.com/handbook/Windows-serial-configuration. Do not
   install this on Windows 10.

-  Run the fw-loader tool from the command line to update the KitProg3
   firmware:
   ::

       fw-loader --update-kp3

-  After updating the firmware, run this command to switch to DAPLink
   mode:
   ::

       fw-loader --mode kp3-daplink

-  Then, run the following from the command line:
   ::

       pyocd list

   The reply should indicate that a valid device is connected. For example:

   +-----+-------------------------------------+------------------------------------------------------------+
   |  #  |   Probe                             |     Unique ID                                              |
   +-----+-------------------------------------+------------------------------------------------------------+
   |  0  |   CY8CKIT-062-WIFI-BT [cy8c6xx7]    |     190013016c121817036c121800000000000000002e127069       |
   +-----+-------------------------------------+------------------------------------------------------------+

.. note::
  
   If you get an error message that libusb library was not found,
   make sure you have completed all the required steps described in
   https://github.com/mbedmicro/pyOCD#installing.

Create Application in Mbed CLI
------------------------------

Open an Mbed CLI prompt and create the directory for Mbed examples. For
example:

	mkdir mbed-examples

	cd mbed-examples

.. note::

   On Windows, do **not** use a Cygwin terminal window, and make
   sure Cygwin is not in your Windows PATH system environment variable. Use
   the MINGW bash terminal instead.

Import the mbed-os-example-blinky example:

	mbed import mbed-os-example-blinky

Switch to the example directory:

	cd mbed-os-example-blinky

Compile Application in Mbed CLI (Optional)
------------------------------------------

.. note::

   This step is optional. It may be useful to confirm that the
   application builds successfully and produces a HEX file.

Compile the Mbed application for one of the supported target boards with
one of the supported toolchains

	mbed compile --target **TARGET** --toolchain **TOOLCHAIN**

where TARGET is one of:

  |  CY8CKIT_062_WIFI_BT
  |  CY8CPROTO_062_4343W
  |  CY8CKIT_062_BLE
  |  CY8CKIT_062_4343W

and TOOLCHAIN is one of:

  |  GCC_ARM
  |  ARM
  |  IAR

For example:

	mbed compile --target CY8CKIT_062_WIFI_BT --toolchain GCC_ARM

Export Application from Mbed CLI
--------------------------------

To export the Mbed application for use in the Eclipse IDE, use the Mbed
CLI export command. For example:

mbed export -i eclipse_gcc_arm -m CY8CKIT_062_WIFI_BT

Import Application into Eclipse IDE
-----------------------------------

Use the standard Eclipse **Import** function to import the Mbed
application. Expand the **C/C++** folder and select the **Existing Code
as Makefile Project** option. Click **Next >**.

|image28|

Click **Browse…** and navigate to the directory where you exported the
application. Ensure the **Toolchain for Indexer Settings** is set to
**ARM Cross GCC**. Do not change any other settings. Click **Finish**.

|image29|

When the import finishes, the Mbed OS application will be shown in the
`Project Explorer <#project-explorer>`__.

|image30|

Configure Eclipse IDE
---------------------

Define the Workspace pyOCD Path
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Open the Preferences dialog, select **MCU > Workspace pyOCD Path**, and
set the following workspace paths (adjust the path to the Scripts
directory for your python/pyocd installation):

|image31|

.. note::

   Your path folder may differ from these examples. Check the
   correct path folder using the which pyocd-gdbserver command.

Windows:

-  Workspace pyOCD Path > Executable = pyocd-gdbserver

-  Workspace pyOCD Path > Folder = C:\\Python27\\Scripts

macOS:

-  Workspace pyOCD Path > Executable = pyocd-gdbserver

-  Workspace pyOCD Path > Folder =
   /Applications/MBEDCLI.app/Contents/Resources/miniconda/bin

Linux:

-  Workspace pyOCD Path > Executable = pyocd-gdbserver

-  Workspace pyOCD Path > Folder = ~/.local/bin

Verify Properties Settings
~~~~~~~~~~~~~~~~~~~~~~~~~~

Right-click on the project and select **Properties**. Navigate to
**C/C++ Build > Settings**. Verify that the Toolchain path and Build
Tools path have appropriate vales. Click **Apply** and then click
**Apply and Close**.

|image32|

.. note::

   This step is needed due to a defect in Eclipse.

Build, Program, Debug Application
---------------------------------

Build
~~~~~

In the Quick Panel, click the "Build mbed-os-example-blinky Application"
link to build the project and generate the HEX file.

|image33|

When complete, the Console displays a message similar to the following:

	link: mbed-os-example-blinky.elf

	arm-none-eabi-objcopy -O binary mbed-os-example-blinky.elf
	mbed-os-example-blinky.bin

	arm-none-eabi-objcopy -O ihex mbed-os-example-blinky.elf
	mbed-os-example-blinky.hex

Program
~~~~~~~

In the Quick Panel, under **Launches**, click the
"CY8CKIT_062_WIFI_BT_mbed-os-example-blinky_program" link.

|image34|

Messages will display in the Console. When programming is complete, the
LED should blink red on the board. You may need to press the **Reset**
button.

Debug
~~~~~

In the Quick Panel, under **Launches**, click the
"CY8CKIT_062_WIFI_BT_mbed-os-example-blinky_debug" link.

|image35|

The IDE will switch to debugging mode and will halt at the break, ready
for debugging.

.. note::

   While debugging, you may see various errors in your code
   (*main.cpp* file), such as "function ‘printf’ could not be resolved."
   These are likely not real errors. They are caused by the import of the
   make file into Eclipse. To turn these errors off, go to **Project >
   Properties > C/C++ > Code Analysis**, and disable "Potential Programming
   Problems" and "Syntax and Semantic Errors."

|image36|

Using MiniProg4 in DAPLink Mode
-------------------------------

You can use MiniProg4 as a probe in DAPLink mode for
programming/debugging an Mbed OS project imported into the Eclipse IDE.
You switch the MiniProg4 to DAPLink mode in the same way as for
KitProg3. See `Switch Kit to DAPLink
Mode <#switch-kit-to-daplink-mode>`__.

After importing an Mbed OS Application into the Eclipse IDE, make
additional changes to the appropriate launch configurations. Under the
**Debugger** tab:

|image37|

-  Select MiniProg4 as the **Board**. In the drop-down list it will be
   shown as "Generic Board - ARM DAPLink CMSIS-DAP (00001301..."

-  Select the **Override target** check box.

-  Select/enter the appropriate target name to the right of **Override
   target**. You can run the following command from a terminal window to
   get a list of supported targets:

pyocd list -t -r Cypress

IDE Description
===============

.. _overview-1:

Overview
--------

The IDE is based on the Eclipse IDE. It uses several plugins, including
the Eclipse C/C++ Development Tools (CDT) plugin. For more information
about Eclipse, refer to the `Eclipse Workbench User
Guide <http://help.eclipse.org/oxygen/index.jsp?nav=%2F0_4_4>`__.
Cypress also provides a document called the `Eclipse IDE Survival
Guide <https://www.cypress.com/EclipseIDESurvivalGuide>`__, which
provides tips and hints for how to use the Eclipse IDE.

The IDE contains Eclipse standard menus and toolbars, plus various panes
such as the Project Explorer, Code Editor, and Console. One difference
from the standard Eclipse IDE is the "ModusToolbox Perspective." This
perspective provides the "Quick Panel," a "News View," and adds tabs to
the Project Explorer. "Perspective" is an Eclipse term for the initial
set and layout of views in the IDE. The Eclipse IDE also provides a
Welcome Page, which displays on first launch of the IDE for a given
workspace.

|image38|

.. note::

   If you switch to a different perspective, you can restore the
   ModusToolbox Perspective by clicking the ModusToolbox icon button in the
   upper right corner. You can also select **Perspective > Open Perspective
   > ModusToolbox** from the **Window** menu. To restore the ModusToolbox
   perspective to the original layout, select **Perspective > Reset
   Perspective** from the **Window** menu.

The following describe different parts of the IDE:

-  Menus and Toolbars – Use the various menus and toolbars to access
   build/program/debug commands for your application. Many of these are
   covered in the `Eclipse Workbench User
   Guide <http://help.eclipse.org/oxygen/index.jsp?nav=%2F0_4_4>`__.

-  Project Explorer – Use the Project Explorer to find and open files in
   your application. See `Project Explorer <#project-explorer>`__ for
   more information.

-  `Quick Panel <#quick-panel>`__ – Use this tab to access appropriate
   commands, based on what you select in the Project Explorer.

-  News View – This is an Eclipse view in the ModusToolbox Perspective
   that displays a page of blog articles from cypress.com. This is
   located in the same panel as the Console and Problems tabs.

|image39|

-  Code Editor – Use the Code Editor to edit various source files in
   your application.

Project Explorer
----------------

In the Eclipse IDE, after creating an application, the Project Explorer
contains one or more related project folders. The following images show
a PSoC 6 MCU application and a WICED Bluetooth application. For
applications imported from Mbed OS, see `Mbed OS to ModusToolbox
Flow <#mbed-os-to-modustoolbox-flow>`__.

====================== ===========================
PSoC 6 MCU Application WICED Bluetooth Application
====================== ===========================
|image40|              |image41|
====================== ===========================

Both types of applications contain a similar project structure. Each
contains the main application source code, and a Makefile. Note that
PSoC 6 MCU applications contain a libs directory, while BT applications
have a wiced_btsdk project with shared SDK, BSPs, and libraries for all
Bluetooth applications in a workspace.

Quick Panel
-----------

As stated previously, the Quick Panel is part of the ModusToolbox
Perspective. It provides quick access to commands and documentation
based on what you have selected in the `Project
Explorer <#project-explorer>`__.

====================== ===========================
PSoC 6 MCU Application WICED Bluetooth Application
====================== ===========================
|image42|              |image43|
====================== ===========================

The Quick Panel contains links to various commands and documentation,
organized as follows:

-  **Start** – This contains the New Application link to create new
   applications, and a link to find Code Examples.

-  **Selected <app-name> project** – This contains different
   project-related links based on the project that is selected in the
   Project Explorer, as well as the type of application. Links here
   include: Build and Clean the application.

-  **Launches** – This contains various Launch Configurations, based on
   the selected application project and device, which can be used to
   program the device and launch the debugger. This area is only
   populated if you have the top project in your application selected
   (*<app-name>*). For more information, see `Launch
   Configurations <#launch-configurations>`__.

-  **Tools** – This contains links to the various tools available for
   the selected project. For more information, see `Use
   Configurators <#use-configurators>`__ and `Use Tools <#use-tools>`__.

-  **Documentation** – This may contain several documents in HTML
   format, which are included as part of the chosen BSP.

Configure Applications
======================

This chapter covers how to make various changes to your application. It
includes:

-  `Modify Code <#modify-code>`__

-  `Use Configurators <#use-configurators>`__

-  `Use Tools <#use-tools>`__

-  `Rename Application <#rename-application>`__

Modify Code
-----------

Most applications work as they are, and there is no need to add or
modify code. However, if you want to update and change the application
to do something else, or if you are developing your own application,
open the appropriate file in the code editor.

-  **PSoC 6 MCU:** In the Project Explorer, double-click the *main.c*
   file.

|image44|

-  **WICED Bluetooth:** In the Project Explorer, expand the *<app-name>*
   project folder and double-click the application *<app-name>.c* file.

|image45|

As you type into the file, an asterisk (*) will appear in the file’s tab
to indicate changes were made. The **Save/Save As** commands will also
become available to select.

Use Configurators
-----------------

ModusToolbox software provides graphical applications called
configurators that make it easier to configure a hardware block. Each
configurator provides a separate guide, available from the
configurator's **Help** menu. For more information about configurators,
refer to the *ModusToolbox User Guide*.

Launching Configurators from the IDE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To launch a configurator from the Eclipse IDE, right-click on the
*<app-name>* project in the Project Explorer, select **ModusToolbox**,
and then select the appropriate configurator.

|image46|

.. note::

   You can also launch available configurators from links in the
   Quick Panel.

Depending on the enabled resources in your application, there may be
several configurators available to launch.

-  If you launch the Device Configurator from the IDE, you are opening
   the project’s *design.modus* file, which is responsible for holding
   all of the BSP configuration information. It contains the following:

-  Selected device

-  Resource parameters

-  Constraints

-  If you launch any of the other configurators from the IDE, they will
   open using that configurator’s configuration file
   (*design.cycapsense*, *design.cyseglcd*, etc.). These files are
   specific to the given resource, and they may rely on configuration
   data from the *design.modus* file.

Launching Configurators without the IDE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To launch any Configurator without using the IDE, navigate to the
desired configurator’s installation directory, and run the executable.
Some configurators will open without any configuration information. You
will need to open a configuration file or create a new one. Others will
open with a default configuration file that needs to be saved.

Use Tools
---------

In addition to the configurators, there are several tools, including
Library Manager, BTSpy, ClientControl, Power Estimator, and Device
Firmware Update (DFU) Host Tool. Many tools do not apply to all types of
projects. So, the available tools depend on the project/application you
have selected in the Project Explorer.

Launching Tools from the IDE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To launch a tool from the Eclipse IDE, right-click on the *<app-name>*
project in the Project Explorer, select **ModusToolbox**, and then
select the appropriate tool.

|image47|

.. note::

   You can also launch available tools from links in the Quick  
   Panel.

Library Manager
~~~~~~~~~~~~~~~

The Library Manager allows you to select which Board Support Package
(BSP) and version should be used by default when building a ModusToolbox
application. It also allows you to add and remove libraries, as well as
change their versions.

For more information about how to use this tool, refer to the *Library
Manager User Guide*, available from the tool's **Help** button.

BTSpy and ClientControl
~~~~~~~~~~~~~~~~~~~~~~~

BTSpy is a trace utility that can be used in the WICED Bluetooth
applications to view protocol and generic trace messages from the
embedded device. The tool listens on the UDP port 9876 and can receive
specially formatted message from another application on the same or
different system.

BTSpy can be used in conjunction with ClientControl to receive, decode
and display protocol application and stack trace messages. ClientControl
communicates with the embedded app to perform various functionalities,
tests, exercising features, etc.

DFU Host Tool
~~~~~~~~~~~~~

The Device Firmware Update (DFU) Host tool is a stand-alone program used
to communicate with a PSoC 6 MCU that has already been programmed with
an application that includes device firmware update capability. For more
information, refer to the *Device Firmware Update Host Tool* guide,
available from the tool's **Help** menu.

Power Estimator
~~~~~~~~~~~~~~~

The Power Estimator tool provides an estimate of power consumed by a
target device (also called platform). This is a stand-alone tool
included with the ModusToolbox software. Currently, it can only be used
with the following devices:

-  CYW920819EVB_02 (BTSDK)

-  CY8CKIT-062S2-43012 (Mbed OS)

For more information, refer to the *Power Estimator User Guide*,
available from the tool's **Help** menu.

Rename Application
------------------

The Eclipse IDE for ModusToolbox uses the standard Eclipse rename
functionality. That is, right-click on the application and select
**Rename**. If you use the rename feature, you will need to update your
application’s launch configurations. The easiest way to do this is to
use the “Generate Launches…” link in the Quick Panel.

After renaming the application, select it in the Project Explorer and
notice that there is only one item under Launches. Click on the
“Generate Launches…” link. After a few moments, the generate process
completes. Click on the application again in the Project Explorer and
notice that all the items are shown under Launches in the Quick Panel.

|image48| |image49|

Build Applications
==================

This chapter covers various aspects of building applications. Building
applications is not specifically required, because building is performed
as part of the `programming and debugging 
process <#program-and-debug>`__. However, if you are running
the Eclipse IDE without any hardware attached, you may wish to build
your application to ensure all the code is correct. If you changed code
in one of your projects, you may wish to build just that project.

-  `Build with Make <#build-with-make>`__

-  `Build with IDE <#build-with-ide>`__

-  `GCC Version <#gcc-version>`__

Build with Make
---------------

You can build applications from the command line using Make. Refer to
the *ModusToolbox User Guide* located in the *<install>/ide_2.1/docs
directory* for more information. It is also available from the IDE's
**Help** menu.

Build with IDE
--------------

After loading an application, it is best to first build everything to
generate the necessary files. Click on a project in the Project
Explorer. Then in the **Quick Panel**, click the "Build <app-name>
Application" link.

|image50|

Building an application will typically allow IntelliSense to work
better. It may also be useful to right-click on a project and select
**Index > Rebuild** to allow IntelliSense to find references.

Messages display in the Console, indicating whether the build was
successful or not.

|image51|

.. note::

   Be aware that there are several Console views available.

You can access the different views using the **Display Selected
Console** |image52| button (or the pull-down arrow). The Global Build
Console is the only way to see all of the results in one place. If you
just have the standard Console open, it resets every time a new project
in the application starts building. You won’t see any errors if they are
not on the final project that gets built.

For subsequent updates, you can build one or more projects using the
right-click menu options. Any projects that are dependent on the project
being built will also be built. The Eclipse IDE supports all the usual
application and build options available for the native Eclipse IDE.

.. note::

   When the active build configuration is changed it affects only
   the selected project. The active build configuration for any dependent
   projects is specified separately for each project. Therefore, if you
   want to use the same configuration for all projects (for example, Debug
   or Release), it must be set for each project. It is possible to select
   multiple projects at once from the Project Explorer and then select the
   active configuration.

GCC Version
-----------

ModusToolbox software includes GCC version 7.2.1 as the preferred
toolchain to use with the Eclipse IDE. If you have a different version
of GCC you prefer, update the project properties to point to the
appropriate toolchain folder. Open the project Properties dialog, and
click the **Toolchains** tab, and then click the appropriate link to
update global or workspace preferences, or project properties.

|image53|

Program and Debug
=================

Programming and debugging is native to your chosen development
environment. Cypress devices are supported in the major program and
development solutions. Primarily, this means J-Link and OpenOCD. These
solutions provide for programming flash within a device and provide a
GDB server for debugging.

This chapter covers various topics related to building and debugging
using the Eclipse IDE for PSoC 6 devices and WICED Bluetooth devices.

-  `PSoC 6 Programming/Debugging <#psoc-6-programming-debugging>`__

	-  `Launch Configurations <#launch-configurations>`__

	-  `Using JTAG Interface in MiniProg4 <#using-jtag-interface-in-miniprog4>`__

	-  `Erasing external memory <#erasing-external-memory>`__

	-  `Programming eFuse <#programming-efuse>`__

	-  `Debug Connection Options <#debug-connection-options>`__

	-  `Select Specific CMSIS-DAP
   	   Device <#select-specific-cmsis-dap-device>`__

	-  `Select Specific J-Link Device <#select-specific-j-link-device>`__

	-  `KitProg Firmware Loader <#kitprog-firmware-loader>`__

-  `Bluetooth
   Programming/Debugging <#bluetooth-programming-debugging>`__

	-  `Program Configuration <#program-configuration>`__

	-  `Attach Configurations <#attach-configurations>`__

	-  `Debug Settings <#debug-settings>`__

-  `Mbed OS Programming/Debugging <#mbed-os-programming-debugging>`__

	-  `Launch Configurations <#launch-configurations-1>`__

	-  `Change PyOCD Message Color <#change-pyocd-message-color>`__

	-  `Attach to Running Target
   	   (PyOCD) <#attach-to-running-target-pyocd>`__

PSoC 6 Programming/Debugging
----------------------------

Launch Configurations
~~~~~~~~~~~~~~~~~~~~~

The flow for programming and debugging is similar for all devices. The
Eclipse IDE contains several Launch Configurations that control various
settings for programming the devices and launching the debugger.
Depending on the kit and type of applications you are using, there are
various Launch Configurations available.

There are two sets of configurations: one for KitProg3 (included
on-board on most Cypress PSoC 6 based kits) and another for SEGGER
J-Link.

.. note::

   The KitProg3 launch configuration also supports the MiniProg4,
   which you attach to the kit via a 10-pin debug connection. See the
   `MiniProg4 product
   page <https://www.cypress.com/documentation/development-kitsboards/cy8ckit-005-miniprog4-program-and-debug-kit>`__
   for details. The MiniProg3 is not supported.

The Launch Configurations are shown in the Run/Debug Configurations
dialog, similar to the following.

|image54|

You can open these dialogs from the **Run** menu or by selecting the
down arrow |image55| next to the **Run** and **Debug** commands.

These configurations include the application name and protocol, for
example:

	*CapSenseSlider Program (KitProg3)*

	*CapSenseSlider Debug (J-Link)*

.. note::

   KitProg3 configurations may not work if a J-Link probe is
   attached to the kit.

When an application is created, the tool generates the following launch
configurations for both KitProg3 and J‑Link. Some items display in the
**Quick Panel**, and some are in the Run/Debug Configurations dialog
only.

-  **Attach:** This launch configuration starts a Cortex-M4 debugging
   session attaching to a running PSoC 6 target without programming or
   reset.

-  **Debug:** This launch configuration builds the entire application on
   both cores, programs all the device's memories, and then starts a
   Cortex-M4 debugging session.

-  **Erase:** This launch configuration erases all internal memories.

-  **Program:** This launch configuration builds the entire application
   on both cores, programs all the device's memories, and then runs the
   program.

Using JTAG Interface in MiniProg4
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The MiniProg4 can interact with the target via the SWD or JTAG protocol.
By default, the SWD protocol will be used.

In order to use JTAG, you need to add specific commands for the
appropriate Debug Configuration. Under the **Debugger** tab in the
**Config options** field (after -c "source [find
interface/kitprog3.cfg]"), insert the following lines:

|image56|

-  -c "cmsis_dap_vid_pid 0x04B4 0xF151 0x04B4 0xF152"

-  -c "transport select jtag"

Erasing external memory
~~~~~~~~~~~~~~~~~~~~~~~

To erase external memory, you must modify the "Erase" launch
configuration options on the **Debugger** tab as follows:

For the **CY8CKIT-064-S2-4343W and CY8CPROTO-064-SB kits**, enter the
following to disable external memory programming via SMIF:

	-c "set DISABLE_SMIF 1"

|image57|

For **all other kits**, add the path to the GeneratedSource folder
(after -s "${openocd_path}/../scripts"):

	-s "./libs/<TARGET_NAME>/COMPONENT_BSP_DESIGN_MODUS/GeneratedSource"

|image58|

Programming eFuse
~~~~~~~~~~~~~~~~~

PSoC 6 MCUs contain electronic fuses (eFuses), which are one-time
programmable. They store device specific information, protection
settings and customer data.

| By default eFuses are not programmed even if they are present in the
  programming file. To enable eFuse programming, add the following
  command for the appropriate Debug Configuration, under the
  **Debugger** tab in the **Config options** field
| (after -c "source [find target/psoc6.cfg]"):

	-c "psoc6 allow_efuse_program on"

|image59|

.. warning::

   Because blowing an eFuse is an irreversible process, Cypress
   recommends programming only in mass production programming under
   controlled factory conditions and not prototyping stages. Programming
   fuses requires the associated I/O supply to be at a specific level: the
   device VDDIO0 (or VDDIO if only one VDDIO is present in the package)
   supply should be set to 2.5 V (±5%).

Debug Connection Options
~~~~~~~~~~~~~~~~~~~~~~~~

By default, a typical PSoC 6 application created in the Eclipse IDE
includes `launch configurations <#launch-configurations>`__ set up for
two probes: Cypress KitProg3 (built into Cypress kits) and Segger
J-Link.

+----------------------+----------------------+--------------------------------------------------------------+
| Communication        | Debug Connection     | More Information                                             |
| Firmware             |                      |                                                              |
+======================+======================+==============================================================+
| Cypress-provided     | OpenOCD via          | https://www.cypress.com/products/psoc-programming-solutions  |
| KitProg3             | CMSIS-DAP            |                                                              |
|                      |                      |                                                              |
+----------------------+----------------------+--------------------------------------------------------------+
| SEGGER-provided      | SEGGER J-Link        | `SEGGER J-Link <https://www.segger.com/products/debu         |
| J-Link DLL           |                      | g-probes/j-link/>`__                                         |                 
|                      |                      |                                                              |
+----------------------+----------------------+--------------------------------------------------------------+

Select Specific CMSIS-DAP Device
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If there are two or more CMSIS-DAP devices connected to your computer,
the first detected device will be used by default. KitProg3 supports
both CMSIS-DAP modes – HID and BULK. BULK devices are selected first,
then HID devices. You can specify a CMSIS-DAP device by:

-  VID and PID of the USB device

-  Serial Number of the USB device

-  VID, PID, and Serial Number of the USB device

To do this, you must add a specific command for the appropriate Debug
Configuration, under the **Debugger** tab in the **Config options**
field (after -c "source [find interface/kitprog3.cfg]")

|image60|

Selecting by VID and PID
^^^^^^^^^^^^^^^^^^^^^^^^

Use OS-specific tools to determine the VID and PID of connected devices.
For example, on Windows, use the Device Manager. Use the
"cmsis_dap_vid_pid" command to select a CMSIS-DAP device with a specific
VID and PID. If there are two or more devices with the same specified
VID/PID pair, OpenOCD uses the first detected device from the passed
list.

-  To specify KitProg3 in CMSIS-DAP BULK mode with VID = 0x04B4 and PID
   = 0xF155:

..

   	cmsis_dap_vid_pid 0x04B4 0xF155

-  To specify KitProg3 in CMSIS-DAP HID mode with VID = 0x04B4 and PID =
   0xF154:

..

  	 cmsis_dap_vid_pid 0x04B4 0xF154

-  To specify any (HID or BULK) connected KitProg3 device:

..

  	 cmsis_dap_vid_pid 0x04B4 0xF154 0x04B4 0xF155

Selecting by Serial Number
^^^^^^^^^^^^^^^^^^^^^^^^^^

| There should not be more than one device with the same serial number.
  Use this method if you want to use only one specific device. Use
  OS-specific tools to determine the Serial Number of connected devices.
  You can also use the fw-loader utility with
| --device-list option. See `KitProg Firmware
  Loader <#kitprog-firmware-loader>`__.

Use the "cmsis_dap_serial" command to select a CMSIS-DAP device with a
specific Serial Number.

-  To specify a CMSIS-DAP device with Serial Number = 0B0B0F9701047400
   the following command is used:

   cmsis_dap_serial 0B0B0F9701047400

Selecting by both VID/PID and Serial Number
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can use both commands together in any order. For example:

	cmsis_dap_vid_pid 04B4 F155 cmsis_dap_serial 0B0B0F9701047400

Select Specific J-Link Device
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If there are two or more J-Link devices connected to your computer, the
first detected device is used by default. You can select a specific
J-link device by setting the serial number for the appropriate Debug
Configuration, under the **Debugger** tab:

|image61|

KitProg Firmware Loader
~~~~~~~~~~~~~~~~~~~~~~~

The PSoC 6 MCU kits include onboard programmer/debug firmware, called
KitProg. The CY8CPROTO-062-4343W kit has KitProg3 by default. However,
the CY8CKIT-062-BLE and CY8CKIT-062-WIFI-BT kits come with KitProg2
firmware installed, which does not work with the ModusToolbox software.
**You must update to KitProg3**. KitProg3 provides the CMSIS-DAP (HID)
protocol and the CMSIS-DAP (Bulk) protocol, which is up to ~2.5 times
faster. Both modes can be used via OpenOCD.

ModusToolbox software includes a command-line tool "fw-loader" to update
Cypress kits and switch the KitProg firmware from KitProg2 to KitProg3,
and back. The following is the default installation directory of the
tool on Windows:

	*<user_home>\\ModusToolbox \\tools_<version>\\fw-loader\bin\\*

For other operating systems, the installation directory will vary, based
on where the software was installed.

Use the fw-loader tool to update the KitProg firmware as required for
your needs. KitProg2 does not work with the ModusToolbox software.
Likewise, if you update to KitProg3, PSoC Creator won’t work with
Cypress kits until you restore KitProg2.

.. note::
 
   On a Linux machine, you must run the *udev_rules\install_rules.sh* 
   script before the first run of the fw-loader.

   For more details, refer to the *KitProg3 User Guide*.

Bluetooth Programming/Debugging
-------------------------------

WICED Bluetooth applications have different launch configurations than
PSoC 6 MCU applications. Bluetooth applications use External Tools
configurations for programming the device, and Attach configurations for
attaching to the debugger. The Program launch configurations are
available from the Quick Panel when you select <app name> project. Each
configuration is preceded by the application name.

|image62|

Program Configuration
~~~~~~~~~~~~~~~~~~~~~

This launch configuration runs a script and programs the device's
memories.

-  **<app-name> Program:** Program is used to build and program embedded
   applications onto the target device.

Attach Configurations
~~~~~~~~~~~~~~~~~~~~~

The Attach configurations are used to attach to the debugger without
first programming the device. They include:

-  **<app-name> Attach_JLink**

-  **<app-name> Attach_KitProg3**

Debug Settings
~~~~~~~~~~~~~~

WICED Bluetooth devices use the JTAG SWD interface on a kit for debug
via OpenOCD or J-Link probe. Typically, GPIOs need to be reprogrammed
(via firmware) to enable SWD, so debug can't be performed directly after
a device reset. The debugger is usually attached to a running device and
symbols only are loaded.

The CYW920819EVB-02 and CYBT-213043-MESH kits have additional
requirements in order to launch the Eclipse IDE debugger. In general,
most debugging for these kits is done with logs and sniffers, since real
time execution is usually needed for the protocols to run properly.

Refer to the `WICED Hardware Debugging
document <https://cypresssemiconductorco.github.io/btsdk-docs/BT-SDK/WICED-Hardware-Debugging.pdf>`__
(002-20504) for more details.

Mbed OS Programming/Debugging
-----------------------------

.. _launch-configurations-1:

Launch Configurations 
~~~~~~~~~~~~~~~~~~~~~

When an application is imported from Mbed OS into the Eclipse IDE, the
tool generates the following launch configurations. There are three
debug configurations and three program configurations:

	*<target>_<app-name>_debug – Programs the device as needed and launches
	the debugger.*

	*<target>_<app-name>_erase – Erases the device.*

	*<target>_<app-name>_program – Programs the device.*

|image63|

.. note::

   The three debug launches will attempt to switch to the debug
   perspective, while the three program launches will not. Cypress
   recommends using the debug launch for "debug" and the program launches
   for "erase" and "program."

Change PyOCD Message Color
~~~~~~~~~~~~~~~~~~~~~~~~~~

Messages in the console for pyOCD display in red because they are
written to stderr. The Eclipse preference for stderr defaults to red. If
you want to change the color of stderr:

1. Open the Preferences dialog and select **Run/Debug > Console**.

2. Click on the option for **Standard Error text color**, select the
   desired color, and click **OK**.

3. Then, click **Apply and Close** to close the Preferences dialog.

Attach to Running Target (PyOCD)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are some cases where you may wish to attach to a running PSoC 6
target as part of a debug session. This is accomplished by copying and
modifying existing debug configurations.

1. See `Mbed OS to ModusToolbox Flow <#mbed-os-to-modustoolbox-flow>`__
   for instructions to import an example Mbed project into the Eclipse
   IDE workspace.

2. Open the Debug Configurations dialog, and duplicate the Debug
   configuration; for example,
   "CY8CPROTO_062_4343W_mbed-os-example-blinky_debug."

|image64|

3. Rename the new Debug configuration to something meaningful, such as
   "CY8CPROTO_062_4343W_mbed-os-example-blinky_attach."

4. Go to the **Startup** tab.

|image65|

	a. Unselect **Initial Reset**.

	b. Unselect **Load executable**.

	c. Unselect **Pre-run/Restart reset**.

	d. Unselect **Set breakpoint at**.

	e. Select **Continue**.

5. Ensure your KitProg3 is in DAPLink mode and your target is running.
   See `KitProg Firmware Loader <#kitprog-firmware-loader>`__.

6. Click **Debug** to attach to the running target using the new launch
   configuration.

.. |image1| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image4.png
.. |image2| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image5.png
.. |image3| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image6.png
.. |image4| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image6.png
.. |image5| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image7.png
.. |image6| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image8.png
.. |image7| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image9.png
.. |image8| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image10.png
.. |image9| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image11.png
.. |image10| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image12.png
.. |image11| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image13.png
.. |image12| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image14.png
.. |image13| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image15.png
.. |image14| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image16.png
.. |image15| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image17.png
.. |image16| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image18.png
.. |image17| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image19.png
.. |image18| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image20.png
.. |image19| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image21.png
.. |image20| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image22.png
.. |image21| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image23.png
.. |image22| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image24.png
.. |image23| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image8.png
.. |image24| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image25.png
.. |image25| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image26.png
.. |image26| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image27.png
.. |image27| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image28.png
.. |image28| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image29.png
.. |image29| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image30.png
.. |image30| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image31.png
.. |image31| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image32.png
.. |image32| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image33.png
.. |image33| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image34.png
.. |image34| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image35.png
.. |image35| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image36.png
.. |image36| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image37.png
.. |image37| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image38.png
.. |image38| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image39.png
.. |image39| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image40.png
.. |image40| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image41.png
.. |image41| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image42.png
.. |image42| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image43.png
.. |image43| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image44.png
.. |image44| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image45.png
.. |image45| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image46.png
.. |image46| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image47.png
.. |image47| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image48.png
.. |image48| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image49.png
.. |image49| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image50.png
.. |image50| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image19.png
.. |image51| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image51.png
.. |image52| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image52.png
.. |image53| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image53.png
.. |image54| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image54.png
.. |image55| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image55.png
.. |image56| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image56.png
.. |image57| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image57.png
.. |image58| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image58.png
.. |image59| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image59.png
.. |image60| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image60.png
.. |image61| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image61.png
.. |image62| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image22.png
.. |image63| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image62.png
.. |image64| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image63.png
.. |image65| image:: ../_static/image/tool-guide/ModusToolbox-IDE-User-Guide/image64.png
