===================================
ModusToolbox IDE Quick Start Guide
===================================


ModusToolbox™ software is a set of tools that support Cypress device
configuration and application development. These tools enable you to
integrate Cypress devices into your existing development methodology.

This guide provides a quick walkthrough for using the Eclipse IDE
provided as part of ModusToolbox. If you wish to use other IDEs, such as
VS Code, IAR, or µVision, refer to the “Export to IDEs” chapter in the
`ModusToolbox User
Guide <http://www.cypress.com/ModusToolboxUserGuide>`__.

Download/Install ModusToolbox and Run Eclipse IDE
=================================================

Refer to the instructions in the `ModusToolbox Installation
Guide <http://www.cypress.com/ModusToolboxInstallGuide>`__. After you
accept the License Agreement, the Eclipse IDE opens a dialog to specify
the workspace. Then, it opens with an empty Project Explorer by default.

|image0|

This guide focuses on these areas of the Eclipse IDE:

-  The **Project Explorer** contains the application folders and files.

-  The **Quick Panel** provides links to tool commands and
   documentation.

-  The **Console** displays various messages when updating the
   application in some way (building, programming, etc.)

Create New Application
======================

Creating an application includes several steps, as follows:

Step 1: Open Project Creator Tool
---------------------------------

In the **Quick Panel**, click the “New Application” link.

|image1|

Or, select **File > New > ModusToolbox Application**.

|image2|

The Eclipse IDE displays a message and waits for the Project Creator
tool to open.

|image3|

Step 2: Choose Board Support Package (BSP) 
------------------------------------------

When the Project Creator tool opens, click on the **Kit Name**; see the
description for it on the right. For this example, select the
**CY8CKIT-062-WIFI-BT** kit. The following image is an example; the
precise list of boards available in this version of ModusToolbox will
reflect the platforms available for development.

|image4|

Step 3: Select Application
--------------------------

Click **Next >** to open the Select Application page.

The applications demonstrate different features available on the kit.
For this example, select **Hello World** from the list. This application
exercises the PSoC 6 MCU to blink an LED.

|image5|

.. note:: The actual application names available might vary.

   Type a name for your application or leave the default name. Do not use
   spaces in the application name. Also, do not use common illegal
   characters, such as:

   \* . “ ‘ / \\ [ ] : ; \| = ,

Step 4: Create Application
--------------------------

Click **Create** to start creating the application. The tool displays
various messages.

|image6|

When the process completes, a message states that the application was
created. Click the **Close** button to close the Project Creator tool.

|image7|

Then, the IDE displays a message that the project is being imported.

|image8|

After several moments, the application opens with the *Hello_World*
project in the Project Explorer, and the *README.md* file opens in the
file viewer.

|image9|

Add/Modify Application Code
===========================

Code example applications work as they are, and there is no need to add
or modify code. However, if you want to update and change the
application to do something else, open the appropriate file in the code
editor.

Double-click the *main.c* file to open it.

|image10|

As you type into the file, an asterisk (*) will appear in the file’s tab
to indicate changes were made. The **Save/Save As** commands will also
become available to select.

Configure Device Resources
==========================

To enable and configure peripherals, pins, clocks, etc., open the Device
Configurator. Right-click on the project folder and select
**ModusToolbox > Device Configurator 2.1**.

|image11|

The Device Configurator provides access to the BSP resources and
settings. Each enabled resource contains one or more links to the
related API documentation. There are also buttons to open other
configurators for CapSense\ :sup:`®`, QSPI, Smart I/O™, etc.

.. note::

   The Device Configurator cannot be used to open Library
Configurators, such as Bluetooth and USB.

For more information, refer to the `Device Configurator
Guide <https://www.cypress.com/ModusToolboxDeviceConfig>`__, which is
also available by selecting **View Help** from the tool’s **Help** menu.

Build the Application
=====================

Building the application is not specifically required, because building
will be performed as part of the programming and debugging process.
However, if you are running the Eclipse IDE without any hardware
attached, you may wish to build your application to ensure all the code
is correct.

In the **Quick Panel**, click the “Build Hello_World Application” link.
Build information will display in the Console pane.

|image12|

If you have the *Hello_World* project selected, you can also select
**Build Project** from the **Project** menu or from the right-click
menu. The Eclipse menus change based on what you have selected in the
Project Explorer.

|image13|

Program the Device
==================

There are several different “Launch Configurations” for programming and
debugging the various development kits and starter applications within
the Eclipse IDE. These Launch Configurations provide details about how
to configure the application. This section provides a brief walkthrough
for programming this example. For more details about other Launch
Configurations and settings, refer to the `Eclipse IDE for ModusToolbox
User Guide <http://www.cypress.com/MTBEclipseIDEUserGuide>`__, “Program
and Debug” chapter.

Connect the Kit
---------------

Follow the instructions provided with the CY8CKIT-062-WIFI-BT kit to
connect it to the PC with the USB cable.

.. note::

   This kit’s firmware may be set to KitProg2. To use this kit
   with the ModusToolbox software, you must upgrade the firmware to
   KitProg3. Refer to the `KitProg3 User
   Guide <https://www.cypress.com/file/452701/download>`__ for more
   details.

Program
-------

In the Project Explorer, select the *Hello_World* application. Then, in
the **Quick Panel**, click the “Hello_World Program (KitProg3)” link.

|image14|

If needed, the IDE builds the application and messages display in the
Console. If the build is successful, device programming starts
immediately. If there are build errors, then error messages will
indicate as such. When complete, the LED will start blinking.

|image15|

Debug the Program
=================

As mentioned under the `Program the Device <#program-the-device>`__
section in this document, there are many different Launch Configurations
for different kits and applications. This section provides a brief
walkthrough for debugging this example. For more details about other
Launch Configurations and settings, refer to the `Eclipse IDE for
ModusToolbox User
Guide <http://www.cypress.com/MTBEclipseIDEUserGuide>`__, “Program and
Debug” chapter.

In the Project Explorer, select the *Hello_World* application. Then, in
the **Quick Panel**, click the “Hello_World Debug (KitProg3)” link under
**Launches**.

|image16|

If needed, the IDE builds the application and messages display in the
Console. If the build is successful, the IDE switches to debug mode
automatically. If there are build errors, then error messages will
indicate as such.

|image17|

Next Steps
==========

Refer to the `Eclipse IDE for ModusToolbox User
Guide <http://www.cypress.com/MTBEclipseIDEUserGuide>`__ for information
such as configuring pins, application settings, peripheral options, as
well as adding/removing software components.

Refer to the `Eclipse Workbench User
Guide <http://help.eclipse.org/oxygen/index.jsp?nav=%2F0_4_4>`__ for
information about the Eclipse environment. Cypress also provides the
`Eclipse IDE Survival
Guide <http://www.cypress.com/EclipseIDESurvivalGuide>`__.

.. |image0| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image1.png
.. |image1| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image2.png
.. |image2| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image3.png
.. |image3| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image4.png
.. |image4| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image5.png
.. |image5| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image6.png
.. |image6| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image7.png
.. |image7| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image8.png
.. |image8| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image9.png
.. |image9| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image10.png
.. |image10| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image11.png
.. |image11| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image12.png
.. |image12| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image13.png
.. |image13| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image14.png
.. |image14| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image15.png
.. |image15| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image16.jpg
.. |image16| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image17.png
.. |image17| image:: ../_static/image/tool-guide/ModusToolbox-IDE-Quick-Start-Guide/image18.png

